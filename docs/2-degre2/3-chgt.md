# Changement de variable

- $3x^2 + 8x + 5$ est un trinôme du second degré en $x$.
- $3t^2 + 8t + 5$ est un trinôme du second degré en $t$.
- $3(t+1)^2 + 8(t+1) + 5$ est un trinôme du second degré en $t+1$.
- $3t^4 + 8t^2 + 5$ est un trinôme du second degré en $t^2$.
- $x-2\sqrt x -3$ est un trinôme du second degré en $\sqrt x$, en effet, on peut l'écrire $\left( \sqrt x \right)^2 -2\sqrt x -3$.

!!! example "Exemple d'utilisation : résoudre $x-2\sqrt x -3 = 0$"

    On pose $t = \sqrt x$, l'équation s'écrit alors $t^2-2t-3 = 0$, on obtient un trinôme du second degré en $t$ dont le discriminant est $\Delta = (-2)^2 -4×1×(-3) = 4 + 12 = 16$. On constate que $\Delta > 0$ et on a $\sqrt \Delta = \sqrt {16} = 4$, l'équation a pour solutions

    $$t_1 = \frac{2-4}{2} \text{ et } t_2 = \frac{2+4}{2}$$

    D'où $t = -1$ **ou** $t = 3$.

    > On peut vérifier que $(t+1)(t-3) = t^2-2t-3$

    Il reste à résoudre l'équation $\sqrt x = t$ pour les deux valeurs de $t$ trouvées.

    1. $\sqrt x = -1$ n'a aucune solution.
    2. $\sqrt x = 3$ ne possède qu'**une** seule solution : $x = 9$.

    **Conclusion** : L'unique solution de $x-2\sqrt x -3 = 0$ est $x = 9$. On peut aussi le vérifier en traçant un graphique.

