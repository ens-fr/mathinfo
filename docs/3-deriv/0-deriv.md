# Introduction

==À reprendre==

## Contexte

Dans ce chapitre, on travaille avec des fonctions usuelles, et celles que l'on construit facilement avec. Des fonctions numériques à une seule variable réelle.

Dans les études supérieures, on rencontre

- des fonctions à plusieurs variables, et on dérive par rapport à l'une et/ou l'autre...
- des _monstres_ : des fonctions curieuses, continues, mais nulle part dérivables...

Dans ces conditions, on donne des exemples de fonctions simples que l'on envisage :

1. $f(x) = (2x-1)(x+3)$ sur $\mathbb R$
1. $f(x) = x^2(\sqrt x + 1)$ sur $[0\,;\,+\infty[$
1. $f(x) = \dfrac{x^3+1}{x^2-1}$ sur $\mathbb R\setminus \{-1\,;\,+1\}$
1. $f(x) = \dfrac{x^2+x+1}{\sqrt x}$ sur $]0\,;\,+\infty[$
1. $f(x) = \dfrac{\sqrt x}{x-1}$ sur $]1\,;\,+\infty[$

Une telle fonction possède une courbe représentative.

Pour **presque chaque point** de la courbe, on peut tracer une tangente à la courbe.

Lorsque c'est le cas pour une fonction $f$ au point d'abscisse $x_0$, on peut écrire en notant $f'(x_0)$ le coefficient directeur de cette tangente.

$$f(x) \approx f(x_0) + f'(x_0)×(x - x_0)  \text{ pour } x\approx x_0$$


## Le nombre dérivé

Pour déterminer le nombre dérivé $f(x_0)$, on peut utiliser la notion de limite, quand elle existe.


## L'équation de la tangente

Si une fonction $f$ est dérivable en $x_0$, alors la courbe représentative de $f$, que l'on note $\mathcal C_f$ possède une tangente d'équation :

$$T_{x_0} : y = f(x_0) + f'(x_0)×(x - x_0)$$

!!! example "Exemple"
    Si $f$ est dérivable en $5.1$, avec

    - $f(5.1) = -3.2$
    - $f'(5.1) = 7.8$

    alors $\mathcal C_f$ admet une tangente au point $A$ d'abscisse $(5.1\,;\,-3.2)$, d'équation

    $$T_A : y = -3.2 + 7.8×(x - 5.1)$$

    On vérifie sans mal que cette tangente passe par $A$ et possède un coefficient directeur égal à $7.8$.


!!! tip "La réciproque est vraie"
    Si une fonction $f$ possède une courbe représentative $\mathcal C_f$ avec une tangente au point d'abscisse $x_0$, dont l'équation est :

    $$T_{x_0} : y = f(x_0) + m×(x - x_0)$$

    Alors $f$ est dérivable en $x_0$ et on a $f'(x_0) = m$.

## Dériver une fonction

On procède par analyse de la fonction :

- Est-ce une fonction usuelle ?
    - Si oui, on connait sa dérivée. (Le tableau de résumé est à apprendre)
    - Si non, quelle est la **dernière** opération en terme priorité pour la construire ? On la décompose.

