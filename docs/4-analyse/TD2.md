# TD2

## Exercice 1

Montrer, à l'aide de la définition, que la suite de terme général $u_n = \frac{n + 1}{2n + 3}$ a pour limite $\frac12$.

??? success "Réponse - étape 1"

    On cherche à majorer $\left\vert u_n - \frac12\right\vert$

    On a , pour $n\in\mathbb N$ :

    $$\begin{align*}
    \left\vert u_n - \frac12\right\vert &= \left\vert\frac{n + 1}{2n + 3} - \frac12\right\vert\\
    \left\vert u_n - \frac12\right\vert &= \left\vert\frac{2(n + 1)}{2(2n + 3)} - \frac{2n+3}{2(2n+3)}\right\vert\\
    \left\vert u_n - \frac12\right\vert &= \left\vert\frac{(2n + 2)-(2n+3)}{2(2n + 3)}\right\vert\\
    \left\vert u_n - \frac12\right\vert &= \left\vert\frac{-1}{2(2n + 3)}\right\vert\\
    \left\vert u_n - \frac12\right\vert &= \frac{1}{2(2n + 3)}\\
    \left\vert u_n - \frac12\right\vert &< \frac{1}{4n} \text{ pour } n\neq 0\\
    \end{align*}$$

??? success "Réponse - étape 2"

    Soit $\varepsilon>0$, on pose $N\in\mathbb N^*$ la partie entière supérieure de $\dfrac{1}{4\varepsilon}$, on a donc $N\geqslant \dfrac{1}{4\varepsilon}$

    La fonction inverse étant décroissante sur $\mathbb R_{+}^*$, on a pour tout $n \geqslant N, \dfrac1n \leqslant \dfrac1N\leqslant 4\varepsilon$, d'où $\dfrac1{4n} \leqslant\dfrac1{4N} \leqslant \varepsilon$

    On déduit alors de l'étape 1 que $\left\vert u_n - \frac12\right\vert < \dfrac1{4n} \leqslant\dfrac1{4N} \leqslant \varepsilon$ pour tout $n\geqslant N$

    On vient de montrer que $\forall \varepsilon >0, \exists N\in \mathbb N$ tel que si $n\geqslant N$, alors $\left\vert u_n - \frac12\right\vert < \varepsilon$

    D'après la définition, $(u_n)_{n\in\mathbb N}$ est une suite qui admet $\frac12$ comme limite quand $n$ tend vers l'infini.


## Exercice 2

Montrer, à l'aide de la définition, que la suite de terme général $u_n = \frac{2n + 1}{n + 2}$ a pour limite $2$.

??? success "Réponse"

    On cherche à majorer $\left\vert u_n - 2\right\vert$

    On a , pour $n\in\mathbb N$ :

    $$\begin{align*}
    \left\vert u_n - 2\right\vert &= \left\vert\frac{2n + 1}{n + 2} - 2\right\vert\\
    \left\vert u_n - 2\right\vert &= \left\vert\frac{2n + 1}{n + 2} - \frac{2(n+2)}{n+2}\right\vert\\
    \left\vert u_n - 2\right\vert &= \left\vert\frac{(2n + 1)-(2n+4)}{n + 2}\right\vert\\
    \left\vert u_n - 2\right\vert &= \left\vert\frac{-3}{n + 2}\right\vert\\
    \left\vert u_n - 2\right\vert &= \frac{3}{n+2}\\
    \left\vert u_n - 2\right\vert &< \frac{3}{n} \text{ pour } n\neq 0\\
    \end{align*}$$

    Soit $\varepsilon>0$, on pose $N\in\mathbb N^*$ la partie entière supérieure de $\dfrac{3}{\varepsilon}$, on a donc $N\geqslant \dfrac{3}{\varepsilon}$

    La suite est totalement similaire à l'exercice précédent.


## Exercice 3

Soit la suite géométrique de raison $\frac14$ et de premier terme $u_1 = 2$.

1. Exprimer le terme d'indice $n$ en fonction du terme d'indice $n − 1$ puis en fonction de $n$.
2. Étudier la monotonie de cette suite.
3. Cette suite est-elle convergente ?

??? success "Réponse"

    1. On a :
        - $u_n = \frac14 × u_{n-1}$, pour tout $n\geqslant 2$
        - Remarque $u_0$ n'est pas défini ici, donc on n'a pas le droit d'écrire cette relation pour $n=1$
        - $u_n = u_1×\left(\frac14\right)^{n-1}$, pour $n\geqslant 1$, d'où
        - $u_n = \frac2{4^{n-1}}$, pour $n\geqslant 1$
        - $u_n = \frac8{4^n}$, pour $n\geqslant 1$
    2. $(u_n)_n$ est décroissante comme toute suite géométrique dont la raison est comprise entre $0$ et $1$ et dont le premier terme est positif.
    3. $(u_n)_n$ converge vers zéro, comme toute suite géométrique de raison comprise entre $-1$ et $1$.

## Exercice 4

Soit la suite arithmétique de raison $5$ et de premier terme $u_1 = 1$.

1. Exprimer le terme d'indice $n$ en fonction du terme d'indice $n-1$ puis en fonction de $n$.
2. Étudier la monotonie de cette suite.
3. Cette suite est-elle convergente ?

??? success "Réponse"

    1. On a :
        - $u_n = u_{n-1} + 5$, pour tout $n\geqslant 2$
        - Remarque $u_0$ n'est pas défini ici, donc on n'a pas le droit d'écrire cette relation pour $n=1$
        - $u_n = 5(n-1) + u_1$, pour tout $n\geqslant 1$, d'où
        - $u_n = 5(n-1) + 1$, pour tout $n\geqslant 1$
        - $u_n = 5n - 4$, pour tout $n\geqslant 1$
    2. $(u_n)_n$ est croissante comme toute suite arithmétique de raison positive.
    3. $(u_n)_n$ est divergente comme toute suite arithmétique de raison non nulle. Elle tend vers $+\infty$ comme toute suite arithmétique de raison strictement positive.

## Exercice 5

La suite $\left(\dfrac{\cos n}{n + 1}\right)_n$ est-elle convergente ?

??? success "Réponse"

    On a pour $n>0$, $\left\vert\dfrac{\cos n}{n + 1}\right\vert < \dfrac1n$, de sorte que cette suite tend vers zéro quand $n$ tend vers l'infini.

## Exercice 6

La suite $\left(-\dfrac{\sin n^2}{n + 3}\right)_n$ est-elle convergente ?

??? success "Réponse"

    On a pour $n>0$, $\left\vert-\dfrac{\sin n^2}{n + 3}\right\vert < \dfrac1n$, de sorte que cette suite tend vers zéro quand $n$ tend vers l'infini.


## Exercice 7 et 8

Déterminer la nature et la limite éventuelle des suites de termes généraux :

??? success "$u_n = \sqrt{n^2+3n+1} - \sqrt{n^2+2n+1}$ :boom:"

    On vérifie aisément que $u_n$ est bien défini pour tout $n$ de $\mathbb N$, les discriminants des polynômes étant positifs.

    La forme indéterminée $(+\infty) - (+\infty)$ nous conduit à envisager de travailler avec la quantité conjuguée.

    !!! info "Rappel avec la quantité conjuguée"
        Pour $x>0$ et $y>0$, il est parfois utile d'écrire

        $$\begin{align*}
        \sqrt x - \sqrt y &= \dfrac{(\sqrt x + \sqrt y)(\sqrt x - \sqrt y)}{\sqrt x + \sqrt y}\\
        \sqrt x - \sqrt y &= \dfrac{(\sqrt x)^2 - (\sqrt y)^2}{\sqrt x + \sqrt y}\\
        \sqrt x - \sqrt y &= \dfrac{x - y}{\sqrt x + \sqrt y}\\
        \end{align*}$$

    Dans notre situation, on a pour tout $n\in\mathbb N$ :

    $$\begin{align*}
    \sqrt{n^2+3n+1} - \sqrt {n^2+2n+1} &= \dfrac{(n^2+3n+1) - (n^2+2n+1)}{\sqrt {n^2+3n+1} + \sqrt {n^2+2n+1}}\\
    \sqrt{n^2+3n+1} - \sqrt {n^2+2n+1} &= \dfrac{n}{\sqrt {n^2+3n+1} + \sqrt {n^2+2n+1}}\\
    \end{align*}$$

    On a une forme indéterminée quotient de deux infinis, mais on s'en sort aisément avec une factorisation par $n$ au dénominateur. Ainsi, pour $n>0$, on a :

    $$\begin{align*}
    \sqrt{n^2+3n+1} - \sqrt {n^2+2n+1} &= \dfrac{n}{n\left(\sqrt {1+3/n+1/n^2} + \sqrt {1+2/n+1/n^2}\right)}\\
    \sqrt{n^2+3n+1} - \sqrt {n^2+2n+1} &= \dfrac{1}{\sqrt {1+3/n+1/n^2} + \sqrt {1+2/n+1/n^2}}\\
    \end{align*}$$

    De sorte que la suite tend vers $\frac12$.


??? success "$u_n = \frac{1-n}{n^2}$"

    On peut écrire, pour $n>0$ :

    $$u_n = \frac1{n^2} - \frac1n$$

    Chaque terme tend vers zéro quand $n$ tend vers l'infini, de sorte que $(u_n)_n$ tend également vers $0$.



??? success "$u_n = \frac{2+n}{1+n}\left(1+\frac8{n^2}\right)$"


    On peut écrire, pour $n>0$ :

    $$u_n = \frac{\frac2n+1}{\frac1n+1}\left(1+\frac8{n^2}\right)$$

    Chaque facteur tend vers $1$, de sorte que $(u_n)_n$ tend vers $1$ quand $n$ tend vers l'infini.

??? success "$u_n = \frac{3^n-2^n}{3^n+2^n}$"

    On factorise le numérateur et le dénominateur par le terme prépondérant, on a ensuite, pour tout $n\in\mathbb N$ :



    $$\begin{align*}
    u_n &= \frac{3^n-2^2}{3^n+2^2}\\
    u_n &= \frac{3^n\left(1-\frac{2^n}{3^n}\right)}{3^n\left(1+\frac{2^n}{3^n}\right)}\\
    u_n &= \frac{1-\left(\frac{2}{3}\right)^n}{1+\left(\frac{2}{3}\right)^n}\\
    \end{align*}$$

    Ce qui permet de conclure que $(u_n)_n$ tend vers $1$ quand $n$ tend vers l'infini.


??? success "$u_n = \frac{1-n}{n}$"

    On peut écrire, pour $n>0$ :

    $$u_n = \frac1{n} - 1$$

    De sorte que $(u_n)_n$ tend vers $-1$ quand $n$ tend vers l'infini.

??? success "$u_n = \mathrm{e}^{−n^\pi}\cos(\frac{\pi}6)$"

    On a $\pi>0$ donc le terme $-n^\pi$ tend vers $-\infty$ quand $n$ tend vers l'infini, de sorte que son exponentielle tend vers $0$.

    D'autre part, $\cos(\frac{\pi}6)$ est une constante.

    En conclusion, $(u_n)_n$ tend vers $0$ quand $n$ tend vers l'infini.

## Exercice 9 💻

On considère la suite $(u_n)_n$ définie par $u_n = \mathrm e^{2n−1}$

1. Trouver le plus petit entier $n_0$ tel que $n \geqslant n_0 \implies |u_n| > 10^7$
2. Démontrer que $\lim_{n→+∞} u_n = +∞$ en utilisant la définition.

??? success "Réponse"
    $(u_n)_n$ est une suite croissante comme composée de fonction croissante.

    On résout l'équation $\mathrm e^{2x−1} = 10^7$, on tire $2x-1 = \ln(10^7)$, d'où $x = \frac{1+7\ln{10}}{2}$ dont la partie entière supérieure est $n_0=9$.

    On sait que $\mathrm e^x > x$ pour tout $x\in \mathbb R$, on déduit que $u_n>2n-1$ qui peut être aussi grand qu'on le souhaite. Par exemple, pour avoir $u_n>A$ il suffit de prendre $N$ la partie entière supérieure de $\frac{A+1}2$ et alors pour $n\geqslant n$, on a $u_n > A$.

    On a montré que $\forall A\in\mathbb R_{+}^{*}, \exists N\in\mathbb N$ tel que si $n\geqslant N$, alors $u_n>A$.

    Ainsi, d'après la définition $(u_n)_n$ tend vers $+\infty$.

## Exercice 10 💻

On considère la suite $(u_n)_n$ définie par $u_n = 
\ln(2n^2 + 1)$

1. Trouver le plus petit entier $n_0$ tel que $n \geqslant n_0 \implies |u_n| > 10^7$
2. Démontrer que $\lim_{n→+∞} u_n = +∞$ en utilisant la définition.

??? success "Réponse"
    $(u_n)_n$ est une suite croissante comme composée de fonction croissante.

    On résout l'équation $\ln(2x^2 + 1) = 10^7$, on tire $2x^2+1 = \exp(10^7)$, d'où $x = \sqrt{\frac{\exp(10^7)-1}2}$ dont la partie entière supérieure $n_0$ est un entier qui possède plusieurs millions de chiffres...

    On sait que $t\mapsto \ln(t)$ tend vers l'infini quand $t$ tend vers l'infini, donc pour tout $A>0$ il existe un $t_0$ tel que si $t>t_0$ alors $\ln(t)>A$.

    Soit $A>0$, et $t_0$ ainsi associé, on choisit $n_0$ tel que $2n_0^2+1>t_0$, ce qui est possible sachant que $n\mapsto 2n^2+1$ tend vers l'infini quand $n$ tend vers l'infini. De sorte que si $n>n_0$, alors on a $2n^2+1>t_0$ et donc $\ln(2n^2+1)>A$.

    On a montré que $\forall A\in\mathbb R_{+}^{*}, \exists N\in\mathbb N$ tel que si $n\geqslant N$, alors $u_n>A$.

    Ainsi, d'après la définition $(u_n)_n$ tend vers $+\infty$.


## Exercice 11 et 12

Parmi les suites définies ci-dessous par leur terme général dire celles qui sont divergentes et trouver la limite de celles qui sont convergentes.


??? success "$u_n = \frac{1-n^2}{n}$"

    On peut écrire $u_n = \frac{1}{n} - n$

    De sorte que $(u_n)_n$ est la somme d'une suite convergente et d'une suite qui tend vers $-\infty$.


    $(u_n)_n$ est donc divergente et tend également vers $-\infty$.

??? success "$u_n = 3n-7$"

    De même, $(u_n)_n$ est divergente et tend vers $+\infty$.

??? success "$u_n = n \sin^2(n\pi /2)\frac{2n^3+n^2}{(1+n)^2}$"

    Pour $n$ pair, on a $\sin^2(n\pi /2)=0$, donc il y a une suite extraite constante égale à $0$.

    Pour $n$ impair, on a $\sin^2(n\pi /2)=1$, dans ce cas, on peut écrire $u_n = n^2 \frac{2+1/n}{(1+1/n)^2}$ qui offre clairement une suite extraite divergente qui tend vers $+\infty$.

    En conclusion, $(u_n)_n$ est divergente.

    - La suite extraite $(u_{2n})_n$ est constante, égale à $0$.
    - La suite extraite $(u_{2n+1})_n$ tend vers $+\infty$ quand $n$ tend vers l'infini.

??? success "$u_n = \frac{2n^3+n^2}{(1+n)^2}$"
        On peut écrire $u_n = \frac{n^3}{n^2}×\frac{2+1/n}{(1/n+1)^2}$

        D'où $u_n = n×\frac{2+1/n}{(1/n+1)^2}$, ce qui assure que $(u_n)_n$ est divergente et tend vers $+\infty$ quand $n$ tend vers l'infini.

??? success "$u_n = \left(\frac{n}{n+1}\right)^{2n}$"
    On peut écrire :

    $$\begin{align*}
    u_n &= \left(\frac{1}{1+1/n}\right)^{2n}\\
    u_n &= \exp\left(2n×\ln\left(\frac{1}{1+1/n}\right)\right)\\
    u_n &= \exp\left(-2n\ln\left(1+1/n\right)\right)\\
    \end{align*}$$

    Concentrons nous sur $v_n = n\ln\left(1+1/n\right)$ qui tend vers $1$ quand $n$ tend vers l'infini.

    ??? info "Rappel de preuve"
        $v_n = n\ln\left(1+1/n\right)$ s'écrit aussi $v_n = \frac{\ln\left(1+1/n\right)}{1/n} = \frac{\ln\left(1+1/n\right) - \ln(1+0)}{1/n - 0}$

        La limite de $(v_n)_n$ est donc la dérivée en $0$ de la fonction dérivable $x\mapsto \ln(1+x)$.

        Cette dérivée est $x \mapsto \frac{1}{x+1}$, et le nombre dérivé en $0$ est $1$.

        Il suit $\lim_{\substack{x→0 \\ x\neq 0}} \frac{\ln(1+x)}x = 1$, d'où $\lim_{n→+\infty} v_n = 1$

    La fonction $x \mapsto \exp(-2x)$ est continue, par composition, on en déduit que $(u_n)_n$ tend vers $\exp(-2×1)$.

    $$\lim_{n→+\infty} u_n = \mathrm{e}^{-2}$$

!!! info "Croissance comparée polynôme - exponentielle"

    $$\forall n\in\mathbb N\quad \lim_{x→+\infty} \frac{\mathrm{e}^x}{x^n} = +\infty$$

    ??? note "Preuve"
        Soit $n\in\mathbb N$ et $x\in\mathbb R$, on a

        $$\begin{align*}
        \mathrm{e}^{\frac x {n+1}} &> \frac x {n+1}\\
        \mathrm{e}^x &> \left(\frac x {n+1}\right)^{n+1}\\
        \frac{\mathrm{e}^x}{x^n} &> \frac x {(n+1)^{n+1}}\\
        \end{align*}$$

        Avec $n$ fixé, on tire que $\lim_{x→+\infty} \frac{\mathrm{e}^x}{x^n} = +\infty$

??? success "$u_n = (n^4 + 2n^2)\mathrm{e}^{1−n}$"
    On peut écrire $u_n = \mathrm{e}×(1 + 2/n^2) ÷ \frac{\mathrm{e}^{n}}{n^4}$

    De sorte que $(u_n)_n$ tend vers $0$ quand $n$ tend vers l'infini.



??? success "$u_n = \frac{1-n}{n^2}$"
    On peut écrire $u_n = \frac{1}{n}×(\frac1n - 1)$

    D'où $(u_n)_n$ est convergente et tend vers $0$ quand $n$ tend vers l'infini.


??? success "$u_n = 3n^3 - 2n^2 + n - 5$"
    On peut écrire $u_n = n^3(3 - \frac2n + \frac 1{n^2} - \frac5{n^3})$

    Ce qui assure que $(u_n)_n$ est divergente et tend vers $+\infty$ quand $n$ tend vers l'infini.



??? success "$u_n = \sqrt{n+1} - \sqrt{n}$"
    On pense à la quantité conjuguée.

    On peut écrire $u_n = \left(\sqrt{n+1} - \sqrt{n}\right)×\frac{\left(\sqrt{n+1} + \sqrt{n}\right)}{\left(\sqrt{n+1} + \sqrt{n}\right)}$, d'où

    $$\begin{align*}
    u_n &= \frac{\left(\sqrt{n+1}\right)^2 - \left(\sqrt{n}\right)^2}{\left(\sqrt{n+1} + \sqrt{n}\right)}\\
    u_n &= \frac{(n+1) - n}{\left(\sqrt{n+1} + \sqrt{n}\right)}\\
    u_n &= \frac{1}{\left(\sqrt{n+1} + \sqrt{n}\right)}\\
    \end{align*}$$

    D'où $(u_n)_n$ est convergente et tend vers $0$ quand $n$ tend vers l'infini.


??? success "$u_n = \left(n+\frac1n\right)^n$"

    On peut écrire que pour $n > 0$, on a $u_n > n+ \frac 1n$, de sorte que $(u_n)_n$ est divergente et tend vers $+\infty$ quand $n$ tend vers l'infini.

!!! info "Croissance comparée polynôme - logarithme"

    $$\forall \alpha\in\mathbb R_{+}^{*}\quad \lim_{x→+\infty} \frac{\ln(x)}{x^\alpha} = 0$$

    ??? note "Preuve"
        Soit $\alpha\in\mathbb R_{+}^{*}$ et $x\in\mathbb R_{+}^{*}$, on a

        $$\begin{align*}
        \frac{\ln(x)}{x^\alpha} &= \frac2\alpha×\frac1{x^{\alpha/2}}×\frac{\ln(x^{\alpha/2})}{x^{\alpha/2}}
        \end{align*}$$

        On sait que $\ln(t) < t$ pour tout $t>0$, on en tire que

        $$\forall \alpha\in\mathbb R_{+}^{*}\quad \lim_{x→+\infty} \frac{\ln(x)}{x^\alpha} = 0$$

??? success "$u_n = \frac{\left(\ln n\right)^\alpha}{n^\beta}$, avec $\alpha>0, \beta>0$"

    On peut écrire $u_n = \left(\frac{\ln n}{n^{\beta/\alpha}}\right)^\alpha$ et d'après ce qui précède, on tire que $(u_n)_n$ est convergente et tend vers $0$ quand $n$ tend vers l'infini.

## Exercice 13

1. Montrer que la suite $\left(\sin\left(n\frac\pi 2\right)\right)_n$ n'a pas de limite.
2. On considère la suite de terme général $u_n = \sin\frac{n\pi}2+\frac1n$. Construire trois suites extraites de $(u_n)_n$ : une qui converge vers $0$, une qui converge vers $1$ et une qui converge vers $−1$.

??? success "Réponse 1."
    - Pour $n$ pair, on a $\sin\frac{n\pi}2 = 0$, il existe donc une suite extraite convergente.
    - Pour $n$ impair, on a $\sin\frac{n\pi}2 = \pm 1$.
    
    Ce qui assure que cette suite n'a pas de limite.

??? success "Réponse 2."
    1. On a $u_{2n} = 0+\frac1n$, de sorte que la suite extraite $(u_{2n})_n$ converge vers $0$.
    2. On a $u_{4n+1} = 1+\frac1n$, de sorte que la suite extraite $(u_{4n+1})_n$ converge vers $1$.
    3. On a $u_{4n+3} = -1+\frac1n$, de sorte que la suite extraite $(u_{4n+3})_n$ converge vers $-1$.

## Exercice 14

Soit $q$ un entier au moins égal à $2$.
Pour tout $n \in \mathbb N$, on pose $u_n = \cos\frac{2n\pi}q$

1. Montrer que $u_{n+q} = u_n$ pour tout $n$.
2. Calculer $u_{nq}$ et $u_{nq+1}$. En déduire que $(u_n)_n$ n'a pas de limite.

??? success "Réponse"
    Soit $n\in\mathbb N$, on a :

    $$\begin{align*}
    u_{n+q} &= \cos\left(\frac{2(n+q)\pi}q\right)\\
    u_{n+q} &= \cos\left(\frac{2n\pi}q + 2\pi\right)\\
    u_{n+q} &= \cos\left(\frac{2n\pi}q\right)\\
    u_{n+q} &= u_n\\
    \end{align*}$$

    On démontre aisément, par récurrence, que $u_{nq} = u_0 = 1$

    D'autre part, on a de même : $u_{nq+1} = u_1 = \cos \frac{2\pi}q$, or $q$ est un entier au moins égal à $2$.

    - Pour $q=2$, on a $u_1 = \cos \frac{2\pi}2 = -1 \neq 1$.
    - Pour $q=3$, on a $u_1 = \cos \frac{2\pi}3 = -\frac12 \neq 1$.
    - Pour $q\geqslant 4$, on a $\frac{2\pi}q \in ]0 \,;\, \frac\pi 2]$, donc $u_1 = \cos \frac{2\pi}q$, avec $u_1 \in [0\,;\,1[$ donc $u_1\neq 1$

    On en déduit que les suites extraites $(u_{nq})_n$ et $(u_{nq+1})_n$ sont toutes deux convergentes, mais avec des limites distinctes, ce qui assure que $(u_n)_n$ est divergente.
