# Fonction dérivée

## Formules en résumé

==Le résumé est à relire souvent et à connaitre par cœur==

### Fonctions usuelles

- $m, p \in\mathbb R$ désignent des constantes.
- $n\in\mathbb N^*$ désigne une constante.


| Intervalle pour $f$ | Fonction $f$ définie par | Intervalle pour $f'$ |Dérivée $f'$ définie par|
|:-----------|:-------------------------|:-----------|:-----------------------|
|$\mathbb R$ | $f(x) = p$       |$\mathbb R$ | $f'(x) = 0$   |
|$\mathbb R$ | $f(x) = mx + p$  |$\mathbb R$ | $f'(x) = m$   |
|$\mathbb R$ | $f(x) = x^n$     |$\mathbb R$ | $f'(x) = nx^{n-1}$ |
|$\mathbb R^*$ | $f(x) = \dfrac 1 {x^n}$    |$\mathbb R^*$ | $f'(x) = \dfrac {-n} {x^{n+1}}$ |
|$\mathbb R_+$ | $f(x) = \sqrt{x}$       |$\mathbb R_{+}^{*}$ | $f'(x) = \dfrac 1{2\sqrt x}$ |

### Opérations

- $k, m, p$ sont des constantes réelles,
- $u, v$ sont des fonctions dérivables

| Opération | Fonction à dériver | Fonction dérivée |
|:----------|:------------------:|:----------------:|
|Produit constant | $k×u$ | $(k×u)' = k×u'$ |
|Somme   | $u+v$ | $(u+v)' = u'+v'$ |
|Produit | $u×v$ | $(u×v)' = u'×v + u×v'$|
|Inverse  | $\dfrac 1 v$ | $\left(\dfrac 1 v\right)' = \dfrac{-v'}       {v^2}$|
|Quotient | $\dfrac u v$ | $\left(\dfrac u v\right)' = \dfrac{u'×v -u×v'}{v^2}$|
|Composée d'affine | $f(x) = g(mx+p)$ | $f'(x) = m×g'(mx+p)$ |

### Fonctions classiques

| Intervalle pour $f$ | Fonction $f$ définie par | Intervalle pour $f'$ |Dérivée $f'$ définie par|
|:-----------|:-------------------------|:-----------|:-----------------------|
|$\mathbb R$ | $f(x) = \exp(x)$         |$\mathbb R$ | $f'(x) = \exp(x)$      |
|$\mathbb R$ | $f(x) = \sin(x)$         |$\mathbb R$ | $f'(x) = \cos(x)$      |
|$\mathbb R$ | $f(x) = \cos(x)$         |$\mathbb R$ | $f'(x) = -\sin(x)$     |
|$\mathbb R\setminus \{\frac{\pi}2 +k\pi, k\in\mathbb Z\}$ | $f(x) = \tan(x)$         |$\mathbb R$ | $f'(x) = 1+\tan^2(x)$  |


## Exercices simples

### Fonction constante

??? question "Calculer $f'(x)$ pour $f(x) = 2023$ sur $\mathbb R$"
    Comme pour toute fonction constante, on a $f'(x) = 0$

### Fonction affine

??? question "Calculer $g'(x)$ pour $g(x) = 5x-2$ sur $\mathbb R$"
    $g$ est affine donc $g'$ est constante. On a $g'(x) = 5$

### Monôme de degré $n$

??? question "Calculer $f'(x)$ pour $f(x) = x^4$ sur $\mathbb R$"
    On a $f'(x) = 4x^3$.

??? question "Calculer $g'(x)$ pour $g(x) = 7x^4$ sur $\mathbb R$"
    On a $g'(x) = 7×4x^3 = 28x^3$

### Polynômes

??? question "Calculer $f'(x)$ pour $f(x) = 5x^2 -3x + 1$ sur $\mathbb R$"
    On a $f'(x) = 10x -3$

??? question "Calculer $g'(x)$ pour $g(x) = x^3 +4x^2 +7x -6$ sur $\mathbb R$"
    On a $f'(x) = 3x^2 +8x +7$

### Produit de polynômes

??? question "Calculer $f'(x)$ pour $f(x) = (5x-1)(x+3)$ sur $\mathbb R$"
    On pose

    $$\begin{align*}
    u(x) &= 5x-1 & u'(x) &= 5\\
    v(x) &= x+3 & v'(x) &= 1\\
    \end{align*}$$

    On a donc

    $$\begin{align*}
    f'(x) &= (u(x)×v(x))'\\
    f'(x) &= u'(x)×v(x) + u(x)×v'(x)\\
    f'(x) &= 5(x+3) + (5x-1)×1\\
    f'(x) &= 10x + 14\\
    \end{align*}$$

??? question "Calculer $g'(x)$ pour $g(x) = (x^2-x+2)(2x^3-4)$ sur $\mathbb R$"
    On pose

    $$\begin{align*}
    u(x) &= x^2-x+2 & u'(x) &= 2x+1\\
    v(x) &= 2x^3-4 & v'(x) &= 6x^2\\
    \end{align*}$$

    On a donc

    $$\begin{align*}
    g'(x) &= (2x+1)×(2x^3-4) + (x^2-x+2)×6x^2\\
    g'(x) &= (4x^4-8x+2x^3-4) + (6x^4 - 6x^3 +12x^2)\\
    g'(x) &= 10x^4 -4x^3 +12x^2 -8x -4\\
    \end{align*}$$

### Produit de fonctions usuelles

**Objectif** : donner l'ensemble de dérivabilité de chaque fonction, puis calculer sa dérivée.

??? question "1. Avec $f(x) = \sqrt x(x+1)$"
    $f$ est le produit de deux fonctions,
    
    - l'une est dérivable sur $\mathbb R_{+}^{*}$
    - et l'autre sur $\mathbb R$,

    la fonction $f$ est donc dérivable sur l'intersection $\mathbb R_{+}^{*}$.

    On pose

    $$\begin{align*}
    u(x) &= \sqrt x & u'(x) &= \dfrac1{2\sqrt x}\\
    v(x) &= x+1 & v'(x) &= 1\\
    \end{align*}$$

    On obtient

    $$\begin{align*}
    f'(x) &= \dfrac1{2\sqrt x} × (x+1) + \sqrt x ×1\\
    f'(x) &= \dfrac1{2\sqrt x} × (x+1) + \dfrac{\sqrt x×\left(2\sqrt x\right)}{2\sqrt x}\\
    f'(x) &= \dfrac{(x+1) + 2x}{2\sqrt x}\\
    f'(x) &= \dfrac{3x + 1}{2\sqrt x}\\
    \end{align*}$$

??? question "2. Avec $g(x) = \sqrt x(x^2-x+1)$"
    $f$ est le produit de deux fonctions, l'une est dérivable sur $\mathbb R_{+}^{*}$ et l'autre sur $\mathbb R$,  
    la fonction $f$ est donc dérivable sur l'intersection $\mathbb R_{+}^{*}$.

    On pose

    $$\begin{align*}
    u(x) &= \sqrt x & u'(x) &= \dfrac1{2\sqrt x}\\
    v(x) &= x^2-x+1 & v'(x) &= 2x-1\\
    \end{align*}$$

    On obtient

    $$\begin{align*}
    g'(x) &= \dfrac1{2\sqrt x} × (x^2-x+1) + \sqrt x ×(2x-1)\\
    g'(x) &= \dfrac{(x^2-x+1) + 2x(2x-1)}{2\sqrt x}\\
    g'(x) &= \dfrac{5x^2 -3x + 1}{2\sqrt x}\\
    \end{align*}$$

    Si on factorise le numérateur on peut en étudier le signe.



### Quotient, fonctions usuelles

??? question "Calculer $f'(x)$ pour $f(x) = \dfrac14x^4 -\dfrac13x^3 +\dfrac12x^2 -10$ sur $\mathbb R$"
    On a $f'(x) = x^3 - x^2 + x$

??? question "Calculer $g'(x)$ pour $g(x) = \sqrt x + \dfrac1x$ sur $\mathbb R_{+}^*$"
    Il est possible de mettre au même dénominateur... On a
    
    $$\begin{align*}
    f'(x) &= \dfrac1{2\sqrt x} + \dfrac{-1}{x^2}\\
    f'(x) &= \dfrac{1×\left(x\sqrt x\right)}{2\sqrt x×\left(x\sqrt x\right)} + \dfrac{-2}{2x^2}\\
    f'(x) &= \dfrac{x\sqrt x -2}{2x^2}\\
    \end{align*}$$

    C'est mieux pour, ensuite, en étudier le signe.


### Composée d'affine

1. Identifier chaque fonction sous la forme $f(x) = g(ax+b)$
2. Préciser l'ensemble de dérivabilité
3. Calculer $f'(x)$


??? question "1. Avec $f(x) = (5x+3)^2$"
    1. Avec $g(t) = t^2$ et $ax+b = 5x+3$, on a $f(x) = g(ax+b)$
    2. $g$ est dérivable sur $\mathbb R$, donc $f$ est aussi dérivable sur $\mathbb R$.
    3. $f'(x) = a×g'(ax+b)$, on déduit avec $a=5$, $b=3$

    $$\begin{align*}
    g'(t) &= 2t\\
    f'(x) &= 5×2×(5x+3)\\
    f'(x) &= 50x+30\\
    \end{align*}$$

??? question "2. Avec $f(x) = \sqrt{3x-4}$"
    1. Avec $g(t) = \sqrt t$ et $ax+b = 3x-4$, on a $f(x) = g(ax+b)$
    2. $g$ est dérivable sur $\mathbb R_{+}^{*}$, donc $f$ est aussi dérivable sur l'ensemble des solutions
    de l'inéquation $3x-4>0$, soit $\left]\dfrac43\,;\,+\infty\right[$ ; il n'y a pas d'erreur $\dfrac43$ est bien inclus dans l'ensemble de définition de $f$, mais exclu pour $f'$
    3. $f'(x) = a×g'(ax+b)$, on déduit avec $a=3$, $b=-4$
    
    $$\begin{align*}
    g'(t) &= \dfrac1{2\sqrt t}\\
    f'(x) &= 3×\dfrac1{2\sqrt {3x-4}}\\
    f'(x) &= \dfrac3{2\sqrt {3x-4}}\\
    \end{align*}$$

??? question "3. Avec $f(x) = \left(\dfrac12 x - 1\right)^3$"
    1. Avec $g(t) = t^3$ et $ax+b = \dfrac12 x - 1$, on a $f(x) = g(ax+b)$
    2. $g$ est dérivable sur $\mathbb R$, donc $f$ est aussi dérivable sur $\mathbb R$.
    3. $f'(x) = a×g'(ax+b)$, on déduit avec $a=\dfrac12$, $b=-1$
    
    $$\begin{align*}
    g'(t) &= 3t^2\\
    f'(x) &= \dfrac12×3×\left(\dfrac12 x - 1\right)^2\\
    f'(x) &= \dfrac32×\left(\dfrac12 x - 1\right)^2\\
    \end{align*}$$
