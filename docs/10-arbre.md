# Arbre Binaire

!!! info "Prérequis, informations"
    Ce cours suppose d'avoir abordé la notion de récursivité ainsi que les structures linéaires telles que pile, file et liste chainées. Il propose certains exercices en Python, mais la plupart peuvent être écrits sous forme de pseudo-code.

    Un arbre binaire est une structure de donnée qui ne doit pas être confondue avec celle d'arbre enraciné, même d'arité 2 ; deux concepts distincts. Nous suivons les définitions que l'on rencontre dans différents cours universitaires, ainsi que sur OEIS.

    Ce cours ne porte que sur les **arbres binaires**, et non sur les arbres enracinés. Quand on parlera de sous-arbres, ce sera toujours un arbre binaire.
    
    Nous dessinerons toujours les arbres binaires avec les _nil_, et chaque nœud possède **exactement deux** sous-arbres, éventuellement vides. La hauteur de l'arbre binaire vide est $0$.


!!! example "Un exemple d'arbre binaire"
    ```mermaid
    graph TD
        15 --> 22
        15 --> 8
        22 --> 22g(( ))
        22 --> 22d(( ))
        8 --> 36
        8 --> 8d(( ))
        36 --> 36g(( ))
        36 --> 36d(( ))
    ```

## Définitions

Un **arbre binaire** est une structure de données de nature récursive. C'est un ensemble fini de nœuds agencés de manière hiérarchique, où :

- chaque nœud pointe vers deux sous-arbres, un **à gauche**, un **à droite** ;
- chaque nœud peut porter une **valeur** ;
- **un arbre binaire peut être vide**.

!!! quote "Définition récursive"
    * Un arbre binaire peut avoir zéro nœud, il est alors vide, on le note `nil`.
    * Un arbre binaire non vide possède un nœud particulier, sa racine, et qui pointe vers deux sous arbres, un **à gauche** et un **à droite**. Chaque nœud peut porter une **valeur**.

!!! quote "Taille d'un arbre binaire"

    La taille d'un arbre binaire est son nombre de nœuds.


!!! quote "Hauteur d'un arbre binaire"

    * La hauteur d'un arbre binaire **vide** est nulle.
    * La hauteur d'un arbre binaire **non vide** (qui possède donc une racine et deux sous arbres) est le nombre maximal de liens pour joindre la racine à un `nil`.


    ??? info "Avec les arbres enracinés"
        Dans certains ouvrages, on trouve une **autre définition** qui est celle des arbres enracinés :
        
        - un arbre enraciné minimaliste (réduit à un nœud) est de hauteur $0$ ;
        - de manière générale, la hauteur d'un arbre enraciné est le nombre maximal de liens de la racine vers une feuille.

        - un arbre enraciné vide n'existe pas, mais parfois on le note avec une hauteur de $-1$, parfois ne possède pas de hauteur. Un arbre enraciné vide ne devrait pas exister !

        Certains cours mélangent arbres enracinés et arbres binaires, ce qui conduit à certaines contorsions.

        Concrètement, on enlève $1$ par rapport à la définition de la hauteur pour les arbres binaires.

        !!! tip "Quelle importance ?"
            La hauteur $h$ est une notion souvent importante pour la complexité d'un algorithme sur les arbres binaires. De nombreux algorithmes ont une complexité en $\mathcal O(h)$, et ainsi, un décalage de $1$ dans la définition est absolument **sans incidence**.

!!! example "Exemple"
    L'arbre binaire ci-dessous possède **4 nœuds**. On a représenté les `nil` par des disques vides. Ici, chaque nœud porte une valeur : un prénom.

    ```mermaid
    graph TD
        Z([Zoé]) --> R([Régis])
        Z --> U([Annie])
        R --> Q(( ))
        R --> S([Serge])
        U --> B(( ))
        U --> N(( ))
        S --> Sa(( ))
        S --> Sb(( ))
    ```

    - La taille de cet arbre binaire est $4$.
    - La hauteur de cet arbre binaire est $3$.

## Exercices débranchés

!!! question "Arbres à moins de 3 nœuds"
    Dessiner tous les petits arbres binaires ; moins de 3 nœuds et qui ont la même valeur `Truc`.

    ??? success "Réponse"
        !!! inline example "Un arbre à deux nœuds"
            Un des deux modèles.

            ```mermaid
            graph TD
                r([Truc])
                rg([Truc])
                rd(( ))
                rgg(( ))
                rgd(( ))
                r --> rg
                r --> rd
                rg --> rgg
                rg --> rgd
            ```

        !!! inline example "L'autre arbre à deux nœuds"
            Un des deux modèles.

            ```mermaid
            graph TD
                r([Truc])
                rg(( ))
                rd([Truc])
                rdg(( ))
                rdd(( ))
                r --> rg
                r --> rd
                rd --> rdg
                rd --> rdd
            ```

        !!! inline example "L'arbre vide"
            Il n'y en a qu'un modèle.

            ```mermaid
            graph TD
                0(( ))
            ```

        !!! inline example "L'arbre à un nœud"
            Il n'y en a qu'un modèle.

            ```mermaid
            graph TD
                r([Truc])
                rg(( ))
                rd(( ))
                r --> rg
                r --> rd
            ```


!!! question "Arbres à 3 nœuds"
    Dessiner tous les arbres binaires dont les 3 nœuds ont la même valeur `Truc`.

    ??? success "Réponse"
        Il y en a 5.

        !!! inline example "Cas 1"
            ```mermaid
            graph TD
                0([Truc]) --> 0g([Truc])
                0 --> 0d(( ))
                0g --> 0gg([Truc])
                0g --> 0gd(( ))
                0gg --> 0ggg(( ))
                0gg --> 0ggd(( ))
            ```

        !!! inline example "Cas 2"
            ```mermaid
            graph TD
                0([Truc]) --> 0g([Truc])
                0 --> 0d(( ))
                0g --> 0gg(( ))
                0g --> 0gd([Truc])
                0gd --> 0gdg(( ))
                0gd --> 0gdd(( ))
            ```

        !!! inline example "Cas 3"
            ```mermaid
            graph TD
                0([Truc]) --> 0g(( ))
                0 --> 0d([Truc])
                0d --> 0dg([Truc])
                0d --> 0dd(( ))
                0dg --> 0dgg(( ))
                0dg --> 0dgd(( ))
            ```

        !!! inline example "Cas 4"
            ```mermaid
            graph TD
                0([Truc]) --> 0g(( ))
                0 --> 0d([Truc])
                0d --> 0dg(( ))
                0d --> 0dd([Truc])
                0dd --> 0ddg(( ))
                0dd --> 0ddd(( ))
            ```

        !!! inline example "Cas 5"
            ```mermaid
            graph TD
                0([Truc]) --> 0g([Truc])
                0 --> 0d([Truc])
                0g --> 0gg(( ))
                0g --> 0gd(( ))
                0d --> 0dg(( ))
                0d --> 0dd(( ))
            ```

!!! question ":boom: Arbres à 4 nœuds :boom:"
    Dessiner à la main tous les arbres binaires ayant quatre nœuds et la même valeur qu'il sera inutile de préciser.

    ??? success "Réponse"
        * Il y a $1$ arbre binaire ayant $0$ nœud, l'arbre vide.
        * Il y a $1$ arbre binaire ayant $1$ nœud.
        * Il y a $2$ arbres binaires ayant $2$ nœuds.
        * Il y a $5$ arbres binaires ayant $3$ nœuds.
        * Il y a $14$ arbres binaires ayant $4$ nœuds.

        Les 14 arbres binaires de taille 4, peuvent être répartis en fonction de la taille des sous arbres binaires.
        
        Il y a un nœud racine et 3 autres nœuds à répartir à gauche ($t_g$) et à droite ($t_d$). On a :
        
        $$(1 + t_g + t_d = 4)$$ 

        $(t_g=0, t_d=3)$

        ![](images/example0.svg)
        ![](images/example1.svg)
        ![](images/example2.svg)
        ![](images/example3.svg)
        ![](images/example4.svg)

        ---

        $(t_g=1, t_d=2)$

        ![](images/example5.svg)
        ![](images/example6.svg)

        ---

        $(t_g=2, t_d=1)$

        ![](images/example7.svg)
        ![](images/example8.svg)

        ---

        $(t_g=3, t_d=0)$

        ![](images/example9.svg)
        ![](images/example10.svg)
        ![](images/example11.svg)
        ![](images/example12.svg)
        ![](images/example13.svg)


!!! question ":boom: :boom: Dénombrement des arbres binaires :boom: :boom:"
    Combien y a-t-il d'arbres binaires ayant $5$ nœuds qui ont la même valeur ?
    
    *(Ne pas tous les dessiner ; trouver juste le moyen de les dénombrer.)*

    ??? success "Réponse"
        Pour un arbre binaire à $5$ nœuds, il y a un nœud racine et 4 autres nœuds à répartir à gauche ($t_g$) et à droite ($t_d$). On a :
        
        $$(1 + t_g + t_d = 5)$$ 

        Les différentes possibilités sont :

        - $(t_g=0, t_d=4)$, il y a $1×14=14$ arbres binaires de ce type.
        - $(t_g=1, t_d=3)$, il y a $1×5=5$ arbres binaires de ce type.
        - $(t_g=2, t_d=2)$, il y a $2×2=4$ arbres binaires de ce type.
        - $(t_g=3, t_d=1)$, il y a $5×1=5$ arbres binaires de ce type.
        - $(t_g=4, t_d=0)$, il y a $14×1=14$ arbres binaires de ce type.
        
        Ce qui donne un total de $14+5+4+5+14 = 42$ arbres binaires de taille 5.


## Exercices en Python

Pour les exercices suivants en Python, on suppose qu'un objet `ab` instance de la classe `ArbreBinaire` (que nous allons bientôt créer) possède les attributs suivants :

- `ab.est_vide()` : renvoie un booléen qui indique si `ab` est vide, ou non.
- si `ab` est **non vide** :
    - `ab.gauche` : le sous-arbre binaire à gauche de `ab` non vide.
    - `ab.droite` : le sous-arbre binaire à droite de `ab` non vide.
    - `ab.valeur` : la valeur portée à la racine de `ab` non vide.

On pourra aussi réaliser ces exercices en donnant un pseudo-code.

!!! tip "Classe `ArbreBinaire` minimaliste"
    ```python
    class ArbreBinaire:
        def __init__(self, gauche=None, valeur=None, droite=None):
            self.gauche = gauche
            self.valeur = valeur
            self.droite = droite
        
        def est_vide(self):
            return (self.valeur is None)
    ```

### Taille

!!! question "Exercice"
    Écrire une fonction `taille` qui renvoie la taille d'un arbre binaire passé en paramètre.

    On pourra compléter le code suivant :

    !!! tip "Tests"

        ```python
        class ArbreBinaire:
            def __init__(self, gauche=None, valeur=None, droite=None):
                self.gauche = gauche
                self.valeur = valeur
                self.droite = droite

            def est_vide(self):
                return self.valeur is None

        # À compléter ici
        def taille(ab):
            if ab.est_vide():
                return ...
            else:
                return 1 + ...(ab.gauche) + ...


        # tests
        serge = ArbreBinaire(
            ArbreBinaire(),
            "Serge",
            ArbreBinaire()
        )
        regis = ArbreBinaire(
            ArbreBinaire(),
            "Régis",
            serge
        )
        annie = ArbreBinaire(
            ArbreBinaire(),
            "Annie",
            ArbreBinaire()
        )
        zoe = ArbreBinaire(
            regis,
            "Zoé",
            annie
        )

        assert taille(ArbreBinaire()) == 0
        assert taille(zoe) == 4
        ```

    ??? success "Réponse"

        ```python
        def taille(ab: ArbreBinaire) -> int:
            if ab.est_vide():
                return 0
            else:
                return 1 + taille(ab.gauche) + taille(ab.droite)
        ```

### Hauteur

!!! question "Exercice"
    Écrire une fonction `hauteur` qui renvoie la hauteur d'un arbre binaire passé en paramètre.

    !!! tip "Tests"

        ```python
        # en complétant l'exercice précédent
        def hauteur(ab: ArbreBinaire) -> int:
            ...
        
        assert hauteur(ArbreBinaire()) == 0
        assert hauteur(zoe) == 3
        ```

    ??? success "Réponse"

        ```python
        def hauteur(ab: ArbreBinaire) -> int:
            if ab.est_vide():
                return 0
            else:
                return 1 + max(hauteur(ab.gauche), hauteur(ab.droite))
        ```

### Feuille

!!! quote "Définition"
    Une **feuille** est un arbre binaire qui ne possède qu'un seul nœud.

!!! question "Exercice"
    Écrire une fonction `est_feuille` qui renvoie un booléen qui détermine si un arbre binaire passé en paramètre est une feuille, ou non.

    > La fonction n'utilisera pas la fonction `taille`.

    ??? tip "Tests"

        ```python
        # en complétant l'exercice précédent
        def est_feuille(ab: ArbreBinaire) -> bool:
            ...
        
        assert est_feuille(ArbreBinaire()) == False
        assert est_feuille(zoe) == False
        assert est_feuille(regis) == False
        assert est_feuille(annie) == True
        assert est_feuille(serge) == True
        ```


    ??? success "Réponse"

        ```python
        def est_feuille(ab: ArbreBinaire) -> bool:
            if ab.est_vide():
                return False
            elif not ab.gauche.est_vide():
                return False
            elif not ab.droite.est_vide():
                return False
            else:
                return True
        ```

        Variante en une ligne

        ```python
        def est_feuille(ab: ArbreBinaire) -> bool:
            return (not ab.est_vide()) and ab.gauche.est_vide() and ab.droite.est_vide()
        ```


### Arbre dégénéré

!!! quote "Définition"
    Un arbre binaire est **dégénéré** si pour chaque nœud, au moins un sous-arbre est vide.

!!! info "Cas extrême"
    Un arbre dégénéré montre une situation extrême où certains algorithmes sur les arbres que nous verrons ne seront pas efficaces. Avec un arbre dégénéré, la hauteur est égale à la taille ; on ne peut pas obtenir une plus grande hauteur à taille fixée.

!!! example "Exemple, un peigne à gauche est dégénéré"

    ```mermaid
    graph TD
        11
        11g(( ))
        7
        5
        7d(( ))
        3
        5g(( ))
        2
        3d(( ))
        2g(( ))
        2d(( ))

        11 --> 11g
        11 --> 7
        5 --> 3
        3 --> 3d
        3 --> 2
        7 --> 5
        7 --> 7d
        5 --> 5g
        2 --> 2g
        2 --> 2d
    ```


!!! question "Exercice"
    Écrire une fonction `est_degenere` qui renvoie un booléen qui détermine si un arbre binaire passé en paramètre est dégénéré, ou non.


    ??? success "Réponse 1"

        ```python
        def est_degenere(ab: ArbreBinaire) -> bool:
            return taille(ab) == hauteur(ab)
        ```

        Avec cette méthode, on parcourt deux fois l'arbre, c'est acceptable, mais pas idéal.

    ??? success "Réponse 2"

        ```python
        def est_degenere(ab: ArbreBinaire) -> bool:
            if ab.est_vide():
                return True
            elif ab.droite.est_vide():
                return est_degenere(ab.gauche)
            elif ab.gauche.est_vide():
                return est_degenere(ab.droite)
            else:
                return False
        ```

        Avec cette méthode, on teste `est_vide` deux fois sur plusieurs nœuds, mais pas tous. C'est mieux.

    ??? success "Réponse 3"

        ```python
        def est_degenere(ab: ArbreBinaire) -> bool:
            "ab peut être vide"
            if ab.est_vide():
                return True
            elif ab.droite.est_vide():
                return est_degenere(ab.gauche)
            elif ab.gauche.est_vide():
                return est_degenere_bis(ab.droite)
            else:
                return False

        def est_degenere_bis(ab: ArbreBinaire) -> bool:
            "ab n'est pas vide"
            if ab.droite.est_vide():
                return est_degenere(ab.gauche)
            elif ab.gauche.est_vide():
                return est_degenere_bis(ab.droite)
            else:
                return False
        ```

        Avec cette méthode, on teste `est_vide` une seule fois chaque nœud, c'est idéal. Il s'agit de deux fonctions mutuellement récursives.

        On remarque que le code 3 est **bien** plus complexe que le code 1. Que choisirait une entreprise ? Un code en une ligne simple ou un code plus complexe deux fois plus efficace ? **Le code 1 !**

        Le code 3 garde un intérêt pédagogique !

    ??? success "Réponse 4"

        ```python
        def est_degenere(ab: ArbreBinaire, non_teste=True) -> bool:
            """
            non_teste=True -> peut-être vide ou non...
            non_teste=False -> certainement non vide
            """
            if non_teste and ab.est_vide():
                return True
            elif ab.droite.est_vide():
                return est_degenere(ab.gauche, True)
            elif ab.gauche.est_vide():
                return est_degenere(ab.droite, False)
            else:
                return False
        ```

        Le code 4 utilise un paramètre par défaut qui factorise les deux fonctions du code 3.

### Arbre binaire parfait

!!! quote "Définition"
    Un **arbre binaire parfait** possède une taille maximale pour une hauteur donnée.

!!! info "Caractéristiques"
    - Aucun nœud ne possède un seul sous-arbre vide.
    - Les feuilles sont au même niveau de profondeur dans l'arbre binaire.
    - Les _nil_ sont au même niveau de profondeur dans l'arbre binaire.
    - Il y a $2^{\text{hauteur}}-1$ nœuds dans cet arbre binaire.

!!! example "Exemple"

    ```mermaid
    graph TD
        1 --> 2
        1 --> 3
        2 --> 4
        2 --> 5
        3 --> 6
        3 --> 7
        4 --> 4a(( ))
        4 --> 4b(( ))
        5 --> 5a(( ))
        5 --> 5b(( ))
        6 --> 6a(( ))
        6 --> 6b(( ))
        7 --> 7a(( ))
        7 --> 7b(( ))
    ```

    - Il y a $2^3-1=7$ nœuds dans cet arbre binaire parfait de hauteur $3$.

!!! question "Exercice"
    Écrire une fonction `est_parfait` qui renvoie un booléen qui détermine si un arbre binaire passé en paramètre est parfait, ou non.

    Trois réponses sont proposées :

    1. avec une fonction utilisant `taille` et `hauteur` ;
    2. avec une fonction récursive peu efficace ;
    3. avec une fonction récursive efficace.

    ??? success "Réponse 1"

        ```python
        def est_parfait(ab: ArbreBinaire) -> bool:
            return 2 ** hauteur(ab) - 1 == taille(ab)
        ```

        Cette solution simple effectue deux parcours de l'arbre. C'est acceptable.

    ??? success "Réponse 2"

        ```python
        def est_parfait(ab: ArbreBinaire) -> bool:
            if ab.est_vide():
                return True
            else:
                return (
                    hauteur(ab.gauche) == hauteur(ab.droite)
                    and est_parfait(ab.gauche)
                    and est_parfait(ab.droite)
                )
        ```

        Cette solution effectue un nombre très important de fois le parcours de l'arbre. Ce n'est pas acceptable du tout ! En guise d'exercice, essayez de trouver les premiers termes de la suite `nb_appels_est_vide(h)` en fonction de `h` la hauteur d'un arbre binaire parfait passé en paramètre.

    ??? success "Réponse 3"

        Il est possible de créer une fonction récursive `hauteur_et_perfection` qui renvoie le couple `(hauteur(ab), est_parfait(ab)` pour `ab` passé en paramètre, et ce avec un seul parcours de l'arbre.

        On utilise alors un _wrapper_ comme réponse.

        ```python
        def est_parfait(ab: ArbreBinaire) -> bool:
            h, parfait = hauteur_et_perfection(ab)
            return parfait
        
        def hauteur_et_perfection(ab: ArbreBinaire) -> Tuple[int, bool]:
            if ab.est_vide():
                return (0, True)
            else:
                g_h, g_parfait = hauteur_et_perfection(ab.gauche)
                d_h, d_parfait = hauteur_et_perfection(ab.droite)
                return (max(g_h, d_h), (g_h == d_h) and g_parfait and d_parfait)
        ```

### :boom: :boom: Arbre équilibré :boom: :boom:

!!! quote "Définition"
    Un arbre binaire est **équilibré** si pour chaque nœud, l'écart de hauteur entre son sous-arbre à gauche et son sous-arbre à droite est $1$ au plus.

!!! question "Exercice"
    Écrire une fonction `est_equilibre` qui renvoie un booléen qui détermine si un arbre binaire passé en paramètre est équilibré, ou non.

    On s'inspirera de la réponse 3 de l'exercice précédent.

    TODO -> e-nsi/pratique



### Somme des valeurs

On suppose pour cet exercice que les instances `ab` de type `ArbreBinaire` possèdent un attribut `ab.valeur` qui est **numérique**, ce qui permet de faire plus d'opérations, comme la somme. On dira que `ab` est un arbre binaire numérique.

!!! question "Exercice"
    Écrire une fonction `somme` qui renvoie la somme des valeurs d'un arbre binaire numérique passé en paramètre.

    ??? success "Réponse"

        ```python
        # Numerique désigne ici : int, float, ou un autre type numérique

        def somme(ab: ArbreBinaire) -> Numerique:
            if ab.est_vide():
                return 0
            else:
                return somme(ab.gauche) + ab.valeur + somme(ab.droite)
        ```

### Maximum d'un arbre binaire non vide

On suppose pour cet exercice que les instances `ab` de type `ArbreBinaire` possèdent un attribut `ab.valeur` qui est **comparable** aux autres, par exemple des chaines de caractères avec l'ordre lexicographique, ou bien des entiers, ou bien des décimaux.

!!! question "Exercice"
    Écrire une fonction `maximum` qui renvoie la valeur maximale d'un arbre binaire **non vide** passé en paramètre. (On ne demande pas de gérer le cas vide !)

    ??? success "Réponse"

        ```python
        # Comparable désigne ici : int, float, str
        #   ou un autre type de valeurs comparables entre elles

        def maximum(ab: ArbreBinaire) -> Comparable:
            "ab est garanti non vide"
            if ab.droite.est_vide():
                if ab.gauche.est_vide():
                    return ab.valeur
                else:
                    # ab.gauche est non vide
                    return max(maximum(ab.gauche), ab.valeur)
            elif ab.gauche.est_vide():
                # ab.droite est non vide
                return max(ab.valeur, maximum(ab.droite))
            else:
                # ab.gauche est non vide
                # ab.droite est non vide
                return max(
                    max(maximum(ab.gauche), ab.valeur),
                    maximum(ab.droite)
                )
        ```


### Test d'égalité

!!! question "Exercice"
    Écrire une fonction `sont_egaux` qui renvoie un booléen qui détermine si deux arbres binaires `ab_x` et `ab_y` passés en paramètre ont les mêmes valeurs à la même place, ou non.


    ??? success "Réponse"

        ```python
        def sont_egaux(ab_x: ArbreBinaire, ab_y: ArbreBinaire) -> bool:
            if ab_x.est_vide():
                return ab_y.est_vide()
            elif ab_x.valeur != ab_y.valeur:
                return False
            else:
                return (
                        sont_egaux(ab_x.gauche, ab_y.gauche)
                    and sont_egaux(ab_x.droite, ab_y.droite)
                )
        ```


### Test du miroir

!!! question "Exercice"
    Écrire une fonction `sont_miroir` qui renvoie un booléen qui détermine si deux arbres binaires passés en paramètre ont les mêmes valeurs à la place **en miroir** l'un de l'autre, ou non.

    ??? success "Réponse"

        ```python
        def sont_miroir(ab_x: ArbreBinaire, ab_y: ArbreBinaire) -> bool:
            if ab_x.est_vide():
                return ab_y.est_vide()
            elif ab_x.valeur != ab_y.valeur:
                return False
            else:
                return (
                        sont_miroir(ab_x.gauche, ab_y.droite)
                    and sont_miroir(ab_x.droite, ab_y.gauche)
                )
        ```


### Test d'inclusion

!!! question "Exercice"
    Écrire une fonction `est_inclus` qui renvoie un booléen qui détermine si, pour deux arbres binaires `sab` et `ab` passés en paramètre, `sab` est un sous-arbre de `ab`, ou non.

    ??? success "Réponse"

        ```python
        def est_inclus(sab: ArbreBinaire, ab: ArbreBinaire) -> bool:
            if sab.est_vide():
                return True
            elif ab.est_vide():
                return False
            else:
                return (
                    sont_egaux(sab, ab)
                    or est_inclus(sab, ab.gauche)
                    or est_inclus(sab, ab.droite)
                )
        ```


### Construction d'arbre

Pour cet exercice et les suivants, on suppose qu'on a accès à un constructeur d'arbre binaire qui s'utilise ainsi, de deux manières possibles :

- `ArbreBinaire()` renvoie un arbre binaire vide.
- `ArbreBinaire(gauche, valeur, droite)` renvoie un arbre binaire avec une racine portant `valeur` et ayant un sous-arbre à `gauche` ainsi qu'un sous-arbre à `droite`.

!!! question "Exercice"
    Construire l'arbre suivant :

    ```mermaid
    graph TD
        Z([Zoé]) --> R([Régis])
        Z --> U([Annie])
        R --> Q(( ))
        R --> S([Serge])
        U --> B(( ))
        U --> N(( ))
        S --> Sa(( ))
        S --> Sb(( ))
    ```

    On pourra compléter le code suivant :

    ```python
    serge = ArbreBinaire(ArbreBinaire(), "Serge", ArbreBinaire())
    regis = ArbreBinaire(ArbreBinaire(), "Serge", serge)
    ...
    ```

    ??? success "Réponse"

        ```python
        serge = ArbreBinaire(ArbreBinaire(), "Serge", ArbreBinaire())
        regis = ArbreBinaire(ArbreBinaire(), "Serge", serge)

        annie = ArbreBinaire(ArbreBinaire(), "Annie", ArbreBinaire())
        zoe = ArbreBinaire(regis, "Zoé", annie)
        ```

### Copie d'arbre binaire

!!! question "exercice"
    Écrire une fonction `copie` qui renvoie une copie de l'arbre binaire passé en paramètre.

    ??? success "Réponse"

        ```python
        def copie(ab: ArbreBinaire) -> ArbreBinaire:
            if ab.est_vide():
                return ArbreBinaire()
            else:
                return ArbreBinaire(
                    copie(ab.gauche),
                    ab.valeur,
                    copie(ab.droite)
                )
        ```

### Fabrique d'un miroir

!!! question "Exercice"
    Écrire une fonction `miroir` qui renvoie une copie en miroir de l'arbre binaire passé en paramètre.

    ??? success "Réponse"

        ```python
        def miroir(ab: ArbreBinaire) -> ArbreBinaire:
            if ab.est_vide():
                return ArbreBinaire()
            else:
                return ArbreBinaire(
                    miroir(ab.droite),
                    ab.valeur,
                    miroir(ab.gauche)
                )
        ```


### Problème : Construction aléatoire

!!! question "Question 1 : algorithmique"
    Écrire une fonction `alea` qui prend `h`, `a`, `b` trois entiers en paramètres et qui renvoie un arbre binaire de hauteur `h` dont les valeurs sont aléatoires. Plus précisément :

    - Si `h` est nul,
        - la fonction renvoie un arbre binaire vide.
    - Sinon,
        - la fonction renvoie un arbre binaire avec une racine portant une valeur aléatoire `random.randrange(a, b)`,
        - un des deux sous-arbre est de hauteur `h-1`, obtenu de manière récursive,
        - l'autre sera d'une hauteur aléatoire donnée par `random.randrange(h)`,
        - ce choix binaire pour la latéralité pourra se faire avec le booléen `random.randrange(2) == 0`.

    ??? success "Réponse"

        ```python
        import random

        def alea(h, a, b):
            if h == 0:
                return ArbreBinaire()
            else:
                valeur = random.randrange(a, b)
                hasard_1 = alea(h - 1, a, b)
                hasard_2 = alea(random.randrange(h), a, b)
                if random.randrange(2) == 0:
                    hasard_1, hasard_2 = hasard_2, hasard_1
                return ArbreBinaire(hasard_1, valeur, hasard_2)
        ```

!!! question "Question 2 : Algorithmique et Statistiques"
    Donner la fréquence approximative de `sont_egaux(alea(h, a, b), alea(h, a, b))`,
    
    1. pour `h, a, b = 1, 0, 4`
    2. pour `h, a, b = 2, 1, 4`
    3. pour `h, a, b = 3, 0, 3`

    On pourra donner la fréquence obtenue à l'issue de 100 succès.


    ??? success "Réponse"

        ```python
        def freq(h, a, b):
            nb_egaux, nb_tests = 0, 0
            while nb_egaux < 100:
                if sont_egaux(alea(h, a, b), alea(h, a, b)):
                    nb_egaux += 1
                nb_tests += 1
            return nb_egaux / nb_tests
        
        for h, a, b in [(1, 0, 4), (2, 1, 4), (3, 0, 3)]:
            print(h, a, b, "donne", freq(h, a, b))
        ```

        ```output
        1 0 4 donne 0.25 (environ)
        2 1 4 donne 0.023 (environ)
        3 0 3 donne 0.00059 (environ)
        ```


!!! question ":boom: :boom: Question 3 : Mathématiques et Probabilités :boom: :boom:"
    Pour `h, a, b` trois entiers donnés, calculer la probabilité que `alea(h, a, b)` soit égal à `alea(h, a, b)`, une autre instance. On pourra se contenter de donner une formule récursive. On pourra vérifier que statistiques et probabilités se rejoignent.

    ??? success "Réponse"
        On fixe `a` et `b`, avec `a ≤ b`

        Soit `hasard_1 = alea(h, a, b)` et `hasard_2 = alea(h, a, b)`, on note $p_h$ la probabilité que `hasard_1` et `hasard_2` soit égaux.

        - Si $h=0$,
            - alors clairement $p_0=1$
        - Sinon,
            - Avec la probabilité $\frac1{b-a}$ ils ont la même racine, (et sinon ils ne sont pas égaux),
                1. avec la probabilité $\frac1{h^2}$, les deux sous-arbres de `hasard_1` et les deux de `hasard_2` sont de hauteur $h-1$.
                2. pour chaque $i$ dans $[\![0\,;\,h-1[\![$, avec la probabilité $\frac{1}{2h^2}$, les sous-arbres de `hasard_1`, comme pour `hasard_2`, sont de mêmes hauteurs $h$ et $i$ tous les deux, **et du même côté**.
                3. dans les autres cas, les formats ne correspondent pas du tout.
            - On déduit $p_h = \frac1{b-a}×p_{h-1}×\left(p_{h-1} + \frac12\sum_{i=0}^{h-2}p_{i} \right) ×\frac1{h^2}$
        
        La fonction suivante permet de vérifier que les statistiques et les probabilités se rejoignent.

        ```python
        def proba(h, a, b):
            if h == 0: return 1
            return 1/(b-a) * proba(h-1, a, b) * (proba(h-1, a, b) + 1/2 * sum(proba(i, a, b) for i in range(h-1))) / h**2
        ```




!!! question ":boom: Question 4 : Mathématiques et Probabilités :boom:"
    Pour `h, a, b` trois entiers donnés, calculer la probabilité que `alea(h, a, b)` soit égal à son arbre miroir. On pourra se contenter de donner une formule qui dépend de l'exercice précédent.

    ??? success "Réponse"
        On fixe `a` et `b`.

        Soit `ab = alea(h, a, b)`, on note $p'_h$ la probabilité cherchée.

        - Si $h=0$,
            - alors clairement $p'_0=1$
        - Sinon,
            1. avec la probabilité $\frac1h$, les deux sous-arbres de `ab` sont de hauteur $h-1$. Ils sont en miroir avec la probabilité $p_{h-1}$.
            2. avec la probabilité $\frac{h-1}h$, l'un des deux a une hauteur $h-1$, l'autre strictement inférieure, ils ne sont donc pas en miroir.
            3. On déduit que $p'_h=\frac1h×p_{h-1}$


## Représentation en Python

```mermaid
graph TD
    Z([Zoé]) --> R([Régis])
    Z --> U([Annie])
    R --> Q(( ))
    R --> S([Serge])
    U --> B(( ))
    U --> N(( ))
    S --> Sa(( ))
    S --> Sb(( ))
```

### Avec un tuple à trois éléments

Un arbre binaire peut être représenté _simplement_ par un ensemble de nœuds de la forme `(gauche, valeur, droite)` où :

- `None` désigne `nil`, un arbre vide.
- `gauche` est le sous-arbre à gauche, représenté par `None` ou bien le nœud racine.
- `droite` est le sous-arbre à droite, représenté par `None` ou bien le nœud racine.

Notre exemple peut être alors représenté par :

```python
serge = (None, "Serge", None)
regis = (None, "Régis", serge)
annie = (None, "Annie", None)
zoe = (regis, "Zoé", annie)
```

Avec cette représentation on peut donner le code de certaines fonctions.

```python
--8<-- "modules/arbre_binaire_1.py"
```

!!! warning "Ne pas mélanger les deux représentations"
    Sans POO, on peut modéliser l'arbre vide par `None`, ce n'est pas gênant.

    En revanche, avec POO, on ne peut pas faire ainsi, l'arbre vide doit être une instance de la classe et posséder les méthodes communes, ce n'est donc pas `None`. On peut bien sûr utiliser `None` en interne, mais pas pour l'arbre vide lui-même ! Cette erreur se retrouve malheureusement...

### Avec paradigme POO

On peut aussi créer une classe `ArbreBinaire` minimaliste

```python
--8<-- "modules/arbre_binaire_2.py"
```

!!! question "Exercice"
    Transformer les fonctions suivantes (vues précédemment) en méthodes pour la classe `ArbreBinaire` : `est_feuille`, `est_degenere`, `est_parfait`, `est_equilibre`, `somme`, `sont_egaux`, `sont_miroir`, `est_inclus`, `copie`, `miroir`.

    Pour `maximum (et `minimum`), on utilisera un _wrapper_ pour traiter le cas vide à part :

    ```python
    def maximum(self):
        if self.est_vide():
            raise ValueError("Un arbre vide n'a pas de maximum")
        else:
            return self.max_rec()

    def max_rec(self):
        """Renvoie le maximum de self qui est non vide, de manière récursive"""
        ...
    ```



## Parcours en profondeur d'un arbre binaire

!!! question "Question motivante"
    Les fonctions que nous avons découvertes utilisaient des opérateurs commutatifs, en effet on a 
    
    - `max(a, b) == max(b, a)` et
    - `1 + x + y == x + 1 + y == x + y + 1`.
    
    Mais que se passe-t-il avec des **fonctions non commutatives** ?

    Si on conserve une action à gauche **avant** celle de droite, alors il reste **trois** positions pour appliquer l'action sur le nœud en cours.

=== "Parcours préfixe"
    - On fait **l'action sur le nœud**,
    - puis l'action sur le sous arbre gauche,
    - puis l'action sur le sous arbre droite.

=== "Parcours infixe"
    - On fait l'action sur le sous arbre gauche,
    - puis **l'action sur le nœud**,
    - puis l'action sur le sous arbre droite.

=== "Parcours postfixe"
    - On fait l'action sur le sous arbre gauche,
    - puis l'action sur le sous arbre droite.
    - puis **l'action sur le nœud**.

!!! question "Exercice"
    Appliquer les fonctions suivantes. **Vous ferez sur papier** les parcours avec `action` définie comme « afficher le nœud en cours », sur l'arbre de notre exemple.

    ```python
    def parcours_prefixe(ab, action):
        if not ab.est_vide():
            action(ab)
            parcours_prefixe(ab.gauche)
            parcours_prefixe(ab.droite)

    def parcours_infixe(ab, action):
        if not ab.est_vide():
            parcours_infixe(ab.gauche)
            action(ab)
            parcours_infixe(ab.droite)

    def parcours_postfixe(ab, action):
        if not ab.est_vide():
            parcours_postfixe(ab.gauche)
            parcours_postfixe(ab.droite)
            action(ab)
    ```


!!! tip "Humour"
    Ceci est un dessin humoristique lié aux parcours d'arbre.

    [![https://xkcd.com/2407/](images/depth_and_breadth.png)](https://xkcd.com/2407/)


## Exercices sur les arbres binaires

!!! question "Exercice - Parcours infixe"
    On considère l'arbre binaire suivant :

    ```mermaid
    graph TD
        A --> B
        A --> D
        B --> Ba(( ))
        B --> C
        C --> Ca(( ))
        C --> Cb(( ))
        D --> Da(( ))
        D --> Db(( ))
    ```

    1. Construire sa représentation interne `exemple` en Python à l'aide de la classe `ArbreBinaire`.
    2. Écrire une fonction `affichage` définie par :
        * sur un arbre vide, ne rien faire ;
        * sur un arbre non vide, 
            * afficher `(`, puis
            * afficher (récursivement) le sous-arbre à gauche, puis
            * afficher la racine, puis
            * afficher (récursivement) le sous-arbre à droite, puis
            * afficher `)`
    3. Vérifier que `affichage(exemple)` produit `'((B(C))A(D))'`.
    4. Dessiner un `arbre` dont `affichage(arbre)` produit `'(1((2)3))'`.
    5. De manière générale, expliquer comment retrouver un arbre dont l'affichage est donné.

!!! question "Exercice - Parcours en largeur"

    * Pour afficher (ou faire une autre action sur) un arbre binaire avec un parcours en largeur :
        * On utilise une file initialement vide.
        * On enfile l'arbre dans la file.
        * Tant que la file est non vide :
            * On défile un arbre.
            * S'il n'est pas vide, il y a un nœud racine et :
                * On affiche (ou on traite suivant une action) la valeur portée par ce nœud.
                * On enfile le sous-arbre à gauche (puis celui à droite).

    1. Écrire une fonction de parcours en largeur.
    2. Tester votre fonction.
    3. Vérifier qu'elle consiste à lire l'arbre étage par étage en commençant par la racine, puis de gauche à droite pour chaque étage.
    4. Modifier votre fonction pour que chaque valeur soit affichée avec son niveau. La racine est au niveau `0`.

    ??? success "Réponse"

        ```python
        # On suppose que l'on dispose d'une classe File
        # avec les méthodes enfile et défile.

        def parcours_largeur(ab):
            suite = File()
            suite.enfile(ab)
            while not suite.est_vide():
                ab = suite.defile()
                if not ab.est_vide():
                    print(ab.valeur)
                    suite.enfile(ab.gauche)
                    suite.enfile(ab.droite)

        def parcours_largeur(ab):
            # Avec les niveaux
            suite = File()
            suite.enfile((ab, 0))
            while not suite.est_vide():
                ab, niveau = suite.defile()
                if not ab.est_vide():
                    print(ab.valeur, niveau)
                    suite.enfile((ab.gauche, niveau + 1))
                    suite.enfile((ab.droite, niveau + 1))
        ```

!!! question "Exercice - Reconstruire un arbre binaire"

    Un arbre binaire est étiqueté avec des lettres.

    * Un parcours préfixe donne un affichage `ALORHGIMET`.
    * Un parcours infixe donne un affichage `OLHRAMIEGT`.

    1. Reconstruire l'arbre qui a produit ces affichages.
    2. Qu'obtient-on avec un parcours en largeur ?
    3. Qu'obtient-on avec un parcours postfixe ?

    ??? success "Réponse"

        1. Le parcours préfixe commence par un `A`, donc la racine est étiquetée `A`. Il n'y a qu'un seul `A`, on déduit alors aussi que :
            * Le sous-arbre à gauche est composé des étiquettes `OLHR` en infixe, et donc `LORH` en préfixe.
            * Le sous-arbre à droite est composé des étiquettes `MIEGT` en infixe, et donc `GIMET` en préfixe.
        2. La racine du sous arbre à gauche est donc `L`, et `G` à droite.
            * Sous-arbre à gauche :
                * Son sous-arbre à gauche : `O` en préfixe comme en infixe.
                * Son sous-arbre à droite : `RH` en préfixe et `HR` en infixe.
            * Sous-arbre à droite :
                * Son sous-arbre à gauche : `IME` en préfixe et `MIE` en infixe.
                * Son sous-arbre à droite : `T` en préfixe comme en infixe.
        3. On déduit ensuite que le parcours en largeur est `ALGORITHME`.

