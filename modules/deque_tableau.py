class Deque():
    """Classe deque basée sur un tableau ayant une taille_max donnée
    """

    def __init__(self, taille_max):
        self.taille_max = taille_max
        self.données = [None for _ in range(taille_max)]
        self.i_droite = 0
        self.i_gauche = taille_max - 1
        self.taille = 0
    
    def est_vide(self):
        return self.taille == 0
    
    def ajout_droite(self, élément):
        if self.taille == self.taille_max:
            raise ValueError("Deque pleine")
        self.données[self.i_droite] = élément
        self.i_droite += 1
        if self.i_droite == self.taille_max:
            self.i_droite = 0
        self.taille += 1
    
    def ajout_gauche(self, élément):
        if self.taille == self.taille_max:
            raise ValueError("Deque pleine")
        self.données[self.i_gauche] = élément
        self.i_gauche -= 1
        if self.i_gauche == -1:
            self.i_gauche = self.taille_max - 1
        self.taille += 1

    def extrait_droite(self):
        if self.taille == 0:
            raise ValueError("Deque vide")
        self.i_droite -= 1
        if self.i_droite == -1:
            self.i_droite = self.taille_max - 1
        self.taille -= 1
        élément = self.données[self.i_droite]
        #self.données[self.i_droite] = None # facultatif
        return élément

    def extrait_gauche(self):
        if self.taille == 0:
            raise ValueError("Deque vide")
        self.i_gauche += 1
        if self.i_gauche == self.taille_max:
            self.i_gauche = 0
        self.taille -= 1
        élément = self.données[self.i_gauche]
        #self.données[self.i_gauche] = None # facultatif
        return élément

if __name__ == '__main__':
    # tests
    test = Deque(256)  # taille de 256 éléments

    # test est_vide
    assert test.est_vide()

    # test un ajout/extraction à droite
    test.ajout_droite(42)
    élément = test.extrait_droite()
    assert élément == 42
    assert test.est_vide()

    # test un ajout/extraction à gauche
    test.ajout_gauche(1337)
    élément = test.extrait_gauche()
    assert élément == 1337
    assert test.est_vide()

    # test plusieurs ajouts
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajout_gauche(élément)
    assert [test.extrait_droite()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
    for élément in premiers:
        test.ajout_droite(élément)
    assert [test.extrait_gauche()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
