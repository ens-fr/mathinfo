# Second degré

!!! note "Rappel : Fonctions affines"
    Une fonction affine est une fonction de la forme $f(x) = ax +b$, avec $a, b\in \mathbb R$.

    - $b$ est le terme constant, il est égal à $b×x^0$, on dit que c'est le terme de degré $0$.
    - $ax$ est le terme proportionnel à $x$, il est égal à $a×x^1$, on dit que c'est le terme de degré $1$ quand $a\neq0$.

Une fonction polynôme du second degré[^1] est de la forme
$f(x) = ax^2 + bx +c$, avec $a, b, c\in \mathbb R$, et $a\neq 0$.

[^1]: :material-wikipedia: [Équation du second degré](https://fr.wikipedia.org/wiki/%C3%89quation_du_second_degr%C3%A9)

- $ax^2$ est le terme de degré $2$.
- $f$ est définie sur $\mathbb R$
- Le discriminant de $f$ est $\Delta = b^2-4ac$

## Forme canonique

Avec un peu de calculs, on obtient

$$f(x) = a\left[\left(x - \dfrac{-b}{2a}  \right)^2  - \dfrac \Delta {4a^2} \right]$$

??? tip "Détails"
    Vérifier chaque étape pour bien comprendre !

    - $f(x) = ax^2 + bx +c$
    - $f(x) = a\left(x^2 + 2x\dfrac{b}{2a} + \dfrac c a\right)$
    - $f(x) = a\left(x^2 + 2x\dfrac{b}{2a} + \dfrac{b^2}{4a^2} - \dfrac{b^2}{4a^2}  +\dfrac c a\right)$
    - $f(x) = a\left[\left(x^2 + 2x\dfrac{b}{2a} + \dfrac{b^2}{4a^2}\right) - \dfrac{b^2}{4a^2}  -\dfrac {-4ac} {4a^2}\right]$
    - $f(x) = a\left[\left(x - \dfrac{-b}{2a} \right)^2 - \dfrac{\Delta}{4a^2}\right]$
    

On déduit que

1. La représentation graphique est une parabole avec un axe de symétrie $x_m = \dfrac{-b}{2a}$.
2. $f$ possède un extremum en $x_m = \dfrac{-b}{2a}$ qui est égal à $f(x_m) = \dfrac {-\Delta}{4a}$.

Il y a quatre cas suivant le signe de $\Delta$ et de $a$.

![](./asy/par1.svg){ .autolight }
![](./asy/par2.svg){ .autolight }
![](./asy/par3.svg){ .autolight }
![](./asy/par4.svg){ .autolight }


## Représentation graphique

En observant la représentation graphique de $f$, avec $f(x) = ax^2+bx+c$ et $a\neq 0$, on peut déterminer le signe de $a$, de $b$ et de $c$.

- L'orientation de la parabole donne le signe de $a$.
    - $a>0$ pour une parabole orientée vers le haut,
    - $a<0$ pour une parabole orientée vers le bas,
- L'ordonnée à l'origine indique la valeur de $c$.
    - $f(0) = a×0^2 + b×0 + c = c$
- Pour la valeur de $b$, on regarde la tangente à la courbe au point d'abscisse $0$. En effet, pour $x$ proche de $0$, on a $f(x) \approx bx+c$ et le terme $ax^2$ devient négligeable pour $x$ proche de zéro. La courbe représentative de $f$ possède alors une tangente associée à la fonction affine $x\mapsto bx+c$.
    - $b$ est le coefficient directeur de la tangente au point d'abscisse $0$.

!!! example "Exemple"
    ![](./asy/parabole.svg){ .autolight }

    - La parabole est orientée vers le bas donc $a<0$
    - On peut lire directement que $f(0)=c=-3$
    - Enfin, la tangente au point de coordonnée $(0, -3)$ a un coefficient directeur négatif, donc $b < 0$.
