class ArbreBinaire:
    def __init__(self, gauche=None, valeur=None, droite=None):
        self.gauche = gauche
        self.valeur = valeur
        self.droite = droite

    def est_vide(self):
        return self.valeur is None

def sont_egaux(ab_x: ArbreBinaire, ab_y: ArbreBinaire) -> bool:
    if ab_x.est_vide():
        return ab_y.est_vide()
    elif ab_x.valeur != ab_y.valeur:
        return False
    else:
        return sont_egaux(ab_x.gauche, ab_y.gauche) and sont_egaux(ab_x.droite, ab_y.droite)

import random

def alea(h, a, b):
    if h == 0:
        return ArbreBinaire()
    else:
        valeur = random.randrange(a, b)
        hasard_1 = alea(h - 1, a, b)
        hasard_2 = alea(random.randrange(h), a, b)
        if random.randrange(2) == 0:
            hasard_1, hasard_2 = hasard_2, hasard_1
        return ArbreBinaire(hasard_1, valeur, hasard_2)


def freq(h, a, b):
    nb_egaux, nb_tests = 0, 0
    while nb_egaux < 100:
        if sont_egaux(alea(h, a, b), alea(h, a, b)):
            nb_egaux += 1
        nb_tests += 1
    return nb_egaux / nb_tests

def proba(h, a, b):
    if h == 0: return 1
    return 1/(b-a) * proba(h-1, a, b) * (proba(h-1, a, b) + 1/2 * sum(proba(i, a, b) for i in range(h-1))) / h**2

# tests
for h, a, b in [(1, 0, 4), (2, 1, 4), (3, 0, 3)]:
    print(h, a, b, "donne", freq(h, a, b), proba(h, a, b))



