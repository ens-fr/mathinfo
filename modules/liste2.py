class Maillon:
    """Un maillon est donné par son élément
     et ses maillons à gauche et à droite, éventuellement None.
    """

    def __init__(self, gauche, élément, droite):
        self.gauche = gauche
        self.élément = élément
        self.droite = droite

    def __str__(self):
        return str(self.élément)


class Liste:
    """Une liste est donnée par ses maillons de gauche et de droite.

    On crée une structure de deque avec une liste doublement chaînée.
    """

    def __init__(self):
        self.maillon_gauche = None
        self.maillon_droite = None

    def est_vide(self):
        return     (self.maillon_gauche is None) \
               and (self.maillon_droite is None)

    def ajout_gauche(self, élément):
        if self.est_vide():
            maillon = Maillon(None, élément, None)
            self.maillon_gauche = maillon
            self.maillon_droite = maillon
        else:
            # le nouveau maillon de gauche est un maillon qui
            #    - pointe à gauche vers Nil
            #    - possède l'élément donné en paramètre
            #    - pointe à droite vers l'ancien maillon de gauche
            self.maillon_gauche = Maillon(None, élément, self.maillon_gauche)

            # le nouveau maillon de gauche pointe à droite vers
            #   l'ancien maillon de gauche (pas un Nil)
            #    - il pointait avant à gauche sur Nil,
            #    - on modifie vers le nouveau maillon gauche
            self.maillon_gauche.droite.gauche = self.maillon_gauche

    def ajout_droite(self, élément):
        if self.est_vide():
            maillon = Maillon(None, élément, None)
            self.maillon_gauche = maillon
            self.maillon_droite = maillon
        else:
            # le nouveau maillon de droite est un maillon qui
            #    - pointe à droite vers Nil
            #    - possède l'élément donné en paramètre
            #    - pointe à gauche vers l'ancien maillon de droite
            self.maillon_droite = Maillon(self.maillon_droite, élément, None)

            # le nouveau maillon de droite pointe à gauche vers l'ancien maillon de droite (pas un Nil)
            #    - il pointait avant à droite sur Nil, on modifie vers le nouveau maillon droite
            self.maillon_droite.gauche.droite = self.maillon_droite

    def extrait_gauche(self):
        # cas : 0 maillon
        if self.est_vide():
            raise ValueError("Liste vide")

        # cas : 1 seul maillon
        if self.maillon_droite == self.maillon_gauche:
            élément = self.maillon_gauche.élément
            self.maillon_gauche = None
            self.maillon_droite = None
            return élément

        # cas : au moins deux maillons
        élément = self.maillon_gauche.élément
        self.maillon_gauche = self.maillon_gauche.droite
        self.maillon_gauche.gauche = None
        return élément
    
    def extrait_droite(self):
        # cas : 0 maillon
        if self.est_vide():
            raise ValueError("Liste vide")

        # cas : 1 seul maillon
        if self.maillon_droite == self.maillon_gauche:
            élément = self.maillon_droite.élément
            self.maillon_gauche = None
            self.maillon_droite = None
            return élément

        # cas : au moins deux maillons
        élément = self.maillon_droite.élément
        self.maillon_droite = self.maillon_droite.gauche
        self.maillon_droite.droite = None
        return élément

    def __str__(self):
        affichage = "Contenu : "
        maillon = self.maillon_gauche
        while maillon is not None:
            affichage += str(maillon) + "::"
            maillon = maillon.droite
        affichage += " fin."
        return affichage


if __name__ == '__main__':
    # tests
    test = Liste()

    # test est_vide
    assert test.est_vide()

    # test un ajout/extraction à droite
    test.ajout_droite(42)
    élément = test.extrait_droite()
    assert élément == 42
    assert test.est_vide()

    # test un ajout/extraction à gauche
    test.ajout_gauche(1337)
    élément = test.extrait_gauche()
    assert élément == 1337
    assert test.est_vide()

    # test plusieurs ajouts
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajout_gauche(élément)
    assert [test.extrait_droite()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
    for élément in premiers:
        test.ajout_droite(élément)
    assert [test.extrait_gauche()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
