class ArbreBinaire:
    def __init__(self, gauche=None, valeur=None, droite=None):
        self.gauche = gauche
        self.valeur = valeur
        self.droite = droite

    def est_vide(self):
        return self.valeur is None

def taille(ab):
    if ab.est_vide():
        return 0
    else:
        return taille(ab.gauche) + 1 + taille(ab.droite)

def hauteur(ab):
    if ab.est_vide():
        return 0
    else:
        return 1 + max(hauteur(ab.gauche), hauteur(ab.droite))

if __name__ == '__main__':
    # tests
    serge = ArbreBinaire(
        ArbreBinaire(),
        "Serge",
        ArbreBinaire()
    )
    regis = ArbreBinaire(
        ArbreBinaire(),
        "Régis",
        serge
    )
    annie = ArbreBinaire(
        ArbreBinaire(),
        "Annie",
        ArbreBinaire()
    )
    zoe = ArbreBinaire(
        regis,
        "Zoé",
        annie
    )

    assert taille(zoe) == 4
    assert hauteur(zoe) == 3
