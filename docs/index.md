# Mathématiques et informatique

## Liens utiles

```markmap
# Soutien

## Mathématiques Lycée


### [Le livre scolaire 1ère](https://www.lelivrescolaire.fr/page/6826700)
### [Le livre scolaire Term](https://www.lelivrescolaire.fr/page/16724728)
### [Le livre scolaire Term / expert](https://www.lelivrescolaire.fr/page/15117110)

### [H5P : Équations variées](#automatismes)

### [Pyromaths](https://www.pyromaths.org/)

## Algorithmique tous niveaux

### [💥 Ressources sur Python, cours et exercices, faciles et moyens 💥](https://e-nsi.forge.aeif.fr/)
### [💥 💥 💥 Méthodes mathématiques avancées 💥 💥 💥](https://ens-fr.gitlab.io/enumeration/)
### [💥 💥 💥 💥 💥  _Algorithms for Competitive Programming_ (`C++`)  💥 💥 💥 💥 💥](https://cp-algorithms.com/index.html)

### [VSCodium](./0-VSCodium/index.html)

## Calculatrice

### [NumWorks en version locale](./numworks/simulator.html)

```

## Automatismes

!!! info "Les automatismes pour les équations"
    Il est très important de savoir résoudre rapidement les équations simples. Cela donne une aisance pour aborder les calculs plus complexes. Les 3/4 premiers se font de tête, les derniers se font avec une feuille de brouillon pour aider.

    1. [Équations](./h5p/eq_N1.html) de la forme $ax=b$, sans fractions
    2. [Équations](./h5p/eq_N1f.html) de la forme $ax=b$, **avec** fractions :warning:
    3. [Équations](./h5p/eq_N2.html) de la forme $ax+b=c$, sans fractions
    4. [Équations](./h5p/eq_N2f.html) de la forme $ax+b=c$, **avec** fractions :warning:
    5. [Équations](./h5p/eq_N3.html) de la forme $ax+b=cx+d$, sans fractions
    6. [Équations](./h5p/eq_N3f.html) de la forme $ax+b=cx+d$, **avec** fractions :warning:

!!! info "Les automatismes, avec Pyromaths"
    Il est très important pour tous les élèves, surtout ceux qui ont du mal à mener un calcul, ou qui ont d'autres lacunes, de travailler les automatismes.
    
    Avec Pyromaths, vous pouvez vous entrainer en toute autonomie, un corrigé détaillé est disponible. Vous pouvez recommencer les mêmes exercices, le sujet et le corrigé seront automatiquement modifiés avec de nouveaux nombres.

    Les élèves en grande difficulté peuvent aussi donc revoir les bases du collège.



!!! cite "Culture mathématique et scientifique"

    - <https://scienceetonnante.com/>
    - <https://www.3blue1brown.com/>


    ![polygones](assets/régulier.gif)

!!! question "Amusette"
    Quelle est la hauteur de la table ?

    ![](./assets/table.png){ .bordure }

    ??? success "Réponse"
        Avec des notations évidentes, on a les équations :

        - `table + chat - tortue = 170`
        - `table + tortue - chat = 130`

        En additionnant les deux lignes, on obtient :

        - `2 * table = 170 + 130`

        D'où `table = 300 / 2 = 150`

        La table fait 150 cm de haut.


        
