# TD1

## Exercice 1

On utilisera le raisonnement par récurrence pour montrer les résultats suivants :

!!! example "Résultat 1"

    Soit la suite réelle $(u_n)_{n\in\mathbb N}$ définie par 

    $$
    \begin{cases}
    u_0 = 4\\
    u_{n+1} = \dfrac12\left(\frac{11}{u_n}+u_n\right)\text{, pour } n\in\mathbb N
    \end{cases}
    $$

    Montrer que $\forall n \in \mathbb N$, $u_n \geqslant \sqrt{11}$.

    ???+ success "Réponse"
        Soit $P_n$ la proposition de récurrence définie sur $\mathbb N$ par « $u_n \geqslant \sqrt{11}$ ».

        - Pour $n=0$, on a $16\geqslant 11$, et $x\mapsto \sqrt x$ qui est une fonction croissante sur $\mathbb R^+$, ainsi $\sqrt{16}\geqslant \sqrt{11}$, de sorte que $u_0\geqslant \sqrt{11}$. Ainsi $P_0$ est vraie.
        - Soit $n\in\mathbb N$, on suppose $P_n$ vraie. Montrons que $P_{n+1}$ est vraie.
        -  
            On considère la fonction $x\mapsto f(x)=\dfrac12\left(\frac{11}{x}+x\right)$, dont la dérivée est $f'$ avec :
            
            $f'(x) = \dfrac12\left(\frac{-11}{x^2}+1\right)$
            
            On observe que pour $x\geqslant \sqrt{11}$, $f'(x)\geqslant 0$, de sorte que $f$ est croissante sur $\left[\sqrt{11}\,;\,+\infty\right[$
            
            Avec $u_n\geqslant \sqrt{11}$, on déduit $f(u_n) \geqslant f(\sqrt{11})$, et enfin
            
            $u_{n+1} \geqslant \sqrt{11}$, donc $P_{n+1}$ est vraie.
        - On a montré que $P_0$ est vraie et que pour tout $n$ de $\mathbb N$ si $P_n$ est vraie alors $P_{n+1}$ est vraie. Par récurrence, on déduit que $P_n$ est vraie pour tout $n\in\mathbb N$.

            


!!! example "Résultat 2"

    Soit une suite réelle $(u_n)$ telle que $\forall n \in \mathbb N$, $u_{n+2} + u_{n+1} − u_n = 0$.
    
    Montrer que si $(u_n)$ est une telle suite et si $u_0$ et $u_1$ sont des éléments de $\mathbb Z$ alors pour tout élément $n$ de $\mathbb N$, $u_n$ est dans $\mathbb Z$.

    ???+ success "Réponse"
        On suppose que $(u_n)$ est une telle suite, avec $u_0$ et $u_1$ qui sont des éléments de $\mathbb Z$.

        Soit $P_n$ la proposition de récurrence définie sur $\mathbb N$ par « $u_n \in \mathbb Z$ ».

        - Pour $n$ égal à $0$ ou $1$, on sait que $P_n$ est vraie par définition.
        - Soit $n\in\mathbb N$, on suppose $P_k$ vraie pour tout $k$ de $0$ à $n$. Montrons que $P_{n+1}$ est vraie.
        -  
            - Si $n=0$, on sait déjà que $u_1 \in\mathbb Z$.
            - Si $n>0$, on a $u_{n+1} = -u_n + u_{n-1}$, et d'après $P_n$ ce sont des entiers relatifs, ainsi $u_{n+1} \in \mathbb Z$ aussi. De sorte que $P_{n+1}$ est vraie.
        - On a montré que $P_0$ est vraie et que pour tout $n$ de $\mathbb N$ si $P_k$ est vraie pour tout $k$ de $0$ à $n$, alors $P_{n+1}$ est vraie. Par récurrence, on déduit que $P_n$ est vraie pour tout $n\in\mathbb N$.
