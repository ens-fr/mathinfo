# Preuve des formules

Cette page n'intéressera que les élèves motivés.

!!! warning "Preuve"
    Ici les preuves sont simplifiées pour être digestes pour les élèves de première. En post-BAC vous verrez des preuves plus rigoureuses, où l'on précise ce qu'est « être proche », et ce que signifie réellement $f(x)\approx ...$

    Les preuves ne sont pas très simples. Les exemples le sont.

## Construction par étapes


### Approximation par une fonction affine

Si $f$ est dérivable en $a$, et $x$ proche de $a$, on a $f(x) \approx f(a) + f'(a)×(x-a)$.

Réciproquement, si pour $x$ proche de $a$, on a $f(x) \approx p + m×(x-a)$, alors

- $f(a) = p$
- $f'(a) = m$

> On va se servir souvent de cette propriété dans les preuves.

!!! Example "Exemple"
    Si pour $x$ proche de $5$, on a $f(x) \approx 19 - 7×(x-5)$, alors

    - $f(5) = 19$
    - $f'(5) = -7$

### Fonction constante

Si $f$ est une fonction constante sur un intervalle, alors sa fonction dérivée $f'$ est nulle dans l'intervalle ouvert.

!!! tip "Preuve"
    En effet, toute tangente **serait** horizontale, donc de coefficient directeur nul.

!!! Example "Exemple 1"
    Si $f(x) = \dfrac{\pi}4$ pour tout $x\in\mathbb R$, alors $f'(x)=0$ pour tout $x\in\mathbb R$.

!!! Example "Exemple 2"
    Si $f(x) = 1$ pour tout $x\in [5 ; 9]$, alors $f'(x)=0$ pour tout $x\in\; ]5 ; 9 [$.

### Fonction affine

Si $f(x) = mx+p$ sur $\mathbb R$, alors $f$ est dérivable sur $\mathbb R$ et $f'(x) = m$.

!!! tip "preuve"
    En effet, la courbe représentant $f$ est confondue avec la tangente ; elles sont identiques, de coefficient directeur $m$.

!!! Example "Exemple 1"
    Si $f(x) = 6x - 9$ pour $x\in \mathbb R$, alors $f'(x) = 6$ pour $x\in \mathbb R$.

!!! Example "Exemple 2"
    Si $f(x) = x$ pour $x\in \mathbb R$, alors $f'(x) = 1$ pour $x\in \mathbb R$.


### Produit avec une constante

Si $f$ est une fonction dérivable sur un intervalle $I$, et que $k$ est une constante, alors $(kf)'=kf'$.

!!! tip "Preuve"
    Si $a\in I$ et $x$ proche de $a$, on a $f(x) \approx f(a) + f'(a)×(x-a)$,
    
    on tire $kf(x) \approx kf(a) + kf'(a)×(x-a)$, d'où le résultat.

On peut aussi écrire $(kf(x))' = kf'(x)$ pour $x\in I$

!!! Example "Exemple 1"
    Si $f'(x) = 3x^2 - x$ pour $x\in \mathbb R$,
    
    alors $(5(3x^2 - x))' = (5f)'(x) =5f'(x) = 15x^2 - 5x$ pour $x\in \mathbb R$.
    
    > En effet $5$ est une constante.

!!! Example "Exemple 2"
    Si $f'(x) = g(x)$ pour $x\in \mathbb R$,
    
    alors $(-3f)'(x) =-3f'(x) = -3g(x)$ pour $x\in \mathbb R$.
    
    > En effet $-3$ est une constante.

!!! Example "Exemple 3"
    Si $f'(x) = h(x)$ pour $x\in I$,
    
    alors $(\pi f)'(x) =\pi f'(x) = \pi h(x)$ pour $x\in I$.
    
    > En effet $\pi$ est une constante.


### Monôme de degré n

- Pour $n\in \mathbb N^*$, $f: x\mapsto x^n$ est dérivable sur $\mathbb R$, et on a $(x^n)' = nx^{n-1}$
- Pour $n=0$, $f: x\mapsto x^n = 1$ est constante sur $\mathbb R$, donc dérivable sur $\mathbb R$ et on a $(x^0)' = 0$

!!! Example "Exemple 1"
    $(x^{13})' = 13x^{12}$ sur $\mathbb R$.

!!! Example "Exemple 2"
    $x' = (x^1)' = 1x^0 = 1$ sur $\mathbb R$.

!!! Example "Exemple 3"
    $(5x^3)' = 5(x^3)' = 5×3x^2 = 15x^2$ sur $\mathbb R$.

!!! tip "Preuve"
    La preuve n'est pas au programme en première pour $n>2$.

    Mais nous la présentons plus bas.


### Somme de fonctions

Si $f$ et $g$ sont dérivables sur $I$, alors $f+g$ est dérivable sur $I$.

!!! tip "Preuve"
    Pour $a\in I$ et $x$ proche de $a$, on a

    $$\begin{align*}
    f(x)& \approx f(a) + f'(a)×(x-a)\\
    g(x)& \approx g(a) + g'(a)×(x-a)\\
    \end{align*}$$
    
    On déduit

    $$\begin{align*}
    f(x)+g(x)& \approx f(a) + g(a) + f'(a)×(x-a) + g'(a)×(x-a)\\
    (f+g)(x)&  \approx (f+g)(a) + \left(f'(a)+g'(a)\right)×(x-a)\\
    \end{align*}$$
     
    D'où $(f+g)'(a) = f'(a) + g'(a)$, ceci étant valable pour tout $a\in I$

    On peut aussi écrire $(f+g)'(x) = f'(x) + g'(x)$, pour tout $x\in I$.

    On écrit aussi plus simplement $(f+g)' = f'+g'$



!!! Example "Exemple 1 : avec $f(x) = 5x^3 + 7x^2 -3x +8$"

    $$\begin{align*}
    f'(x) &= (5x^3   + 7x^2    -3x      +8)'\\
    f'(x) &= (5x^3)' + (7x^2)' + (-3x)' + (8)'\\
    f'(x) &= 5(x^3)' + 7(x^2)' + (-3(x)') + 0\\
    f'(x) &= 5×3x^2  + 7×2x^1  + (-3×1)     \\
    f'(x) &= 15x^2   + 14x   -3              \\
    \end{align*}$$
    
    pour $x\in \mathbb R$.

!!! Example "Exemple 2 : avec $g(x) = 7x^4 - 3x^3 + 6x^2 -5x +9$"

    $$\begin{align*}
    g'(x) &= (7x^4 - 3x^3 + 6x^2 -5x +9)'\\
    g'(x) &= 7×4x^3 - 3×3x^2 + 6×2x^1 -5\\
    g'(x) &= 28x^3 - 9x^2 + 12x -5\\
    \end{align*}$$
    
    pour $x\in \mathbb R$.

    > On peut aller plus rapidement dès qu'on a bien compris.


### Produit de fonctions

Si $u$ et $v$ sont dérivables sur $I$, alors 

- $u×v$ est dérivable sur $I$,
- $(u×v)' = u'×v + u×v'$

!!! example "Exemple"
    Pour $x\in \mathbb R$, avec
    
    $$\begin{align*}
    u(x) &= x^3 - 2     & u'(x) &= 3x^2\\
    v(x) &= 5x^7 - x^2  & v'(x) &= 35x^6 - 2x\\
    \end{align*}$$

    On déduit

    $$\begin{align*}
    (u(x)×v(x))' &= u'(x)×v(x) + u(x)×v'(x)\\
    (u(x)×v(x))' &= 3x^2 × (5x^7-x^2)  + (x^3-2)×(35x^6-2)\\
    \end{align*}$$

    que l'on pourrait ensuite réduire...

## Exercices

### Fonction racine carrée

On suppose que $x\mapsto u(x) = \sqrt{x}$ est dérivable sur $I$,

On sait que $u(x)×u(x) = x$ sur $\mathbb R_+$

D'une part

$$(u(x)×u(x))' = u'(x)×u(x) + u(x)×u'(x)$$

D'autre part

$$(u(x)×u(x))' = (x)' = 1$$

et donc $2u'(x)×u(x) = 1$, d'où on tire $u'(x) = \dfrac1{2u(x)}$

Conclusion

$$\forall x\in \mathbb R_{+}^{*} \quad \left(\sqrt{x}\right)' = \dfrac1{2\sqrt x}$$


### Fonction inverse

On suppose que $x\mapsto u(x) = \dfrac1x$ est dérivable sur $I$,

- $(u(x)×x) = 1$ sur $\mathbb R^*$, donc
- $\left(\dfrac1x × x\right)' = u'(x)×x + u(x)×(x)'$
- $(1)' = u'(x)×x + \dfrac1x × 1$, d'où
- $0 = u'(x)×x + \dfrac1x$ et enfin
- $u'(x) = \dfrac{-1}{x^2}$ sur $\mathbb R^*$

Conclusion

$$\forall x\in \mathbb R^{*} \quad \left(\dfrac1x\right)' = \dfrac{-1}{x^2}$$

### Fonction $x\mapsto \dfrac1{x^n}$

Avec $n\in\mathbb N^*$.

On suppose que $x\mapsto u(x) = \dfrac1{x^n}$ est dérivable sur $I=\mathbb R^*$,

- On pose $v(x) = x^n$, et on a $v'(x) = nx^{n-1}$ sur $\mathbb R$
- $(u(x)×v(x)) = 1$ sur $\mathbb R^*$, donc
- $(u(x)×u(x))' = u'(x)×v(x) + u(x)×v'(x)$
- $(1)' = u'(x)×x^n + \dfrac1{x^n} × n×x^{n-1}$, d'où (en simplifiant par $x^{n-1}$)
- $0 = u'(x)×x + \dfrac{n}{x^n}$ et enfin
- $u'(x) = \dfrac{-n}{x^{n+1}}$ sur $\mathbb R^*$

Conclusion

$$\forall x\in \mathbb R^{*} \quad \left(\dfrac1{x^n}\right)' = \dfrac{-n}{x^{n+1}}$$

!!! danger "Remarques pour l'avenir"
    1.  La dernière s'écrit aussi $(x^{-n})' = -nx^{-n-1}$
    2.  Si on note $\sqrt{x} = x^{\frac12}$ et $\dfrac1{\sqrt x} = x^{-\frac12}$, alors
        $\left(\sqrt{x}\right)' = (x^{\frac12})' = \frac12x^{\frac12 - 1} = \dfrac1{2\sqrt x}$
    3.  Plus tard vous verrez que $(x^\alpha)' = \alpha x^{\alpha -1}$ pour $\alpha \in \mathbb R^*$, ce qui signifie qu'il n'y a **qu'une seule formule à retenir**.

### Inverse d'une fonction

On suppose que $u$ est dérivable sur $I$ et ne s'y annule pas.

On pose $v(x) = \dfrac1{u(x)}$ ; Objectif déterminer $v'(x)$. (On sait que $v(x)$ est bien définie)

On pose $f(x) = u(x) × v(x) = 1$, donc la dérivée de $f$ est nulle.

- $f'(x) = u'(x)×v(x) + u(x)×v'(x)$
- $(1)' = u'(x)×\dfrac1{u(x)} + u(x)×v'(x)$
- $0 = \dfrac{u'(x)}{u(x)^2} + v'(x)$ ; on a tout divisé par $u(x)$ qui est non nul.
- $v'(x) = \dfrac{-u'(x)}{u(x)^2}$

Conclusion

$$\left(\dfrac1u\right)' = \dfrac{-u'}{u^2}$$

!!! example "Exemple"
    $f(x) = \dfrac1{3x^2+1}$, avec $u(x) = 3x^2+1$, $u'(x) = 6x$.

    $f'(x) = \dfrac{-6x}{(3x^2+1)^2}$ ; définie sur $\mathbb R$

### Quotient de fonctions

Si $u$ et $v$ sont dérivables sur $I$, alors 

- $\dfrac{u}{v}$ est dérivable sur $I$ privé des points où $v$ s'annule,
- $\left(\dfrac u v\right)' = \dfrac{u'×v - u×v'}{v^2}$

!!! example "Exemple"
    Pour $x\in \mathbb R$, et avec

    $$\begin{align*}
    u(x) &= x^3 - 2  & u'(x) &= 3x^2\\
    v(x) &= 5x^2+7   & v'(x) &= 5x\\
    \end{align*}$$
    
    Ici $v$ ne s'annule jamais, on déduit

    $$\begin{align*}
    \left(\dfrac{u(x)}{v(x)}\right)' &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v(x)^2}\\
    \left(\dfrac{u(x)}{v(x)}\right)' &= \dfrac{3x^2×(5x^2+7) - (x^3-2)×5x}{(5x^2+7)^2}\\
    \end{align*}$$

    que l'on pourrait ensuite réduire...

### Preuve de $(x^n)' = nx^{n-1}$

On a démontré la formule pour $n=1$ et $n=2$.

On pourrait la démontrer pour $n=3$, puis $n=4$, puis... et ça prendrait du temps. L'infini, c'est long, surtout vers la fin...

!!! example "Cas `n = 3`"
    Sur $\mathbb R$, on a :
    
    - $(x^3)' = (x^2 × x)'$
    - $(x^3)' = (x^2)' × x + (x^2)×(x)'$
    - $(x^3)' = 2x × x + (x^2)×1$
    - $(x^3)' = 2x^2 + x^2$
    - $(x^3)' = 3x^2$

!!! example "Cas `n = 4`"
    Sur $\mathbb R$, on a :
    
    - $(x^4)' = (x^3 × x)'$
    - $(x^4)' = (x^3)' × x + (x^3)×(x)'$
    - $(x^4)' = 3x^2 × x + (x^3)×1$
    - $(x^4)' = 3x^3 + x^3$
    - $(x^4)' = 4x^3$

On pourrait continuer longtemps...

On suppose qu'on a réussi à démontrer la formule pour une certaine valeur de $n>1$. Mais elle n'est pas encore démontrée pour $n+1$ ; **faisons-le** ! On espère trouver $(x^{n+1})' = (n+1)x^{n+1-1} = (n+1)x^n$ ; c'est notre objectif.

On sait, par hypothèse, que $(x^n)' = nx^{n-1}$

On pose

$$\begin{align*}
u(x) &= x^n & u'(x) &= nx^{n-1}\\
v(x) &= x   & v'(x) &= 1\\
\end{align*}$$

On pose $f(x) = u(x) × v(x) = x^{n+1}$ ; **Objectif** : trouver $f'$.

$$\begin{align*}
f'(x) &= u'(x)×v(x) + u(x)×v'(x)\\
f'(x) &= (nx^{n-1})×x + (x^n)×1\\
f'(x) &= nx^n + x^n\\
f'(x) &= (n+1)x^n\\
&\text{Victoire !}
\end{align*}$$


Résumé :

1. On sait que la formule est vraie pour $n=1$ ; c'est facile.
2. On sait que si elle est vraie pour $n\in\mathbb N^*$, alors elle est aussi vraie pour $n+1$

On déduit par récurrence que le formule est vraie pour tout $n\in\mathbb N^*$.

!!! info "Démonstration par récurrence"
    De 1, on passe automatiquement à 2, puis 3, puis 4, ... puis autant qu'on veut. C'est démontré sans limite ! Cette méthode de démonstration est **très utilisée**. On parle de démonstration par récurrence.


### Composée d'affine

$$f(x) = g(mx+p)$$

Montrons d'abord ce qu'est une composée d'affine, sur des exemples.

!!! example "Exemple 1"
    $x\mapsto (11x-3)^{13}$ est la composée de $x\mapsto 11x-3$ par $t\mapsto t^{13}$.
    
    Ici $f(x)=(11x-3)^{13}$ et $g(t) = t^{13}$,
    
    $f$ est définie sur $\mathbb R$

!!! example "Exemple 2"
    $x\mapsto \sqrt{5x+8}$ est la composée de $x\mapsto 5x+8$ par $t\mapsto \sqrt t$.
    
    Ici $f(x)=\sqrt{5x+8}$ et $g(t) = \sqrt t$,

    $f$ est définie pour $5x+8 \geqslant 0$


En général

$x\mapsto g(mx+p)$ est la composée de $x\mapsto mx+p$, par $g$.

$f$ est définie pour $mx+p \in \mathcal D_g$


Dérivation

On note $f(x) = g(mx+p)$ sur $\mathcal D_f$, 

si $g$ est dérivable en $mx+p$, on a

$$f'(x) = mg'(mx+p)$$


!!! example "Exemple 1"
    $f(x)=(11x-3)^{13}$ avec $g(t) = t^{13}$, $g'(t) = 13t^{12}$
    
    $f'(x) = 11×\left(13(11x-3)^{12}\right)$ d'où

    $f'(x) = 143×(11x-3)^{12}$ sur $\mathbb R$

!!! example "Exemple 2"
    $f(x)=\sqrt{5x+8}$ avec $g(t) = \sqrt t$, $g'(t) = \dfrac1{2\sqrt t}$

    $f'(x) = 5×\left(\dfrac1{2\sqrt {5x+8}}\right)$

    $f'$ est définie pour $5x+8 > 0$


!!! danger "Preuve de la formule"
    **Totalement hors programme.**

    On suppose $g$ dérivable en $b=ma+p$.

    Pour $t=mx+p$, si $x$ est proche de $a$, alors $t$ est proche de $b$.

    $$\begin{align*}
    g(t) &\approx g(b) + g'(b)×(b-t)\\
    g(mx+p) &\approx g(ma+p) + g'(ma+p)×\left((mx+p)-(ma+p)\right)\\
    g(mx+p) &\approx g(ma+p) + g'(ma+p)×\left(mx+p-ma-p\right)\\
    g(mx+p) &\approx g(ma+p) + g'(ma+p)×\left(m(x-a)\right)\\
    g(mx+p) &\approx g(ma+p) + m×g'(ma+p)×(x-a)\\
    f(x) &\approx f(a) + m×g'(ma+p)×(x-a)\\
    \end{align*}$$

    On déduit $f'(a) = m×g'(ma+p)$, valable pour tout $a$ tel que $g$ est dérivable en $ma+p$.

    On peut le réécrire $f'(x) = m×g'(mx+p)$, valable pour tout $x$ tel que $g$ est dérivable en $mx+p$.

