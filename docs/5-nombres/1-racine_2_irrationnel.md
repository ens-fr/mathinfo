# Preuve que racine de deux est irrationnel


## Vocabulaire

1. Une **écriture fractionnaire** est un quotient, comme $\dfrac{x}{\sqrt 5 + 2}$
2. Une **fraction** est un quotient entre deux entiers : le numérateur entier et le dénominateur non nul, comme $\dfrac{22}7$. 
3. **Ratio** est aussi un nom pour désigner un quotient. L'adjectif **rationnel** désigne un nombre qui est une fraction.
4. **Irrationnel** désigne un nombre qui ne peut pas s'écrire sous forme de fraction.

!!! example "Exemples de nombres rationnels"
    $$\begin{align}
    &\frac{22}7\\
    0.3=&\frac3{10}\\
    42=&\frac{42}1\\
    -123.45=&\frac{-12345}{100}\\
    \end{align}$$

On peut écrire les inclusions

$$\mathbb N \subset \mathbb Z \subset \mathbb D \subset \mathbb Q \subset \mathbb R$$

Où :

- $\mathbb N$ est l'ensemble des entiers **n**aturels : $\{0, 1, 2, 3, 4, 5, \cdots\}$
- $\mathbb Z$ est l'ensemble des entiers relatifs : $\{\cdots, -3, -2, -1, 0, +1, +2, +3, ...\}$
- $\mathbb D$ est l'ensemble des nombres **d**écimaux : $\{\cdots, -3.1, -2.7, -1.825, -1, 0, +0.0078, +2.007, +3.141592, +4000, ...\}$
- $\mathbb Q$ est l'ensemble des nombres rationnels : $\{\cdots, -3.1, -\frac{27}{10}, \frac{-73}{40}, 0, \frac{1}3, \frac{22}7, +4000, ...\}$


!!! info "Pourquoi $\mathbb Z$ et $\mathbb Q$"
    - _**Z**ahl_ en allemand signifie entier ; il y a eu de grands mathématiciens allemands, le plus connu est Carl Frederich Gauss.
    - **Q**uotient, pour les nombres rationnels.

!!! question "Les nombres suivants sont-ils rationnels ?"
    ??? success "Le nombre $A = 0.0042$"
        $A = \frac{42}{10000}$, ainsi $A\in\mathbb Q$.

        Tout nombre décimal est rationnel.

    ??? success "Le nombre $B = \left(\dfrac1{\sqrt2}\right)^6$"
        $B = \left(\left(\dfrac1{\sqrt2}\right)^2\right)^3 = \left(\frac12\right)^3=\frac18$, ainsi $B\in\mathbb Q$.
    ??? success "Le nombre $C = q_1 × q_2$, avec $q_1, q_2\in \mathbb Q$"
        Si $q_1, q_2\in \mathbb Q$, alors $q_1 = \frac a b$ et $q_2 = \frac c d$ avec $a, c\in \mathbb Z$ et $b, d\in \mathbb N^*$

        On a $C = q_1 × q_2 = \frac a b × \frac c d = \frac{a×c}{b×d}$, avec $a×c\in\mathbb Z$ et $b×d \in\mathbb N^*$, ainsi $C\in\mathbb Q$.
    ??? success "Le nombre $D = q_1 + q_2$, avec $q_1, q_2\in \mathbb Q$"
        Si $q_1, q_2\in \mathbb Q$, alors $q_1 = \frac a b$ et $q_2 = \frac c d$ avec $a, c\in \mathbb Z$ et $b, d\in \mathbb N^*$

        On a
        
        $$\begin{align*}
        D &= q_1 + q_2\\
        D &= \frac a b + \frac c d\\
        D &= \frac{a×d}{b×d} + \frac{b×c}{b×d}\\
        D &= \frac{a×d + b×c}{b×d}\\
        \end{align*}$$
        
        avec $(a×d+b×c)\in\mathbb Z$ et $b×d \in\mathbb N^*$, ainsi $D\in\mathbb Q$.


!!! info "Nombres rationnels et décimaux"
    Tout nombre décimal est rationnel, en effet un nombre décimal peut s'écrire $\frac{a}{10^k}$ où $a\in\mathbb Z$ et $k\in\mathbb N$, c'est aussi une fraction !

    La réciproque est fausse, par exemple, $\frac13 = 0.333333333\cdots$ n'est pas décimal.

    Propriété
    : Une fraction irréductible $\frac a b$ est un nombre décimal si la décomposition en facteurs premiers de $b$ ne fait intervenir que les facteurs $2$ et $5$.

!!! question "Les nombres suivants sont-ils décimaux ?"
    ??? success "Le nombre $A = 0.0042$"
        $A = \frac{42}{10^4}$, ainsi $A$ est décimal.
    ??? success "Le nombre $B = \frac{-73}{40}$"
        $B = \frac{-73}{40}$ et $\frac{-73}{40}$ est irréductible et $40 = 2^3×5^1$, ainsi $B$ est décimal.
    ??? success "Le nombre $C = \frac{21}{15}$"
        $C = \frac{21}{15}=\frac{7}{5}$ et $\frac{7}{5}$ est irréductible avec $5 = 2^0×5^1$, ainsi $C$ est décimal.
    ??? success "Le nombre $D = \frac{22}{15}$"
        $D = \frac{22}{15}$ et $\frac{22}{15}$ est irréductible avec $15 = 3^1×5^1$, ainsi $D$ **n'est pas** décimal.


## Carré et parité

Montrons d'abord qu'un entier et son carré ont la même parité.

1. Soit $n$ un entier pair, on peut l'écrire $n = 2k$ avec $k\in\mathbb N$.
On a $n^2 = (2k)^2 = 2×(2k^2)$, avec $2k^2\in\mathbb N$, ainsi $n^2$ est pair.
2. Soit $n$ un entier impair,  on peut l'écrire $n = 2k+1$ avec $k\in\mathbb N$.
On a $n^2 = (2k+1)^2 = (2k)^2+2×2k×1+1^2 = 2(2k^2+2k) + 1$, avec $2k^2+2k\in\mathbb N$, ainsi $n^2$ est impair.

Réciproquement, on déduit que pour $n\in\mathbb N$.

1. si $n^2$ est pair, $n$ ne peut pas être impair, il est donc pair.
1. si $n^2$ est impair, $n$ ne peut pas être pair, il est donc impair.

!!! success "Conclusion"
    Un entier et son carré ont la même parité.


## Irrationalité de $\sqrt2$

Raisonnons par l'absurde.

!!! fail "On suppose que $\sqrt 2 \in \mathbb Q$"
    On peut alors écrire, $\sqrt 2 = \frac a b$, avec $a, b\in\mathbb N^*$. Quitte à utiliser l'algorithme d'Euclide, pour simplifier cette fraction, on peut supposer que $\frac a b$ est une fraction irréductible.

    On a

    - $\sqrt 2 = \frac a b$, donc $2 = \frac{a^2}{b^2}$, et donc $2b^2 = a^2$, donc $a^2$ est pair, donc $a$ est aussi pair.
    - On peut alors écrire $a = 2k$ où $k\in\mathbb N^*$, de sorte que $2b^2=a^2$ s'écrit $2b^2 = (2k)^2$, donc $2b^2 = 4k^2$, donc $b^2 = 2k^2$, donc $b^2$ est pair, et $b$ est donc aussi pair.

    On obtient une contradiction : $a$ et $b$ sont pairs, mais la fraction $\frac a b$ était réputée irréductible.

    Notre supposition initiale est donc absurde.

En conclusion, $\sqrt 2$ est irrationnel.

$$\sqrt 2 \notin \mathbb Q$$
