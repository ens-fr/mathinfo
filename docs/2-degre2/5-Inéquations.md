# Inéquations et radicaux

!!! info "Exemples traités ici"

    $$\left(\mathscr E_0\right) \qquad \sqrt{x+2} < \dfrac x3 + 1$$

    $$(\mathscr E_1) \qquad \sqrt{-x^2+2x}>\sqrt{x^2-x-1}$$
    
    $$\left(\mathscr E_2\right) \qquad 2x+1>\sqrt{x^2+1}$$

    $$\left(\mathscr E_3\right) \qquad x-3>\sqrt{x^2-2x}$$

    $$\left(\mathscr E_4\right) \qquad \sqrt{3-2x} \leqslant \sqrt{2x-x^2}$$
    
    $$\left(\mathscr E_5\right) \qquad x<\sqrt{x^2+x-6}$$

    $$\left(\mathscr E_6\right)\qquad \dfrac{x+4}{2\sqrt{x+1}} < \dfrac{x+1}{\sqrt{2x-1}}$$

    !!! tip "Indice pour $\left(\mathscr E_6\right)$"
        Il pourra être utile de factoriser le polynôme $P(x) = 2x^3 -3x^2 -12x +20$


## Prérequis

- [x] Savoir résoudre des inéquations classiques.
- [x] Savoir factoriser un trinôme du second degré et d'autres polynômes simples.
- [x] Savoir travailler avec le sens de variation des fonctions carrée et racine carrée.

**Objectif**

- [ ] Être capable de comprendre la suite et savoir le refaire sans apprendre par cœur.

### Rappels utiles

#### Résoudre une inéquation

Les règles sont les mêmes que pour les équations entre deux membres :

- On a le droit d'ajouter un même terme dans chaque membre.
- On a le droit de soustraire un même terme dans chaque membre.
- On a le droit de multiplier chaque membre entièrement par un même facteur **non nul**.
- On a le droit de diviser chaque membre entièrement par un même facteur **non nul**.

:warning: La seule différence étant que **si on multiplie ou si on divise un membre par un facteur négatif, alors on change le sens le l'inégalité**.

#### Factoriser un trinôme du second degré

Le trinôme $P(x) = ax^2+bx+c$, avec $a, b, c \in \mathbb R$ et $a\neq0$ possède un discriminant $\Delta = b^2-4ac$.

- Si $\Delta < 0$, on ne peut pas factoriser $P(x)$ dans $\mathbb R$.
- Si $\Delta = 0$, on a une écriture factorisée $P(x) = a\left(x - \dfrac{-b}{2a}\right)^2$
- Si $\Delta > 0$, on a une écriture factorisée $P(x) = a\left(x - \dfrac{-b-\sqrt\Delta}{2a}\right)\left(x - \dfrac{-b+\sqrt\Delta}{2a}\right)$

#### Sens de variation et carré

Sur $\mathbb R^+$ les fonctions $x\mapsto x^2$ et $x \mapsto \sqrt x$ sont croissantes, ce qui signifie que :

$$\text{Avec } x,y \in \mathbb R^+\quad \quad
x < y \iff x^2 < y^2$$

$$\text{Avec } x,y \in \mathbb R^+\quad \quad
x < y \iff \sqrt x < \sqrt y$$

:warning: On a aussi la version avec inégalités larges.

:boom: :boom: :boom: Ici $x$ et $y$ sont **positifs**. :boom: :boom: :boom:

## Les cas typiques

### Une seule racine carrée

#### Cas 1 : √(A) ≤ B

Équation de la forme

$$\sqrt A \leqslant B$$

L'ensemble de définition sera l'intersection :

- de l'ensemble de définition de $A$
- de l'ensemble de définition de $B$
- de l'ensemble des solutions de $A \geqslant 0$

Ensuite, on peut déduire que les solutions sont avec $B \geqslant 0$. Et en utilisant que les fonctions $x\mapsto x^2$ et $x \mapsto \sqrt x$ sont croissantes sur $\mathbb R^+$, on déduit :

!!! info "Une racine inférieure à ..."
    $$\sqrt A \leqslant B \iff \begin{cases}
    A \geqslant 0\\
    B \geqslant 0\\
    A \leqslant B^2
    \end{cases}$$

    De même, avec l'inégalité stricte

    $$\sqrt A < B \iff \begin{cases}
    A \geqslant 0\\
    B > 0\\
    A < B^2
    \end{cases}$$

    :warning: On note que dans l'inégalité stricte $A$ peut être nul, mais pas $B$.

!!! tip "Exemple simple"
    L'inéquation $\left(\mathscr E_0\right) \qquad \sqrt{x+2} < \dfrac x3 + 1$ est équivalente à

    $$\begin{cases}
    x+2 \geqslant 0\\
    \dfrac x3 + 1 > 0\\
    x+2 < \left(\dfrac x3 + 1\right)^2
    \end{cases}$$

    Elle-même équivalente aux systèmes suivants

    $$\begin{cases}
    x \geqslant -2\\
    x > -3\\
    9x+18 < x^2 + 6x + 9
    \end{cases}$$

    $$\begin{cases}
    x \geqslant -2\\
    x^2 -3x -9 > 0
    \end{cases}$$

    $$\begin{cases}
    x \geqslant -2\\
    x^2 -3x -9 > 0
    \end{cases}$$

    Après calculs, la deuxième inégalité est équivalente à $x \in \left]-\infty ; \dfrac{3-3\sqrt5}2\right[ \cup \left]\dfrac{3+3\sqrt5}2 ; +\infty\right[$, pour un bilan en ajoutant des deux contraintes du système :

    $$x \in \left[-2 ; \dfrac{3-3\sqrt5}2\right[ \cup \left]\dfrac{3+3\sqrt5}2 ; +\infty\right[$$

#### Cas 2 : √(A) ≥ B

Équation de la forme

$$\sqrt A \geqslant B$$

L'ensemble de définition sera l'intersection :

- de l'ensemble de définition de $A$
- de l'ensemble de définition de $B$
- de l'ensemble des solutions de $A \geqslant 0$

Ensuite, on peut déduire qu'il y a deux situations :

- les solutions avec $B < 0$. Là, $A$ peut être positif quelconque.
- les solutions avec $B \geqslant 0$. Et en utilisant que les fonctions $x\mapsto x^2$ et $x \mapsto \sqrt x$ sont croissantes sur $\mathbb R^+$, on déduit :

!!! info "Une racine supérieure à ..."

    $$\sqrt A \geqslant B \iff
    \begin{cases}
    B < 0\\
    A \geqslant 0\\
    \end{cases}
    \quad \text{ ou bien } \quad
    \begin{cases}
    B \geqslant 0\\
    A \geqslant 0\\
    A \geqslant B^2
    \end{cases}$$

    De même, avec l'inégalité stricte

    $$\sqrt A > B \iff
    \begin{cases}
    A \geqslant 0\\
    B < 0
    \end{cases}
    \quad \text{ ou bien } \quad
    \begin{cases}
    A \geqslant 0\\
    B \geqslant 0\\
    A > B^2
    \end{cases}$$

    :warning: Il faut résoudre les deux situations. Pour chaque situation, les **contraintes** s'ajoutent. À la fin, on ajoute les **solutions**.

### Deux racines carrées

#### √(A) ≤ √(B)

Équation de la forme

$$\sqrt A \leqslant \sqrt B$$

L'ensemble de définition sera l'intersection :

- de l'ensemble de définition de $A$
- de l'ensemble de définition de $B$
- de l'ensemble des solutions de $A \geqslant 0$
- de l'ensemble des solutions de $B \geqslant 0$

Ensuite, on peut déduire que les solutions sont avec $A \geqslant 0$ et $B \geqslant 0$. Et en utilisant que les fonctions $x\mapsto x^2$ et $x \mapsto \sqrt x$ sont croissantes sur $\mathbb R^+$, on déduit :

!!! info "Deux racines à comparer ..."
    $$\sqrt A \leqslant \sqrt B \iff \begin{cases}
    A \geqslant 0\\
    B \geqslant 0\\
    A \leqslant B
    \end{cases}$$

    De même, avec l'inégalité stricte

    $$\sqrt A < \sqrt B \iff \begin{cases}
    A \geqslant 0\\
    B > 0\\
    A < B
    \end{cases}$$

    :warning: On note que dans l'inégalité stricte $A$ peut être nul, mais pas $B$.

## Exemple complet n°1

$$(\mathscr E_1) \qquad \sqrt{-x^2+2x}>\sqrt{x^2-x-1}$$

### Ensemble de définition

L'ensemble de définition de l'inéquation $(\mathscr E_1)$ est donné par les contraintes : $-x^2+2x \geqslant 0$ et $x^2-x-1 \geqslant 0$.

* Pour la première, une simple factorisation par $x$ donne $x(-x+2) \geqslant 0$, et avec un tableau de signe, on obtient la contrainte $x \in [0 ; 2]$.

* Pour la seconde, le discriminant du trinôme est $\Delta = (-1)^2 - 4\times1\times(-1) = 5>0$.  
Une fois étudié, on a 
$x \in \left]-\infty ; \dfrac{1-\sqrt5}{2}\right] \cup \left[\dfrac{1+\sqrt5}{2} ; +\infty\right[$.

Ces deux contraintes donnent, par intersection, $\left[\dfrac{1+\sqrt5}{2} ; 2\right]$ comme ensemble de définition.

### Représentation graphique

On représente
 $x\mapsto \sqrt{-x^2+2x}$ (en bleu), et $x\mapsto \sqrt{x^2-x-1}$ (en rouge).

Cela nous donnera une idée des solutions, sans être une preuve ; c'est une bonne pratique.

![](./assets/in%C3%A9q1a.png)

On vérifie que notre ensemble de définition est environ $[1.6 ; 2]$ ; là où _bleu_ et _rouge_ existent.

Les solutions étant là où _bleu_ est au-dessus de _rouge_.
Zoomons dans cet intervalle.

![](./assets/in%C3%A9q1b.png)

Une lecture graphique nous indique des solutions pour $x \in [1.62 ; 1.78[$ environ.

**Ce n'est pas une preuve**, juste une indication. Faisons maintenant la résolution.

### Résolution

Une inéquation de la forme $\sqrt{A}>\sqrt{B}$ est équivalente à :

$$A\geqslant 0 ; B\geqslant 0 ; A > B$$

En effet, on rappelle que

- $A\geqslant 0$ et $B\geqslant 0$ sont là pour la définition des racines carrées.
- Ensuite, les fonctions $x\mapsto x^2$ et $x\mapsto \sqrt x$ sont strictement croissantes sur $[0\,;\,+\infty[$

Les deux premières conditions nous ont déjà donné l'ensemble de définition.

Reste à résoudre : $-x^2+2x>x^2-x-1$, qui donne après calculs :
$x \in \left]\dfrac{3-\sqrt{17}}{4} ; \dfrac{3+\sqrt{17}}{4}\right[$.

!!! tip "Et en gros ?"
    On donne des valeurs approchées utiles : 

    * $\dfrac{1+\sqrt5}{2} \approx 1.618$ ;  
    * $\dfrac{3-\sqrt{17}}{4}\approx -0.281$ ;  
    * $\dfrac{3+\sqrt{17}}{4}\approx 1.781$ .

!!! done "On assemble les contraintes"
    Les solutions de l'inéquation $(\mathscr E_1)$ sont : $x \in \left[\dfrac{1+\sqrt5}{2} ; \dfrac{3+\sqrt{17}}{4}\right[$.

!!! tip "Vérification avec Python"
    Ce n'est pas une preuve, mais vérifions ce résultat avec Python.

    ```python
    from math import sqrt

    def gauche(x):
        "Définition du membre de gauche de l'inéquation"
        return sqrt(-x*x +2*x)
    
    def droite(x):
        "Définition du membre de droite de l'inéquation"
        return sqrt(x*x -x -1)
    
    for i in range(1618, 1781):
        x = i / 1000  # x vaut de 1.618 à 1.780 par pas de 0.001
        assert gauche(x) > droite(x), f"Erreur avec x = {x}"
    ```

    L'exécution de ce script ne provoque par d'erreur. L'intervalle `[1.618 ; 1.781[` semble correct.

    ??? danger "Ci-dessous est hors programme"
        Pour tester que l'inégalité est invalide en dehors, c'est bien plus délicat. Soit elle n'est pas définie, soit elle est fausse. La méthode est hors programme.

        ```python
        from math import sqrt

        def gauche(x):
            "Définition du membre de gauche de l'inéquation"
            return sqrt(-x*x +2*x)
        
        def droite(x):
            "Définition du membre de droite de l'inéquation"
            return sqrt(x*x -x -1)
        
        for a, b in [(-5000, 1618), (1782, 5000)]:
            for i in range(a, b):
                x = i / 1000
                # x vaut de -5.000 à 1.617 par pas de 0.001
                #      puis de 1.782 à 4.999 par pas de 0.001
                try:
                    assert gauche(x) <= droite(x), f"Erreur avec x = {x}"
                    # le calcul a réussi et l'inégalité était bien fausse
                except:
                    pass
                    # le calcul n'a pas abouti :
                    #   - division par zéro
                    #   - ou racine carrée de nombre négatif
                    #   - ou peu importe...
        ```

## Exemple complet n°2

$$\left(\mathscr E_2\right) \qquad 2x+1>\sqrt{x^2+1}$$

### Ensemble de définition

L'ensemble de définition de l'inéquation $(\mathscr E_2)$ est donné par la contrainte $x^2+1 \geqslant 0$. Soit $x \in \mathbb{R}$, *après un calcul simple*.

### Représentation graphique

![](./assets/in%C3%A9q2a.png)

On constate que les solutions semblent être : $x \in ]0 ; +\infty[$.

### Résolution

Une équation de la forme $A>\sqrt{B}$ est équivalente à :

$$A>0 ; B\geqslant0 ; A^2>B$$

* La première contrainte est $2x+1>0$, soit $x> \dfrac{-1}{2}$.

* La deuxième contrainte est $x^2+1\geqslant 0$, toujours réalisée, soit $x \in \mathbb{R}$.

* La troisième contrainte est $(2x+1)^2 > x^2+1$, qui se ramène, après calculs, à :
    * $3x^2+4x>0$, puis sous forme factorisée :
    * $x(3x+4)>0$ dont les solutions sont :
    * $x \in \left]-\infty ; \dfrac{-4}{3}\right[ \cup ]0 ; +\infty[$.


!!! done "Bilan"
    On assemble les trois contraintes.

    Les solutions de l'inéquation $(\mathscr E_2)$ sont : $x \in ]0 ; +\infty[$.

## Exemple complet n°3

$$\left(\mathscr E_3\right) \qquad x-3>\sqrt{x^2-2x}$$

### Ensemble de définition

L'ensemble de définition de l'équation $\left(\mathscr E_3\right)$ est donné par la contrainte $x^2-2x \geqslant 0$.

Factorisée, elle s'écrit aussi $x(x-2)\geqslant 0$, ou encore $x \in ]-\infty ; 0] \cup [2 ; +\infty[$.

### Représentation graphique

![](./assets/in%C3%A9q3a.png)

Il semble n'y avoir aucune solution.

### Résolution

Une inéquation de la forme $A>\sqrt{B}$ est équivalente à :

$$A>0 ; B\geqslant0 ; A^2>B$$

* La première contrainte est $x-3 > 0$, soit $x>3$.

* La deuxième contrainte est $x^2-2x \geqslant 0$, soit $x \in ]-\infty ; 0] \cup [2 ; +\infty[$.

* La troisième contrainte est $(x-3)^2 > x^2-2x$, qui se ramène à $-4x+9>0$, c'est-à-dire $x<\dfrac{9}{4}$.

!!! done "Bilan"
    $\dfrac{9}{4}<3$, on en conclut que la première et troisième contrainte excluent toute solution.

    Il n'y a aucune solution à cette inéquation $\left(\mathscr E_3\right)$.

## Exemple complet n°4

$$\left(\mathscr E_4\right) \qquad \sqrt{3-2x} \leqslant \sqrt{2x-x^2}$$

### Ensemble de définition

L'ensemble de définition de l'inéquation $\left(\mathscr E_4\right)$ est donné par les contraintes : $3-2x \geqslant 0$ et $2x-x^2 \geqslant 0$.

* La première impose $x \in \left]-\infty ; \dfrac{3}{2}\right]$.

* La seconde impose $x \in [0 ; 2]$.

Ce qui nous donne $\left[0 ; \dfrac{3}{2}\right]$ comme ensemble de définition.

### Représentation graphique

On représente $x\mapsto \sqrt{3-2x}$ (en bleu), et $x\mapsto \sqrt{2x-x^2}$ (en rouge).

![](./assets/in%C3%A9q4a.png)

Il semble y avoir des solutions dans $[1 ; 1.5]$, environ.

### Résolution

Une équation de la forme $\sqrt{A} \leqslant \sqrt{B}$ est équivalente à :

$$A\geqslant0 ; B\geqslant0 ; A \leqslant B$$

Les deux premières contraintes nous ont donné l'ensemble de définition.

* La troisième contrainte est $3-2x \leqslant 2x-x^2$,
* qui s'écrit aussi $x^2-4x+3\leqslant0$,
* équivalente à $(x-3)(x-1)\leqslant0$,
* et donc à $x \in [1 ; 3]$.

!!! done "Bilan"
    Les solutions de l'inéquation $\left(\mathscr E_4\right)$ sont :
    $x \in \left[1 ; \dfrac{3}{2}\right]$.

## Exemple complet n°5

$$\left(\mathscr E_5\right) \qquad x<\sqrt{x^2+x-6}$$

### Ensemble de définition

L'ensemble de définition de l'équation $\left(\mathscr E_5\right)$ est donné par la contrainte $x^2+x-6 \geqslant 0$.

* Factorisée, elle s'écrit aussi $(x-2)(x+3)\geqslant 0$,
* ou encore $x \in ]-\infty ; -3] \cup [2 ; +\infty[$.

### Représentation graphique

![](./assets/in%C3%A9q5a.png)

Et zoomons entre $4$ et $8$ :

![](./assets/in%C3%A9q5b.png)

Il semble y avoir des solutions pour $x \in ]-\infty; -3] \cup ]6 ; +\infty[$, environ.

### Résolution

Une équation de la forme $A<\sqrt{B}$ est équivalente à :

$$A\geqslant0 ; B\geqslant0 ; A^2 < B$$

<center>**ou bien à**</center>

$$A<0 ; B\geqslant0$$

!!! warning "Deux situations !"
    Il ne faut pas oublier les situations où $A$ peut être négatif.
    
    Il y a deux situations à traiter, on fera l'**union** des deux, et non l'intersection !

!!! info "Première situation"
    On a $x\geqslant0$, $x \in ]-\infty ; -3] \cup [2 ; +\infty[$ et une troisième contrainte $x^2 < x^2+x-6$.

    Cette dernière se ramène à $x>6$.

    L'intersection des trois contraintes nous donne $x \in ]6 ; +\infty[$.

!!! info "Seconde situation"
    On a $x<0$ et $x \in ]-\infty ; -3] \cup [2 ; +\infty[$, sans autre contrainte.

    L'intersection donne $x \in ]-\infty ; -3]$.

!!! done " Bilan"
    On fait la réunion des deux cas.
    
    Les solutions de l'inéquation $\left(\mathscr E_5\right)$ sont : $x\in ]-\infty ; -3]\cup]6 ; +\infty[$.

## Exemple complet n°6 : une inéquation sérieuse

Résolution dans $\mathbb R$ de :

$$\left(\mathscr E_6\right)\qquad \dfrac{x+4}{2\sqrt{x+1}} < \dfrac{x+1}{\sqrt{2x-1}}$$

!!! tip "Indice"
    Il pourra être utile de factoriser le polynôme $P(x) = 2x^3 -3x^2 -12x +20$

### Ensemble de définition

L'inéquation
$\left(\mathscr E_6\right)$ est définie si les deux radicandes sont positives et non nulles pour pouvoir ensuite faire la division :

* $x+1>0$, soit $x>-1$ ;  
* $2x-1>0$, soit $x>\frac{1}{2}$. Qui est plus contraignante.

L'ensemble de définition de $\left(\mathscr E_6\right)$ est : $x \in ]\frac{1}{2} ; +\infty[$.

### Résolution graphique

![](./assets/in%C3%A9q6a.png)

Il semble que les solutions soient : $x>\dfrac 1 2$, et $x\neq 2$.

:warning: Il n'y a rien à gauche de $\frac12$, mais la courbe se prolonge à droite à l'infini.

### Factorisation du polynôme donné en indice

On considère $P(x) = 2x^3 -3x^2 -12x +20$

On cherche une racine entière entre $-3$ et $3$.

* On peut regarder une représentation graphique,
* ou bien faire un script Python :

```python
def P(x):
    return 2*x**3 -3*x**2 -12*x +20

for i in range(-3, 4):
    print("L'image de", i, "par P est :", P(i))
```

```console
L'image de -3 par P est : -25
L'image de -2 par P est : 16
L'image de -1 par P est : 27
L'image de 0 par P est : 20
L'image de 1 par P est : 7
L'image de 2 par P est : 0
L'image de 3 par P est : 11
```

On constate que $2$ est une racine du polynôme $P(x)$.

Il s'écrit donc aussi $P(x) = (x-2)(ax^2 +bx +c)$.

En développant, on a : $P(x) = ax^3 +(b-2a)x^2 +(c-2b)x -2c$.  

Mais on a aussi : $\qquad P(x) = 2x^3 -3x^2 -12x +20$.

En identifiant les coefficients, on tire :

$$\begin{cases}
a = +2\\
b-2a = -3\\
c-2b = -12\\
-2c = +20
\end{cases}$$

On déduit, $a=2$, $c=-10$, et enfin $b=1$ de deux manières différentes.

Ainsi $P(x) = (x-2)(2x^2 +x -10)$.

Le discriminant du trinôme du second degré $(2x^2 +x -10)$ est $\Delta = 1^2 - 4\times2\times(-10) = 81$.

Avec $\sqrt{\Delta} = 9$, on peut écrire :

$$2x^2 +x -10 = 2(x-2)(x+\frac{5}{2})$$

Finalement $P(x) = 2(x-2)^2(x+\frac{5}{2})$ est entièrement factorisé.

### Résolution

Dans l'ensemble de définition, les dénominateurs de l'inéquation sont strictement positifs, on peut donc les multiplier tous deux dans chaque membre, pour obtenir :

$$\left(\mathscr E_6\right) \qquad (x+4)\sqrt{2x-1} < 2(x+1)\sqrt{x+1}$$

Dans l'ensemble de définition, on a aussi $x+1>0$ et $x+4>0$, de sorte que les deux membres $A$ et $B$ de l'inéquation de la forme $A < B$ sont déjà positifs. Dans ce cas, on a équivalence avec $A^2 < B^2$, ce qui nous donne :

* $(x+4)^2(2x-1) < 2^2(x+1)^2(x+1)$, qui une fois développée et réduite nous donne :
* $2x^3 -3x^2 -12x +20 > 0$, et on reconnait le polynôme de la question précédente que l'on a factorisé. Cela nous donne :
* $2(x-2)^2(x+\frac{5}{2}) > 0$

Un tableau de signe nous offre les solutions : $x>-\frac{5}{2}$, avec $x\neq 2$.

!!! done "Bilan"
    En tenant compte de l'ensemble de définition, les solutions de l'inéquation $\left(\mathscr E_6\right)$ sont :
    
     $x > \dfrac 1 2$, avec $x\neq 2$.
