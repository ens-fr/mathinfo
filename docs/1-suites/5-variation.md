# Variations d'une suite numérique

## Suite arithmétique

D'après les résultats sur les fonctions affines, on déduit immédiatement ce qui suit.

Avec pour tout $n\in\mathbb N$, $u_n = a\times n + b$ :

* Si $a > 0$, alors la suite $(u_n)$ est strictement croissante.
* Si $a < 0$, alors la suite $(u_n)$ est strictement décroissante.
* Si $a \geqslant 0$, alors la suite $(u_n)$ est croissante.
* Si $a \leqslant 0$, alors la suite $(u_n)$ est décroissante.
* Si $a = 0$, alors la suite $(u_n)$ est constante.

## Suite géométrique à termes positifs

Avec pour tout $n\in\mathbb N$, $v_n = v_0 \times q^n$ :

* Si $v_0>0$ et $1<q$, alors la suite $(v_n)$ est strictement croissante.
* Si $v_0>0$ et $0<q<1$, alors la suite $(v_n)$ est strictement décroissante.

!!! example "Remarques"
    * Si $q$ est négatif, les termes de la suite changent de signe alternativement.
    * Si $v_0$ est négatif, on étudie la suite $(-v_n)$ qui est géométrique et de premier terme positif.
    * Si $q=1$, la suite est constante, égale à $v_0$.
    * Si $q=0$, la suite est constante, nulle à partir du rang 1.

