# TD5 : Déterminants

## Exercice 1

$$A = \begin{pmatrix}
2 & 1 & 4\\
1 & 0 & 3\\
1 & 2 & -1\\
\end{pmatrix}
\quad ; \quad
B = \begin{pmatrix}
1 & 0 & 1 & 1\\
2 & 1 & -3 & 2\\
-1 & -1 & 1 & 1\\
1 & 2 & 2 & 1\\
\end{pmatrix}$$

1. Montrer que $A$ n'est pas inversible.
2. Montrer que $B$ est inversible.

??? success "Réponse pour $A$"
    On utilise la règle de Sarrus.

    $$\begin{align*}
    \det A &= +\left(2×0×(-1) + 1×2×4 + 1×1×3\right) - \left(4×0×1 + 3×2×2 + (-1)×1×1\right)\\
    \det A &= +(0+8+3) - (0+12-1)\\
    \det A &= 11 - 11\\
    \det A &= 0\\
    \end{align*}$$

    On déduit que $A$ n'est pas inversible.

??? success "Variante pour $A$"
    On développe par rapport à la deuxième ligne.

    $$\begin{align*}
    \det A &= (-1) × 1 × \det\begin{pmatrix}1&4\\2&-1\end{pmatrix} + (+1) × 0 × \det\begin{pmatrix}2&4\\1&-1\end{pmatrix}+ (-1) × 3 × \det\begin{pmatrix}2&1\\1&2\end{pmatrix}\\
    \det A &= (-1)×(-1-8) + 0 + (-3)×(4-1)\\
    \det A &= 9+0+(-9)\\
    \det A &= 0\\
    \end{align*}$$

    On déduit que $A$ n'est pas inversible.

??? success "Réponse pour $B$"

    On pourrait développer par rapport à la première ligne, on aurait alors trois déterminants $3×3$ à calculer. C'est un peu longuet. On va plutôt faire quelques opérations sur les lignes et les colonnes qui ne changent pas le déterminant.

    - $L_i ← L_i + \alpha×L_k$, avec $i\neq k$
    - $C_j ← C_j + \alpha×C_k$, avec $j\neq k$

    On cherche à placer plusieurs zéros de sorte à simplifier la fin des calculs.



    $$\begin{align*}
    \det B &= \begin{vmatrix}
    1 & 0 & 1 & 1\\
    2 & 1 & -3 & 2\\
    -1 & -1 & 1 & 1\\
    1 & 2 & 2 & 1\\
    \end{vmatrix} \quad C_3 ← C_3 - C_1\\
    \det B &= \begin{vmatrix}
    1 & 0 & 0 & 1\\
    2 & 1 & -5 & 2\\
    -1 & -1 & 2 & 1\\
    1 & 2 & 1 & 1\\
    \end{vmatrix} \quad C_4 ← C_4 - C_1\\
    \det B &= \begin{vmatrix}
    1 & 0 & 0 & 0\\
    2 & 1 & -5 & 0\\
    -1 & -1 & 2 & 2\\
    1 & 2 & 1 & 0\\
    \end{vmatrix} \quad \text{On développe par rapport à } L_1\\
    \det B &= +1×1×\begin{vmatrix}
    1 & -5 & 0\\
    -1 & 2 & 2\\
    2 & 1 & 0\\
    \end{vmatrix} \quad \text{On développe par rapport à } C_3\\
    \det B &= -2×\begin{vmatrix}
    1 & -5\\
    2 & 1\\
    \end{vmatrix} \quad \text{On calcule ce déterminant}\\
    \det B &= -2×(1×1 - (-5)×2)\\
    \det B &= -2×(1+10)\\
    \det B &= -22\\
    \det B &\neq 0\\
    \end{align*}$$

    On déduit que $B$ est inversible.


## Exercice 2

Soit $a, b ∈ \mathbb R$. Montrer que le déterminant de la matrice

$$A = \begin{pmatrix}
a & 0 & 0 & b\\
b & a & 0 & 0\\
0 & b & a & 0\\
0 & 0 & b & a\\
\end{pmatrix}$$

vérifie $\det(A) = a^4 − b^4$.

Déterminer ensuite le rang de $A$ en fonction de $a$ et $b$.

??? success "Réponse"
    On développe par rapport à la première ligne, on obtient

    $$\begin{align*}
    \det(A) &= (+1)×a×\begin{vmatrix}a & 0 & 0\\b & a & 0\\0 & b & a\\\end{vmatrix} + (-1)×b×\begin{vmatrix}b & a & 0\\0 & b & a\\0 & 0 & b\\\end{vmatrix}\\
    \det(A) &=a×a^3 - b×b^3\\
    \det(A) &=a^4 - b^4\\
    \end{align*}$$

    On déduit que 

    $$\begin{align*}
    \det(A) &= (a^2 - b^2)(a^2 + b^2)\\
    \det(A) &= (a-b)(a+b)(a^2+b^2)\\
    \end{align*}$$

    De sorte que si $a\neq b$ ou $a\neq -b$, on a $A$ inversible, donc de rang $4$. Il reste à étudier le cas $a=b$ et le cas $a=-b$.

    !!! info "Cas $a=b=0$"
        La matrice $A$ est nulle, et de rang $0$.

    !!! info "Cas $a=b\neq0$"
        La matrice $A$ peut être factorisée par $a$, elle est de même rang que les matrices suivantes :

        $$\begin{pmatrix}
        1 & 0 & 0 & 1\\
        1 & 1 & 0 & 0\\
        0 & 1 & 1 & 0\\
        0 & 0 & 1 & 1\\
        \end{pmatrix}\quad L_2 ← L_2 - L_1$$

        $$\begin{pmatrix}
        1 & 0 & 0 & 1\\
        0 & 1 & 0 & -1\\
        0 & 1 & 1 & 0\\
        0 & 0 & 1 & 1\\
        \end{pmatrix}\quad L_3 ← L_3 - L_2$$

        $$\begin{pmatrix}
        1 & 0 & 0 & 1\\
        0 & 1 & 0 & -1\\
        0 & 0 & 1 & 1\\
        0 & 0 & 1 & 1\\
        \end{pmatrix}\quad L_4 ← L_4 - L_3$$

        $$\begin{pmatrix}
        1 & 0 & 0 & 1\\
        0 & 1 & 0 & -1\\
        0 & 0 & 1 & 1\\
        0 & 0 & 0 & 0\\
        \end{pmatrix}$$

        Qui sont donc toutes de rang $3$.



    !!! info "Cas $a=-b\neq0$"
        La matrice $A$ peut être factorisée par $a$, elle est de même rang que les matrices suivantes :

        $$\begin{pmatrix}
        1 & 0 & 0 & -1\\
        -1 & 1 & 0 & 0\\
        0 & -1 & 1 & 0\\
        0 & 0 & -1 & 1\\
        \end{pmatrix} \quad L_2 ← L_2 + L_1$$

        $$\begin{pmatrix}
        1 & 0 & 0 & -1\\
        0 & 1 & 0 & -1\\
        0 & -1 & 1 & 0\\
        0 & 0 & -1 & 1\\
        \end{pmatrix}\quad L_3 ← L_3 + L_2$$

        $$\begin{pmatrix}
        1 & 0 & 0 & -1\\
        0 & 1 & 0 & -1\\
        0 & 0 & 1 & -1\\
        0 & 0 & -1 & 1\\
        \end{pmatrix}\quad L_4 ← L_4 + L_3$$

        $$\begin{pmatrix}
        1 & 0 & 0 & -1\\
        0 & 1 & 0 & -1\\
        0 & 0 & 1 & -1\\
        0 & 0 & 0 & 0\\
        \end{pmatrix}$$

        Qui sont donc toutes de rang $3$.


## Exercice 3

Calculer les déterminants suivants :

??? success "$\begin{vmatrix}1 & 2 & 3 \\2 & 3 & 1 \\3 & 1 & 2 \\\end{vmatrix}$"
    Méthode 1 : On développe par rapport à la première ligne.

    $$\begin{align*}
    d_1 &= \begin{vmatrix}1 & 2 & 3 \\2 & 3 & 1 \\3 & 1 & 2 \\\end{vmatrix}\\
    d_1 &= (+1)×1×\begin{vmatrix}3 & 1 \\1 & 2 \\\end{vmatrix} + (-1)×2×\begin{vmatrix}2 & 1 \\3 & 2 \\\end{vmatrix} + (+1)×3×\begin{vmatrix}2 & 3\\3 & 1\\\end{vmatrix}\\
    d_1 &= +(6-1) -2(4-3) +3(2-9)\\
    d_1 &= +5-2-21\\
    d_1 &= -18\\
    \end{align*}$$

    Méthode 2 : On réalise quelques opérations sur lignes et colonnes avant.

    $$\begin{align*}
    d_1 &= \begin{vmatrix}1 & 2 & 3 \\2 & 3 & 1 \\3 & 1 & 2 \\\end{vmatrix}\quad C_1 ← C_1 + C_2 + C_3\\
    d_1 &= \begin{vmatrix}6 & 2 & 3 \\6 & 3 & 1 \\6 & 1 & 2 \\\end{vmatrix}\quad\text{On enlève la ligne 1 aux suivantes}\\
    d_1 &= \begin{vmatrix}6 & 2 & 3 \\0 & 1 & -2 \\0 & -1 & -1 \\\end{vmatrix}\quad L_3 ← L_3 + L_2\\
    d_1 &= \begin{vmatrix}6 & 2 & 3 \\0 & 1 & -2 \\0 & 0 & -3 \\\end{vmatrix}\quad \text{La forme est triangulaire}\\
    d_1 &= 6×1×(-3)\\
    d_1 &= -18\\
    \end{align*}$$

??? success "$\begin{vmatrix}1 & 0 & 0 & 1 \\ 0 & 1 & 0 & 0 \\ 1 & 0 & 1 & 1 \\ 2 & 3 & 1 & 1 \\\end{vmatrix}$"
    Méthode 1 : On développe par rapport à la première ligne qui possède deux zéros. Il reste à calculer deux déterminants d'ordre 3.

    - Le premier est nul ; il possède deux colonnes identiques.
    - On développe le second par rapport à sa première ligne.

    $$\begin{align*}
    d_2 &= \begin{vmatrix}1 & 0 & 0 & 1 \\ 0 & 1 & 0 & 0 \\ 1 & 0 & 1 & 1 \\ 2 & 3 & 1 & 1 \\\end{vmatrix}\\
    d_2 &= (+1)×1×\begin{vmatrix}1 & 0 & 0 \\0 & 1 & 1 \\3 & 1 & 1 \\\end{vmatrix} + (-1)×1×\begin{vmatrix}0 & 1 & 0\\ 1 & 0 & 1\\ 2 & 3 & 1\\\end{vmatrix}\\
    d_2 &= -(-1)×1×\begin{vmatrix}1 & 1\\ 2 & 1\\\end{vmatrix}\\
    d_2 &= 1-2\\
    d_2 &= -1\\
    \end{align*}$$

    Méthode 2 : On réalise quelques opérations sur lignes et colonnes avant.

    $$\begin{align*}
    d_2 &= \begin{vmatrix}1 & 0 & 0 & 1 \\ 0 & 1 & 0 & 0 \\ 1 & 0 & 1 & 1 \\ 2 & 3 & 1 & 1 \\\end{vmatrix}\quad C_4 ← C_4 - C_1\\
    d_2 &= \begin{vmatrix}1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 1 & 0 & 1 & 0 \\ 2 & 3 & 1 & -1 \\\end{vmatrix}\quad \text{la forme est triangulaire}\\
    d_2 &= 1×1×1×(-1)\\
    d_2 &= -1\\
    \end{align*}$$

??? success "$\begin{vmatrix}a & b & c \\c & a & b \\b & c & a \\\end{vmatrix}$"

    Méthode 1 : On développe par rapport à la première ligne.

    $$\begin{align*}
    d_3 &= \begin{vmatrix}a & b & c \\c & a & b \\b & c & a \\\end{vmatrix}\\
    d_3 &= (+1)×a×\begin{vmatrix}a & b \\c & a \\\end{vmatrix} + (-1)×b×\begin{vmatrix}c & b \\b & a \\\end{vmatrix} + (+1)×c×\begin{vmatrix}c & a\\b & c\\\end{vmatrix}\\
    d_3 &= a(a^2-bc) -b(ac-b^2) +c(c^2-ab)\\
    d_3 &= a^3 -abc -abc + b^3 +c^3-abc\\
    d_3 &= a^3 + b^3 +c^3 - 3abc\\
    \end{align*}$$

    Méthode 2 : On réalise quelques opérations sur lignes et colonnes avant.

    $$\begin{align*}
    d_3 &= \begin{vmatrix}a & b & c \\c & a & b \\b & c & a \\\end{vmatrix}\quad C_1 ← C_1 + C_2 + C_3\\
    d_3 &= \begin{vmatrix}a+b+c & b & c \\a+b+c & a & b \\a+b+c & c & a \\\end{vmatrix}\quad\text{On factorise par }a+b+c\\
    d_3 &= (a+b+c)×\begin{vmatrix}1 & b & c \\1 & a & b \\1 & c & a \\\end{vmatrix}\quad\text{On enlève la ligne 1 aux suivantes}\\
    d_3 &= (a+b+c)×\begin{vmatrix}1 & b & c \\0 & a-b & b-c \\0 & c-b & a-c \\\end{vmatrix}\quad \text{On développe par rapport à la première ligne}\\
    d_3 &= (a+b+c)×(+1)×1×\begin{vmatrix}a-b & b-c \\c-b & a-c \\\end{vmatrix}\quad \text{On calcule ce déterminant}\\
    d_3 &= (a+b+c)×((a-b)×(a-c) + (b-c)^2) \\
    d_3 &= (a+b+c)×(a^2 -ab -ac +bc + b^2 -2bc +c^2) \\
    d_3 &= (a+b+c)×(a^2+b^2+c^2 -ab -ac -bc) \\
    d_3 &= (a^3+ab^2+ac^2 -a^2b -a^2c -abc)+(a^2b+b^3+bc^2 -ab^2 -abc -b^2c)+(a^2c+b^2c+c^3 -abc -ac^2 -bc^2) \\
    d_3 &= a^3 +b^3 + c^ 3 - 3abc +(ab^2+ac^2 -a^2b -a^2c+ a^2b+bc^2 -ab^2 -b^2c+a^2c+b^2c -ac^2 -bc^2) \\
    d_3 &= a^3 +b^3 + c^ 3 - 3abc \\
    \end{align*}$$

    Une expression factorisée a plus de valeur.

    $$d_3 = (a+b+c)(a^2+b^2+c^2 -ab -ac -bc)$$

    On peut encore travailler sur l'expression.

    $$d_3 = (a+b+c)((a+b+c)^2 -3(ab +ac +bc))$$


??? success "$\begin{vmatrix}1 & 2 & 1 & 2 \\ 1 & 3 & 1 & 3 \\ 2 & 1 & 0 & 6 \\ 1 & 1 & 1 & 7 \\\end{vmatrix}$"

    Il n'y a pas beaucoup de zéros, on cherche les deux lignes ou les deux colonnes les plus proches pour les soustraire.
    
    $$\begin{align*}
    d_4 &= \begin{vmatrix}1 & 2 & 1 & 2 \\ 1 & 3 & 1 & 3 \\ 2 & 1 & 0 & 6 \\ 1 & 1 & 1 & 7 \\\end{vmatrix}\quad C_1 ← C_1 - C_3\\
    d_4 &= \begin{vmatrix}0 & 2 & 1 & 2 \\ 0 & 3 & 1 & 3 \\ 2 & 1 & 0 & 6 \\ 0 & 1 & 1 & 7 \\\end{vmatrix}\quad\text{On développe par rapport à la première colonne}\\
    d_4 &= (+1)×2×\begin{vmatrix}2 & 1 & 2 \\3 & 1 & 3 \\ 1 & 1 & 7 \\\end{vmatrix}\quad C_3 ← C_3 - C_1\\
    d_4 &= (+1)×2×\begin{vmatrix}2 & 1 & 0 \\3 & 1 & 0 \\ 1 & 1 & 6 \\\end{vmatrix}\quad L_1 ← L_1 - L_2\\
    d_4 &= (+1)×2×\begin{vmatrix}-1 & 0 & 0 \\3 & 1 & 0 \\ 1 & 1 & 6 \\\end{vmatrix}\quad \text{La forme est triangulaire}\\
    d_4 &= 2×(-1)×1×6\\
    d_4 &= -12\\
    \end{align*}$$




## Exercice 7

Calculer le déterminant d'ordre $n>0$ suivant :

$$\Delta_n = \begin{vmatrix}
1 & 1 & \cdots & \cdots & 1\\
1 & 1 & 0 & \cdots & 0\\
\vdots & 0 & \ddots & \ddots & \vdots\\
\vdots & \vdots & \ddots & 1 & 0\\
1 & 0 & \cdots & 0 & 1\\
\end{vmatrix}$$

Il y a des $1$ :

- sur la diagonale principale,
- sur la première ligne,
- sur la première colonne.

??? success "Réponse"
    Pour le cas $n=1$, on a $\Delta_1 = |1| = 1$

    Pour le cas général $n\geqslant 2$, on développe par rapport à la deuxième ligne. _C'est plus efficace que par rapport à la dernière ligne._

    $$\Delta_n = (-1)×1×\begin{vmatrix}
    1 & 1 & \cdots & \cdots & 1\\
    0 & 1 & 0 & \cdots & 0\\
    \vdots & 0 & \ddots & \ddots & \vdots\\
    \vdots & \vdots & \ddots & 1 & 0\\
    0 & 0 & \cdots & 0 & 1\\
    \end{vmatrix} + (+1)×1×\Delta_{n-1}$$

    Le premier terme contient un déterminant d'ordre $n-1$, d'une matrice triangulaire supérieure, dont les coefficients diagonaux sont égaux à $1$, ce déterminant vaut donc $1^{n-1} = 1$. De sorte que

    $$\Delta_n = -1 + \Delta_{n-1}$$

    On en déduit que $(\Delta_n)_n$ est une suite arithmétique de raison $-1$ et de premier terme $\Delta_1 = 1$, et enfin que

    $$\forall n\geqslant 1,\quad \Delta_n = 2 - n$$
