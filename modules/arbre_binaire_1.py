def est_vide(ab):
    return ab is None

def gauche(ab):
    return ab[0]

def valeur(ab):
    return ab[1]

def droite(ab):
    return ab[2]


def taille(ab):
    if est_vide(ab):
        return 0
    else:
        return taille(gauche(ab)) + 1 + taille(droite(ab))

def hauteur(ab):
    if est_vide(ab):
        return 0
    else:
        return 1 + max(hauteur(gauche(ab)), hauteur(droite(ab)))

if __name__ == '__main__':
    # tests
    serge = (None, "Serge", None)
    regis = (None, "Régis", serge)
    annie = (None, "Annie", None)
    zoe = (regis, "Zoé", annie)

    assert taille(zoe) == 4
    assert hauteur(zoe) == 3

