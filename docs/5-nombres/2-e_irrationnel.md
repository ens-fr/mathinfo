# Preuve que e est irrationnel

On considère les deux suites $(u_n)_n$ et $(v_n)_n$ définies sur $\mathbb N$ par

$$u_n = \sum_{k=0}^n \frac1{k!}$$

$$v_n = u_n + \frac1{n!}$$

## Preuve que $(u)$ et $(v)$ sont adjacentes

### Montrons que $(u)$ est croissante

Pour $n \geqslant 0$, on a 

$$\begin{align*}
u_{n+1} - u_n &= \sum_{k=0}^{n+1} \frac1{k!} - \sum_{k=0}^n \frac1{k!}\\
u_{n+1} - u_n &= \left(\sum_{k=0}^n \frac1{k!} + \frac1{(n+1)!}\right) - \sum_{k=0}^n \frac1{k!}\\
u_{n+1} - u_n &= \frac1{(n+1)!}\\
u_{n+1} - u_n &> 0\\
\end{align*}$$

De sorte que $(u)$ est croissante.

### Montrons que $(v)$ est décroissante à partir du range 2

Pour $n\geqslant 2$, on a 

$$\begin{align*}
v_{n+1} - v_n &= u_{n+1} + \frac1{(n+1)!} - \left(u_n + \frac1{n!}\right)\\
v_{n+1} - v_n &= u_{n+1} - u_n + \frac1{(n+1)!} - \frac1{n!}\\
v_{n+1} - v_n &= \frac1{(n+1)!} + \frac1{(n+1)!} - \frac{n+1}{(n+1)!}\\
v_{n+1} - v_n &= \frac{1+1-(n+1)}{(n+1)!}\\
v_{n+1} - v_n &= \frac{1-n}{(n+1)!}\\
v_{n+1} - v_n &< 0\\
\end{align*}$$

### Montrons que $v_n > u_n$ pour $n\geqslant2$

Pour $n\geqslant 2$, on a 

$$\begin{align*}
v_n - u_n &= \frac1{n!}\\
v_n - u_n &> 0\\
\end{align*}$$

De sorte que $v_n > u_n$ pour $n\geqslant2$

### Montrons que $(v_n - u_n)_n$ tend vers $0$

On a $v_n - u_n = \frac1{n!}$ et $\lim_{n→\infty} n! = +\infty$, de sorte que 

$$\lim_{n→\infty} v_n - u_n = 0$$

### Conclusion

$(u)$ et $(v)$ sont des suites adjacentes, à partir de l'indice $2$.

Ainsi $u$ et $v$ sont convergentes, vers la même limite, que l'on note $\mathrm e$ et on admettra ici que $\mathrm e$ est la base des logarithmes naturels, $\mathrm e = \exp(1)$

## Irrationalité de $\mathrm e$

On a vu que pour $n\geqslant 2$

$$u_n < \mathrm e < v_n$$

Supposons que $\mathrm e$ soit rationnel, il est strictement positif, donc on peut l'écrire $\frac pq$ avec $p, q \in \mathbb N^*$. Quitte à utiliser l'algorithme d'Euclide, on peut rendre ces deux entiers premiers entre eux.

D'autre part, avec $u_2 = 1+1+\frac12 = 2.5$ et $v_2 = 2.5+\frac12 = 3$, on tire que $2.5< \mathrm e < 3$, ce n'est donc pas un entier et donc $q \geqslant 2$.

On déduit $u_q < \mathrm e < v_q$, ce qui s'écrit

$$1+\frac1{1!}+\frac1{2!}+\frac1{3!}+\cdots+\frac1{q!} < \frac pq < 1+\frac1{1!}+\frac1{2!}+\frac1{3!}+\cdots+\frac1{q!} +\frac1{q!}$$

Multiplions toutes ces doubles inégalité par $q!$, on a

$$q!+\frac{q!}{1!}+\frac{q!}{2!}+\frac{q!}{3!}+\cdots+\frac{q!}{q!} < p×(q-1)! < q!+\frac{q!}{1!}+\frac{q!}{2!}+\frac{q!}{3!}+\cdots+\frac{q!}{q!} +\frac{q!}{q!}$$

Pour $a\geqslant b$ entiers, on a 

$$\frac{a!}{b!} = \frac{1×2×3×\cdots×b×\cdots×a}{1×2×3×\cdots×b} = (b+1)×\cdots×a$$

Le membre de gauche est donc un entier que l'on note $N$ et on peut écrire

$$N < p×(q-1)! < N + 1$$

On déduit que $p×(q-1)!$ est un entier compris entre deux entiers consécutifs ; **c'est absurde**.

## Conclusion

Ainsi l'hypothèse initiale que $\mathrm e$ est rationnel est fausse.

!!! info "Résultat"
    $\mathrm e$ est un nombre irrationnel.

    [Cette preuve](https://fr.wikipedia.org/wiki/E_(nombre)#Irrationalit%C3%A9) est due à Joseph Fourier.

