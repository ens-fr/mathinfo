# Devoir surveillé de mathématiques

> Calculatrice autorisée (ou tablette avec NumWorks uniquement)

Version avec corrigé

## Exercice 1 (2 points)

On considère l'expression littérale $f(x) = -5x^2 +3x -7$

1. Indiquer l'ensemble de définition de $f$.
2. Indiquer le nom de la représentation graphique de $f$.
3. Donner le tableau de variations de la fonction $f$.
4. Donner une représentation graphique **très sommaire** de $f$.

??? success "Réponse"
    1. $f$ est une fonction définie sur $\mathbb R$, comme toute fonction polynomiale.
    2. $f(x)$ est un trinôme du second degré en $x$, donc la représentation graphique de $f$ est une **parabole**.
    3. Avec $a=-5$, $b=3$ et $c=-7$, on tire que 
        - avec $a<0$, la parabole est orientée vers le bas ; $f$ possède alors un maximum
        - l'antécédent du maximum est $-b/(2×a)$ = 3/10 = 0.3$
        - $f$ est donc croissante sur $]-\infty ; 0.3]$ et décroissante sur $]0.3 ; \infty]$
    4. On peut placer :
      
        - un point $A$ de coordonnées $(0 ; -7)$,
        - une droite tangente en $A$ d'équation $y==3x-7$
        - un point $B$ de coordonnées $(0.3 ; f(0.3)) = (0.3 ; -6.55)$, qui correspond au maximum de $f$.

        ![](./9-DS-courbe.png)
    

## Exercice 2 (2 points)

Résoudre l'équation : $9x^2 - 8x + 2 = 0$

??? success "Réponse"
    On pose $f(x) = 9x^2 - 8x + 2 = 0$.

    $f(x)$ est un trinôme du second degré en $x$, avec $a=9$, $b=-8$ et $c=2$.

    Son discriminant est $\Delta = b^2-4ac = (-8)^2-4×9×2 = +64 - 72 = -8$

    Le discriminant est strictement négatif, donc **l'équation $f(x) = 0$ n'a aucune solution réelle**.

## Exercice 3 (2 points)

La fonction $g$ suivante est-elle un trinôme du second degré ?

$$x\mapsto g(x) = x^3 + 2x + (1-x)(x-2)^2$$

??? success "Réponse"
    On va développer et réduire l'expression pour vérifier.

    $g(x) = x^3 + 2x + (1-x)(x-2)^2$

    $g(x) = x^3 + 2x + (1-x)(x^2 - 4x + 4)$

    $g(x) = x^3 + 2x + (x^2 - 4x + 4) + (-x^3 + 4x^2 - 4x)$

    $g(x) = 5x^2 -6x + 4$

    **Oui**, Il s'agit bien d'un trinôme du second degré.

## Exercice 4 (2 points)

On considère l'équation d'inconnue réelle $x$, $(\mathcal E) : ax^2+bx+c=0$ où $a$, $b$, et $c$ sont des réels avec $a\neq 0$.

1. Écrire une fonction Python qui renvoie le discriminant du trinôme $ax^2+bx+c$
2. Écrire une fonction Python qui renvoie le nombre de solutions de l'équation $(\mathcal E)$.

On pourra **recopier** et compléter le script ci-dessous :

```python
def delta(a, b, c):
    " Renvoie le discriminant de ax² + bx + c "
    return ...

def nb_solutions(a, b, c):
    """ Renvoie le nombre de solutions
    de l'équation ax² + bx + c = 0
    """
    discriminant = delta(a, b, c)
    if discriminant < 0:
        return ...
    elif discriminant ...:
        return 2
    else:
        return ...
```

??? success "Réponse"

    ```python
    def delta(a, b, c):
        " Renvoie le discriminant de ax² + bx + c "
        return b*b - 4*a*c
        # remarques : 
        # - les multiplications doivent être explicites
        # - le caractère ² est inconnu en Python,
        # donc `b² - 4ac` comporte plusieurs erreurs !
        return b**2 - 4*a*c  # autre possibilité

    def nb_solutions(a, b, c):
        """ Renvoie le nombre de solutions
        de l'équation ax² + bx + c = 0
        """
        discriminant = delta(a, b, c)
        if discriminant < 0:
            return 0
        elif discriminant > 0:
            return 2
        else:
            return 1
    ```

## Exercice 5 (4 points)

Résoudre les inéquations suivantes sur $\mathbb R$ :

1. $(-3x^2+x+2)(x+3) \geqslant 0$
2. $x^4-5x^2+4 < 0$

??? success "Réponse 1."
    On commence à factoriser $-3x^2+x+2$, c'est un trinôme du seconde degré en $x$, avec $a=-3$, $b=1$ et $c=2$.

    Son discriminant est $\Delta = 1^2-4×(-3)×2 = 1+24 = 25$ ; il est positif et $\sqrt{\Delta}=5$.

    Ainsi $-3x^2+x+2 = -3\left(x - \dfrac{-b-\sqrt \Delta}{2a}\right)\left(x - \dfrac{-b+\sqrt \Delta}{2a}\right) = -3\left(x - \dfrac{-1-5}{2×(-3)}\right)\left(x - \dfrac{-1+5}{2×(-3)}\right) = -3\left(x - 1\right)\left(x - \dfrac{-2}{3}\right)$

    L'équation est alors $-3\left(x - 1\right)\left(x - \dfrac{-2}{3}\right)(x+3) \geqslant 0$

    Faisons un tableau de signe

    |$x$      |   |$-3$|   |$-2/3$|   |$1$|   |
    |:--------|---|----|---|------|---|---|---|
    |$-3$     |$-$|$-$ |$-$| $-$  |$-$|$-$|$-$|
    |$x-1$    |$-$|$-$ |$-$| $-$  |$-$|$0$|$+$|
    |$x-(-2/3)$|$-$|$-$|$-$| $0$  |$+$|$+$|$+$|
    |$x+3$    |$-$|$0$ |$+$| $+$  |$+$|$+$|$+$|
    |Produit  |$+$|$0$ |$-$| $0$  |$+$|$0$|$-$|

    Ce qui permet de répondre ; les solutions de l'inéquation $(-3x^2+x+2)(x+3) \geqslant 0$ sont $x\in ]-\infty;-3]\cup[-2/3;1]$

??? success "Réponse 2."
    On pose $X=x^2$, ainsi $x^4-5x^2+4 = X^2-5X+4$ qui est un trinôme du second degré en $X$ dont le discriminant est $(-5)^2-4×1×4=25-16=9$ et sa racine carrée est $3$, ce qui permet de le factoriser : $X^2-5X+4 = 1×\left(X-\dfrac{-(-5)-3}{2×1}\right)\left(X-\dfrac{-(-5)+3}{2×1}\right) = (X-1)(X-4)$

    On peut facilement vérifier, en développant $(X-1)(X-4)$.

    On remplace à nouveau $X$ par $x^2$, et on déduit que

    $x^4-5x^2+4 = (x^2-1)(x^2-4)$ ; on peut encore factoriser

    $x^4-5x^2+4 = (x-1)(x+1)(x-2)(x+2)$ ; on peut alors faire un tableau de signe et répondre à la question.

    Les solutions de l'inéquation $x^4-5x^2+4 < 0$ sont $x\in ]-2;-1[\cup]1;2[$




## Exercice 6 (2 points)

$(u_n)_n$ est une suite géométrique de premier terme $u_0$ qui est différent de zéro. On sait que $3u_0 + 5u_1 + u_2 = 0$.

Quelle peut être la raison de cette suite ?


??? success "Réponse"
    $(u_n)$ est géométrique, donc si on note $q$ sa raison, on a :

    - $u_1 = q×u_0$
    - $u_2 = q×u_1 = q^2×u_0$

    De sorte que $3u_0 + 5u_1 + u_2 = 0$ est équivalent à

    $3u_0 + 5q×u_0 + q^2×u_0 = 0$ ; on peut simplifier par $u_0$ qui est non nul :warning:

    $3 + 5q + q^2 = 0$, on considère donc un trinôme du second degré avec $a=1$, $b=5$, $c=3$, de discriminant $\Delta = 5^2-4×1×3=25-12=13$.

    Les solutions de cette équation donnent les raisons possibles à la suite géométrique : $\dfrac{-5-\sqrt{13}}{2}$ et $\dfrac{-5+\sqrt{13}}{2}$.



## Exercice 7 (2 points)

$(v_n)_n$ est une suite arithmétique de raison $1$. On sait que $v_0^2 + v_1^2 + v_2^2 = 17$.

1. Montrer que $3v_0^2 + 6v_0 + 5 = 17$.
2. Quel peut être le premier terme de cette suite ?

??? success "Réponses"
    $(v_n)$ est arithmétique de raison $1$, donc

    - $v_1 = v_0 + 1$
    - $v_2 = v_1 + 1 = v_0 + 2$

    De sorte que $v_0^2 + v_1^2 + v_2^2 = 17$ est équivalent à

    $v_0^2 + (v_0+1)^2 + (v_0+2)^2 = 17$

    $v_0^2 + v_0^2 +2v_0 +1 + v_0^2 +4v_0 + 4 = 17$

    $3v_0^2 +6v_0 +5 = 17$ ce qu'il fallait démontrer.

    Le premier terme de la suite est donc une solution de l'équation $3x^2 + 6x -12 = 0$ qui est un trinôme du second degré en $x$ qui se simplifie en

    $$x^2+2x-4=0$$

    Le discriminant de ce trinôme du second degré est $\Delta = 2^2-4×1×(-4)=4+16=20$ qui est strictement positif, avec $\sqrt{20} = \sqrt{4×5} = 2\sqrt5$, on obtient deux solutions $\dfrac{-2-2\sqrt5}{2}$ et $\dfrac{-2+2\sqrt5}{2}$.

    Le premier terme de la suite peut donc être ou bien $-1-\sqrt 5$, ou bien $-1+\sqrt 5$.



## Exercice 8 (2 points)

Factoriser l'expression $h(x) = 2x^3+x^2-10x+7$

On pourra tracer la courbe représentative de $h$ avec une calculatrice, conjecturer la valeur d'une racine $x_1$, et **le prouver**. On pourra alors commencer la factorisation $h(x) = (x-x_1)(ax^2+bx+c)$ où $a$, $b$ et $c$ seront à déterminer. On pourra alors finir de factoriser.

??? success "Réponse"
    ![](./9-DS-cubique.png)

    On constate (c'est juste une conjecture) en déplaçant le curseur que $h(1)=0$. Vérifions-le par le calcul.

    $h(1) = 2×1^3+1^2-10×1+7 = 2+1-10+7= 0$ ; ainsi $1$ est une racine de cette équation polynomiale de degré 3. On peut alors écrire

    $h(x) = (x-1)(ax^2 + bx + c)$, où $a$, $b$ et $c$ sont inconnus.

    On trouve facilement $a=2$ et $c=-7$ en développant et identifiant le terme de degré 3 et le terme constant. Pour $b$, on regarde les termes de degré 1 et 2, on déduit

    - $-bx + cx = -10x$, d'où $-b-7=-10$
    - $-ax^2+bx^2=x^2$, d'où $-2+b=1$

    Ces deux équations conduisent à $b=3$.

    Ainsi $h(x) = (x-1)(2x^2 + 3x -7)$ que l'on souhaite encore factoriser.

    Le deuxième facteur est un trinôme du second degré, de discriminant $3^2-4×2×(-7) = 9+42=51$, on peut alors factoriser

    $$h(x) = (x-1)×2×\left(x-\dfrac{-3-\sqrt{51}}{4}\right)\left(x-\dfrac{-3+\sqrt{51}}{4}\right)$$

## Exercice 9 (2 points)

Résoudre l'inéquation : $\sqrt{x-1} > -2x+3$

On indiquera d'abord l'ensemble de résolution.

??? success "Réponse"
    L'ensemble de définition est donné par la contrainte $x-1\geqslant 0$, ainsi $x\geqslant 1$.

    L'équation est alors équivalente à

    $$\begin{cases}
    x-1\geqslant 0\\
    -2x+3< 0
    \end{cases}$$

    **ou bien**

    $$\begin{cases}
    x-1\geqslant 0\\
    -2x+3\geqslant 0\\
    x-1 > (-2x+3)^2
    \end{cases}$$

    Le premier cas est simple à résoudre, il revient à

    $$\begin{cases}
    x\geqslant 1\\
    x > 1.5
    \end{cases}$$

    Il se résume alors à $x > 1.5$

    Il y a un deuxième cas possible, **à ajouter**

    $$\begin{cases}
    x\geqslant 1\\
    x\leqslant 1.5\\
    x-1 > (-2x+3)^2
    \end{cases}$$

    dont la dernière partie est équivalente à

    $x-1 > 4x^2 -12x + 9$
    
    $4x^2 -13x + 10 < 0$, et après calculs

    $(4x-5)(x-2) < 0$, dont les solutions sont entre les racines $1.25$ et $2$

    Le deuxième cas se résume alors à $x\in]1.25 ; 1.5]$

    Enfin, quand on ajoute les deux cas, le bilan est :

    l'inéquation a pour solutions $x>1.2$



## Exercice Bonus (2 points)

Écrire une fonction Python `somme_et_produit` qui donne, en fonction de $s$ et $p$, les solutions au problème suivant :  
« Donner deux nombres dont la somme est $s$ et le produit est $p$. »

Cette fonction s'utilise ainsi :

```python
>>> somme_et_produit(21, 90)
(6.0, 15.0)
```

> Au lieu d'une fonction Python, on pourra aussi donner une méthode détaillée en français.

??? success "Réponse"

    ```python
    from math import sqrt

    def somme_et_produit(s, p):
        """Renvoie deux nombres dont la somme est s et le produit p.
        S'il n'y a pas de solution, renvoie None
        """
        # les nombres sont solutions de x² -sx +p
        delta = s*s - 4*p
        if delta < 0:
            return None
        else:
            return ((s - sqrt(delta)) / 2,
                    (s + sqrt(delta)) / 2
                    )
    ```

    Explication : avec $a=1$, $b=-s$ et $c=p$, il suffit de réécrire les formules du cours.

    Réponse en français, sans Python :

    - On résout l'équation $x^2-sx+p=0$
    - Si le discriminant est strictement négatif, il n'y a pas de solution
    - Sinon, les nombres cherchés sont les deux nombres trouvés
    - En cas de discriminant nul, les deux nombres sont identiques, ce qui permet d'utiliser la méthode du cas précédent.
