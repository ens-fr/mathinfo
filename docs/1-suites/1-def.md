# Définitions générales

Une **suite** est une liste ordonnée d'éléments, qu'on appelle **termes**.

!!! abstract "Exemples"
    * $a = (A, B, C, \cdots , Y, Z)$ est la suite ordonnée des lettres majuscules de l'alphabet latin, c'est une suite finie.
    * $b = (1, 3, 5, 7, \cdots)$ est la suite ordonnée des entiers positifs impairs, elle est infinie.
    * $F = (0, 1, 1, 2, 3, 5, 8, \cdots)$ est la suite débutant par $0$ puis $1$ et dont les termes suivants sont la somme des deux précédents. C'est la suite de Fibonacci.
    * $h = (1, \frac12, \frac13, \frac14, \cdots)$, $h$ pour suite **h**armonique.

!!! cite "Vocabulaire"
    * On dit qu'une suite est **numérique** quand ses termes sont des nombres.
    * Les termes sont **indicés** avec des entiers dans $\mathbb N$.
        * $a_0 = A$, $a_1 = B$, ... $a_{25} = Z$ ; les termes sont définis un à un.
        * $b_0 = 1$, $b_1 = 3$, $b_2 = 5$, et pour tout $n\in\mathbb N$, $b_n = 2n+1$ ; les termes sont définis de manière **explicite**.
        * $F_0=0$, $F_1=1$, et $F_{n+2} = F_{n+1} + F_n$ pour tout $n\in\mathbb N$ ; les termes sont définis de manière **récursive**.
        * $h_n = \frac1n$ pour tout $n\in\mathbb N^*$, $h_0$ n'est pas défini !
    * Il y a plusieurs notations pour une suite :
        * La suite $u$, son premier terme $u_0$.
        * La suite $(u)$, son dixième terme $u_9$.
        * La suite $(u_n)_n$, son terme de rang $n$ : $u_{n}$.
        * La suite $(u_n)_{n\in\mathbb N}$.
        * :octicons-light-bulb-16: On préfère les notations courtes, mais on utilise les notations longues pour lever des ambigüités. Dans ce cours, on utilisera principalement $(u_n)$ pour une suite dont les termes sont $u_n$.

!!! note "Comment définir une suite numérique ?"
    Définir une suite numérique, c'est donner assez d'informations pour pouvoir calculer ses termes.

    - On peut donner une formule **explicite** pour chaque terme.
        - Exemple : $u_n = (4n-3)×(5n-2)$, pour $n\in\mathbb N$.
    - On peut aussi donner les premiers termes et une relation **de récurrence** pour trouver les suivants en fonction des précédents.
        - Exemple avec la suite de Fibonacci citée précédemment.
    - On peut faire autrement, mais c'est plus délicat...

!!! tip "Conseils"
    - En mathématiques, on choisit souvent une seule lettre pour le nom d'une suite, et en particulier la suite $(u_n)_{n\in \mathbb N}$ est la plus souvent choisie pour désigner une suite générique.
    - En informatique, on pourra choisir un nom de variable à plusieurs lettres pour stocker les premiers termes d'une suite. On utilisera une structure de donnée linéaire.
        - Avec Python, on utilisera une liste (_list_).
        - Avec un tableur, on utilisera des cellules contigües.

## Calculatrice NumWorks

- C'est une calculatrice dont on peut [télécharger librement](https://www.numworks.com/fr/simulateur/telecharger/) un émulateur pour ordinateur ou pour tablette, voire téléphone.
- Un émulateur en ligne est disponible [ici, localement](../numworks/simulator.html).

=== "Accueil"
    ![accueil](./images/0-home.png){ .bordure }

    Avec les flèches, on accède au menu **Suites**.

=== "Suites"
    ![suites](./images/1-suites.png){ .bordure }

    On peut alors définir une suite

=== "Choix"
    ![Choix](./images/2-explicite.png){ .bordure }

    On peut créer une suite de manière explicite.


=== "Définition"
    ![définition](./images/3-def.png){ .bordure }

    On prend ici l'exemple de la suite telle que $u_n = 2n-3$ pour $n\in\mathbb N$.

=== "Graphique"
    ![graph](./images/4-graph.png){ .bordure }

    On peut tracer une représentation graphique et se déplacer sur les points crées. On peut alors observer les **variations** de la suite.

=== "Tableau"
    ![tableau](./images/5-tab.png){ .bordure }

    On peut aussi visualiser un tableau de valeurs.

## Tableur

### Formules explicites

Avec un tableur, on peut construire des suites de manière explicite.

- On construit une première colonne avec les indices ; on écrit `0` dans la case `A1` et on l'étire vers le bas.
- Il suffit alors d'écrire `= f(A1)` dans la case `B1`, puis d'étirer vers le bas. On remplacera `f` par la fonction de son choix...
- Ici, on représente la suite avec $u_n = \dfrac1{1+n^2}$

![](./images/expl1.png){ .bordure }

- On peut aussi insérer une première ligne pour l'étiquette. $\boxed{n}$ et $\boxed{u_n}$ par exemple.
- On peut alors obtenir un beau graphique à insérer dans un document.
    - On clique sur **Insérer un diagramme** :bar_chart:

=== "Type de diagramme"
    ![](./images/expl2.png)

    Ensuite, on clique sur **Ligne**, on garde **Points seuls**, et **Suivant >**.

=== "Plage de données"
    ![](./images/expl3.png)

    - On conserve : **Séries de données en colonnes**
    - On conserve : **Première ligne comme étiquette** (:warning: si on l'a bien ajoutée)
    - ==On coche== : **Première colonne comme étiquette** ; pour avoir la seconde colonne en fonction de la première.
    - puis **Suivant >**.

=== "Séries de données"
    ![](./images/expl4.png)

    - Rien de particulier ; **Suivant >**.

=== "Éléments du diagramme"
    ![](./images/expl5.png)

    - On complète les titres et la légende.
    - C'est terminé.

!!! success "Résultat"
    ![](./images/expl6.png)

### Formules par récurrence

Avec un tableur, on peut aussi construire des suites définies par récurrence. Par exemple, pour la suite de Fibonacci :

- On écrit les premiers termes dans les cases `A1` (0) et `A2` (1).
- Dans `A3`, on écrit `= A1 + A2`, et on étire vers le bas.

![](./images/calc-1.png){ .bordure }
![](./images/calc-2.png){ .bordure }

:warning: La première ligne est numérotée `1`, mais elle correspond ici à l'indice `0`.

On peut aussi tracer des graphiques avec le même procédé que précédemment.

## Langage Python

### Fonction explicite

Il peut être utile de définir une fonction qui prend l'indice en paramètre, on peut alors lancer le script et l'utiliser dans la console. L'exemple ci-dessous peut être reproduit aussi dans la calculatrice NumWorks ; il suffit d'aller dans le menu Python, d'ajouter un script et de l'exécuter.

!!! example "Exemple"
    On définit la suite telle que $u_n = \dfrac1{1+n^2}$

{{ IDE('expl') }}

- Lancez le script,
- puis testez `#!py >>> u(5)` pour calculer $u_5$.

!!! success "Résultat"
    ```pycon
    >>> %Script exécuté
    >>> u(5)
    0.038461538461538464
    >>> 
    ```


## Exemples ludiques

!!! example "Curiosité"
    Saurez-vous trouver les termes suivants ?
    
    <code>M <u><strong>♡</strong></u> 8 ~~M~~</code>


    ??? success "Réponse"
        - Ces symboles possèdent un axe de symétrie.
        - Placez-les en colonnes de façon à **partager** l'axe commun de symétrie.
        - N'étudiez **qu'une** moitié...
        - Vous devriez observer `1 2 3 4` et donc déduire les termes suivants.

!!! example "La suite audioactive de Conway"
    Saurez-vous trouver les termes suivants ?

    $$1$$

    $$11$$

    $$21$$

    $$1211$$

    $$111221$$

    $$312212$$

    $$\cdots$$

    ??? success "Réponse"
        En anglais, cette suite s'appelle _Look and say_ qu'on a traduit en audioactive.

        On lit les chiffres d'une ligne, par lots.

        1. Sur la première ligne, il y a **un** $1$, donc on écrit $11$ sur la deuxième ligne.
        2. Sur la deuxième ligne, il y a **deux** $1$, donc on écrit $21$ sur la troisième ligne.
        3. Sur la troisième ligne, il y a **un** $2$ et **un** $1$, donc on écrit $1211$ sur la quatrième ligne.
        3. Sur la quatrième ligne, il y a **un** $1$, **un** $2$ et **deux** $1$, donc on écrit $111221$ sur la cinquième ligne.
        4. ...
        

    Cela fait un joli exercice de programmation : écrire une fonction qui donne, en fonction d'une ligne donnée, la ligne suivante.

    ```pycon
    >>> audioactive("111221")
    '312212'
    ```

