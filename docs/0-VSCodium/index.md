---
author: Franck CHAMBON
---

# VSCodium

!!! failure "VSCode"
    Visual Studio Code (VSCode) est un éditeur libre et multiplateforme pour ordinateur,
     efficace et très complet, produit par Microsoft.
    
    **Mais**
    
    1. VSCode inclut de la télémétrie [^telemetrie] (une sorte de mouchard). On peut certes la désactiver, mais par principe...
    2. Le [*MarketPlace*](https://marketplace.visualstudio.com/VSCode) de VSCode contient des extensions libres, mais aussi d'autres qui ne le sont pas, et qui peuvent inclure de la télémétrie également..
    
    ==Nous ne pouvons donc pas le recommander.==

    [^telemetrie]: Télémétrie dans VSCode, [commentaire d'un développeur VSCode](https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005) ; en anglais.

!!! done "VSCodium"
    [VSCodium](https://github.com/VSCodium/vscodium) est une version
    de VSCode distribuée avec une licence libre (MIT) qui est bien plus respectueuse.

    1. Il n'y a **pas de télémétrie** dans VSCodium.

    2. Le [*MarketPlace*](https://open-vsx.org/) de **VSCodium ne contient que des extensions libres, sans télémétrie**.
    
    ==On recommande d'utiliser VSCodium.==

!!! question "La différence d'utilisation"
    Avec un terminal,
    
    - pour lancer VSCodium, on entre : `#!bash codium .`
    - Pour lancer VSCode, on aurait entré : `#!bash code .`
    
    Tout comme `chromium` est la version libre de `chrome`.

    L'aide que l'on peut trouver en ligne sur VSCode se traduit
     alors en remplaçant `code` par `codium`.
    
    > [La documentation officielle (en anglais)](https://code.visualstudio.com/docs)

    
    On peut aussi utiliser l'autocomplétion, on entre `#!bash $ cod` puis ++tab+space+"."+enter++

## Installation

VSCodium est multiplateforme, il suffit de suivre les indications proposées sur le [site officiel](https://github.com/VSCodium/vscodium). Ci-dessous, une traduction rapide.

=== "Android"
    VSCodium n'est pas disponible pour Android.
    En 2022, il n'y a pas d'IDE Python correct qui respecte le RGPD.

    Sur tablette, on recommande d'utiliser des sites web équipés de Pyodide. Comme Basthon, Capytale ou les productions e-nsi.

=== "Linux (Debian/Ubuntu/Mint)"
    Dans un Terminal, l'administrateur entre :

    ```bash
    sudo apt-get install extrepo
    sudo extrepo enable vscodium
    sudo apt-get update
    sudo apt-get install codium
    ```

=== "Mac OS"

    1. Installer (si ce n'est pas déjà fait) le gestionnaire de paquet
     [Homebrew](https://brew.sh/index_fr). Dans un terminal, entrer :
    ```bash
    $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    ```
    Attention, même si cette méthode d'installation est de plus en plus répandue, elle est sujette à caution. En particulier, elle expose à des vulnérabilités de type « Supply Chain Attack ».
    2. Ensuite, on peut installer VSCodium avec :
    ```bash
    $ brew install --cask vscodium 
    ```

    Attention, même si cette méthode d'installation est de plus en plus répandue, elle est sujette à questionnement. En particulier, elle expose à des vulnérabilités de type « *Supply Chain Attack* ».

=== "Windows"
    Il existe plusieurs gestionnaires de paquets pour Windows.

    === "Avec WinGet"
        Avec *Windows Package Manager (WinGet)*

        1. À partir de Windows 10 1709 (build 16299), on peut installer le
         [App Installer](https://www.microsoft.com/en-us/p/app-installer/9nblggh4nns1?activetab=pivot:overviewtab)

        2. Ensuite, on peut installer VSCodium avec :
        ```bash
        winget install vscodium 
        ```

    === "Avec *Chocolatey*"
        Avec *Chocolatey*

        1. Installer [Chocolatey](https://chocolatey.org/), si ce n'est pas déjà fait.
        2. Ensuite, on peut installer VSCodium avec :

        ```bash
        choco install vscodium 
        ```

## Configuration rapide

On propose dans cette section, une méthode rapide de configuration commune, avec un script. Vous retrouverez ensuite le détail par extension.

1. Supprimer toute ligne qui ne vous parle pas,
2. Lancer ce script dans un terminal

```bash title="Installation d'extensions pour VSCodium"
# Francisation
codium --install-extension MS-CEINTL.vscode-language-pack-fr                # Menus en français
codium --install-extension streetsidesoftware.code-spell-checker    # Correcteur orthographique
codium --install-extension streetsidesoftware.code-spell-checker-french-reforme  # Dictionnaire récent
codium --install-extension valentjn.vscode-ltex  # Correcteur grammatical

# Programmation
codium --install-extension ms-python.python                    # Éditeur connecté avec Python(s)
codium --install-extension ms-toolsai.jupyter                  # Gestion des carnets Jupyter
codium --install-extension KevinRose.vsc-python-indent         # Meilleures indentation Python
codium --install-extension CoenraadS.bracket-pair-colorizer-2  # Parenthésage en couleur

# Travail avec Markdown
codium --install-extension shd101wyy.markdown-preview-enhanced  # Moteur de rendu pour création rapide
codium --install-extension christian-kohler.path-intellisense  # Chemins proposés
codium --install-extension jock.svg                            # Visualisation des fichiers SVG
codium --install-extension tomoki1207.pdf                      # Visualisation des fichiers PDF

# Travail avec Gitlab
codium --install-extension GitLab.gitlab-workflow  # Travail collaboratif sur la forge
codium --install-extension mhutchie.git-graph      # Visualisation de l'arborescence

# Travail sur les bases de données
codium --install-extension mechatroner.rainbow-csv  # Visualisation des fichiers CSV
codium --install-extension alexcvzz.vscode-sqlite   # Faire des requêtes SQL
codium --install-extension dineug.vuerd-vscode      # Création de diagramme Entité-Relation
```

Un jour, celle-ci sera disponible `#!bash codium --install-extension nhoizey.gremlins  # Détection de caractères trompeurs`

Ensuite, une fois installées, il faut les configurer.

## Premier démarrage

Sans extensions, cela ressemble plutôt à

![Premier démarrage](./assets/1-initial.png)

## Configuration rapide

Appuyez sur ++ctrl++ + ++comma++ et cela ressemble à

![Avec extensions](./assets/VSCodium-settings-json.png)

1. Cliquez en haut à droite, « Afficher les paramètres (en JSON) » comme sur l'image.
2. Insérez ou adaptez le dictionnaire suivant :

```json title="Fichier settings.json"
{
    "editor.renderWhitespace": "boundary",
    "editor.parameterHints.enabled": false,
    "workbench.startupEditor": "none",
    "window.zoomLevel": 2,
    "python.terminal.executeInFileDir": true,
    "python.terminal.launchArgs": [
        "-m",
        "IPython",
        "--no-autoindent",
    ],
    "markdown.updateLinksOnFileMove.enabled": "prompt",
}
```

Un fichier JSON est un dictionnaire, on étudie cela en NSI en première, il y a un fonctionnement par clé-valeur. À chaque clé, correspond une valeur.

!!! tip "Explication par `#!json "clé": "valeur"`"

    `#!json "editor.renderWhitespace": "boundary"`
    : Contrôle la façon dont l'éditeur doit restituer les caractères espaces. Avec `boundary`, tous sauf les espaces uniques entre mots. **Très utile.**

    `#!json "editor.parameterHints.enabled": false`
    : Empêche l'ouverture intempestive de fenêtre de documentation quand on tape du code. **Très utile.** Mais, certains aiment bien cette fenêtre...

    `#!json "workbench.startupEditor": "none"`
    : Contrôle quel éditeur s'affiche au démarrage, si aucun n'est restauré de la session précédente.

    `#!json "window.zoomLevel": 2`
    : Modifiez le niveau de zoom de la fenêtre. La taille d'origine est 0. Chaque incrément supérieur (exemple : 1) ou inférieur (exemple : -1) représente un zoom 20 % plus gros ou plus petit. Vous pouvez également entrer des décimales pour changer le niveau de zoom avec une granularité plus fine. **À régler suivant votre préférence.**

    `"python.terminal.executeInFileDir": true`
    : Exécute les fichiers Python dans un terminal depuis le dossier où ils se trouvent, et non depuis le dossier de travail de VSCodium. **Très utile, en particulier lorsqu'on travaille avec des fichiers externes**

    `#!json "python.terminal.launchArgs": [...]`
    : Avec les bons arguments permets d'avoir IPython dans le terminal quand on sélectionne du code et qu'on souhaite le tester immédiatement avec ++shift++ + ++enter++

!!! abstract "Configuration complémentaire"

    Ajouter les `#!json "clé": "valeur"` suivantes, si le sens est limpide pour vous :

    ```json title="suite du fichier settings.json"
    {
        "cSpell.language": "fr-FR",
        "ltex.language": "fr",
        "editor.fontFamily": "'Fira Code'",
        "editor.fontLigatures": false,
        "security.workspace.trust.untrustedFiles": "open",
        "markdown-preview-enhanced.previewTheme": "atom-light.css",
        "markdown-preview-enhanced.mermaidTheme": "dark",
        "markdown-preview-enhanced.printBackground": true,
        "svg.preview.mode": "svg",
        "git.enableSmartCommit": true,
        "git.confirmSync": false,
        "git.autofetch": true,
    }
    ```

    > Pour `Fira Code`, excellente police à chasse fixe pour coder, on ne recommande pas les ligatures aux élèves, mais seulement aux habitués.

    Avant cela, pour installer `Fira Code`, on suivra ces [instructions](https://github.com/tonsky/FiraCode/wiki/Installing)




## Installation manuelle d'extension

!!! info "Inutile"
    Si vous avez suivi les étapes précédentes :wink:

    Mais il y a d'autres extensions présentées.

:warning: Il faudra regarder aussi, les configurations à apporter.

Sinon, au premier démarrage de VSCodium,
on peut aller dans le gestionnaire d'extensions
avec (<kbd>Ctrl</kbd>+<kbd>Maj</kbd>+<kbd>X</kbd>),
ou alors en cliquant (au milieu à gauche) sur le carré coupé en quatre,
dont un morceau est détaché.

![extensions](assets/2-extensions.png)

### Menus en français

Pour franciser VSCodium :

1. Rechercher *french* dans le gestionnaire d'extension.
2. Installer [*French Language Pack for VS Code*](https://open-vsx.org/extension/MS-CEINTL/vscode-language-pack-fr)
3. Redémarrer VSCodium.

### Correction orthographique et grammaticale

Pour les variables du code et les commentaires. :warning: Penser à la configuration.

1. Rechercher et installer [*French - Code Spell Checker*](https://open-vsx.org/extension/streetsidesoftware/code-spell-checker-french)
2. **Configuration**
    - Appuyer sur <kbd>F1</kbd> taper `spell` et choisir `Spell Checker configuration info`
    - Choisir l'onglet `USER`, et décocher `English`, puis cocher `French`.

Pour les fichiers LaTeX et Markdown. :warning: Penser à la configuration.

1. Rechercher et installer [*LTeX*](https://open-vsx.org/extension/valentjn/vscode-ltex)
2. **LTeX n'est pas automatiquement configuré** pour le français, ainsi
    1. Une fois installée, cliquer sur la roue dentée de `LTeX` (paramètres d'extensions)
    2. Dérouler vers le bas et chercher la section `Ltex: language`
    3. Dans le menu déroulant, choisir `fr` pour `french`.

### Enjoliveurs, au choix

1. Des thèmes sombres
    1. Chercher et installer l'extension [*Material Theme*](https://open-vsx.org/extension/Equinusocio/vsc-material-theme) ; pour un thème sombre complet.
    2. Ou alors, chercher et installer l'extension [*Nord*](https://open-vsx.org/extension/arcticicestudio/nord-visual-studio-code)
2. Chercher et installer l'extension [*Bracket Pair Colorizer 2*](https://open-vsx.org/extension/CoenraadS/bracket-pair-colorizer-2) ; pour mieux voir vos parenthèses.

### Python

Rechercher [*Python*](https://open-vsx.org/extension/ms-python/python) et
 installer l'extension de Microsoft. Ceci n'installe pas Python, mais fera
 le lien entre Python déjà installé et VSCodium. Il **faut** donc avoir
 fait l'installation de Python sur votre ordinateur pour utiliser cette extension.

!!! warning "Rappel"
    Quand on installe Python pour Windows, il **faut**
    bien penser à cocher la case « Inclure Python dans le PATH ».

    ![py_win](./assets/py_win.png){ width=50% }

Une fois installé, vous pouvez tester.

1. Créez un fichier `test.py` de type Python.
2. En bas, à gauche, devrait être affichée votre version de python.
3. Éditez `print("Salut à tous !")`
4. Appuyez sur ++ctrl+f5++.
5. Une fenêtre devrait s'ouvrir, avec le résultat attendu de votre script.

D'autre part, on peut aussi exécuter seulement un extrait de code.

1. Ajoutez des lignes de Python à votre fichier `test.py`
2. Sélectionnez quelques lignes
3. Appuyez sur ++shift++ + ++enter++

### Meilleure indentation avec Python

Rechercher et installer l'extension [*Python Indent*](https://open-vsx.org/extension/KevinRose/vsc-python-indent)

### Données en table (`.csv`)

En classe de première, on manipule des fichiers `.csv`,
 et on peut faire aussi les toutes premières expériences avec SQL.

Rechercher et installer l'extension [*Rainbow CSV*](https://open-vsx.org/extension/mechatroner/rainbow-csv)

### Langage SQL

En terminale, on fait une initiation au langage `SQL`.

Rechercher et installer l'extension [*SQLite*](https://open-vsx.org/extension/alexcvzz/vscode-sqlite)
Rechercher et installer l'extension [*ERD Editor*](https://open-vsx.org/extension/dineug/vuerd-vscode)

## Extensions utiles

### Complétion automatique de noms de fichier

Rechercher et installer l'extension [*Path Autocomplete*](https://open-vsx.org/extension/ionutvmi/path-autocomplete) ; pour compléter automatiquement les noms de fichiers.

```bash
codium --install-extension ionutvmi.path-autocomplete
```

### Markdown

Pour créer des pages HTML grâce au langage Markdown, et visualiser en direct le rendu HTML.

Rechercher et installer l'extension [*Markdown Preview Enhanced*](https://open-vsx.org/extension/shd101wyy/markdown-preview-enhanced)

```bash
codium --install-extension shd101wyy.markdown-preview-enhanced
```

## Compléments

### Suggestions d'extensions

- Ouvrir un fichier avec une extension particulière puis le gestionnaire d'extensions.
- Des suggestions sont proposées...

Par exemple avec un fichier `.asy`, l'extension `supakornras.asymptote` intéressera les enseignants qui dessinent avec ce logiciel.

### Où sont les extensions ?

Les extensions sont stockées dans le répertoire :

- Windows : `%USERPROFILE%\.vscode-oss\extensions`
- Linux (tout comme macOS) : `~/.vscode-oss/extensions`

> Il est possible de lancer VSCodium en ligne de commande
 avec un autre répertoire d'extensions avec le paramètre : `--extensions-dir <dir>`

### Installer une extension qui n'est pas (encore) sur le MarketPlace libre

Il est possible de :

- visiter le MarketPlace de VSCode,
- d'y repérer une extension libre intéressante,
- de la télécharger (chercher *Download* et cliquer sur le lien vers un fichier `.vsix`),
- de l'installer pour VSCodium en ligne de commande, comme ci-dessous.

`codium --install-extension mon_extension_toute_choupinou.vsix`

!!! note "Exercice"
    1. Chercher sur le MarketPlace de VSCode : [*Subtitles Editor*](https://marketplace.visualstudio.com/items?itemName=pepri.subtitles-editor)
    2. L'installer pour VSCodium
    3. Vous pourrez alors éditer des fichiers de sous-titres facilement (traduction automatique, décalage, ...)

!!! tip "Gremlins, une extension indispensable"
    L'extension `enhoizey.gremlins` est à chercher et installer.

    Elle détection et affiche les caractères trompeurs.


- Il existe aussi de quoi jouer avec *emoji*.

### D'autres avis

- <https://besson.link/visualstudiocode.fr.html>

- _In english, just follow the [link](https://github.com/VSCodium/vscodium/blob/master/DOCS.md)._
  On y détaille les répertoires pour migrer de VSCode à VSCodium,
  pour ceux qui avaient commencé à utiliser VSCode avant.
