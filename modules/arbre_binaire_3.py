import collections
Noeud = collections.namedtuple('Noeud', ['gauche', 'élément', 'droite'])


def est_vide(ab):
    return ab is None

def somme(ab):
    """On suppose ici que l'arbre binaire ne comporte
    que des valeurs numériques.
    """
    if est_vide(ab):
        return 0
    else:
        return somme(ab.gauche) + ab.element + somme(ab.droite)

def taille(ab):
    if est_vide(ab):
        return 0
    else:
        return taille(ab.gauche) + 1 + taille(ab.droite)

def hauteur(ab):
    if est_vide(ab):
        return 0
    else:
        return 1 + max(hauteur(ab.gauche), hauteur(ab.droite))

def est_peigne(ab):
    "ab peut être vide"
    if est_vide(ab):
        return True
    elif est_vide(ab.droite):
        return est_peigne(ab.gauche)
    elif est_vide(ab.gauche):
        return est_peigne_bis(ab.droite)
    else:
        return False

def est_peigne_bis(ab):
    "ab n'est pas vide"
    if est_vide(ab.droite):
        return est_peigne(ab.gauche)
    elif est_vide(ab.gauche):
        return est_peigne_bis(ab.droite)
    else:
        return False
    
if __name__ == '__main__':
    # test
    serge = Noeud(None, "Serge", None)
    regis = Noeud(None, "Régis", serge)
    annie = Noeud(None, "Annie", None)
    zoe = Noeud(regis, "Zoé", annie)

    assert taille(zoe) == 4
    assert hauteur(zoe) == 3

    assert not est_peigne(zoe)
    assert est_peigne(regis)
